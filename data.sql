USE `shops4scuola` ;
	INSERT INTO users (name, email, password, `type`) VALUES ("pino", "pino@gmail.com", "aaa", "USER"),
                                                        ("babi", "babi@gmail.com", "bbb", "USER"),
														("chris", "chris@gmail.com", "ccc", "USER"),
														("dani", "dani@gmail.com", "ddd", "USER"),
														("erika", "erika@gmail.com", "eee", "USER"),
                                                        ("alberto", "alberto@gmail.com", "fff", "SELLER"),
														("ESCOSHOP", "escoshop@gmail.com", "ggg", "SELLER"),
														("SoundGarden" , "commercial@soundgarden.com" , "hhh", "SELLER");

														
	insert into categories (name) values ("Arte"), ("Automobili"), ("Accessori per la pesca"), ("Attrezzatura da campeggio"), ("Abbigliamento"),
                                    ("Abbigliamento - Pantaloni"), ("Abbigliamento - Cappelli"), ("Abbigliamento - Calzature"), ("Abbigliamento - Giacche"), ("Abbigliamento - accessori"),
									("Alimentari"), ("Borse"),("Biciclette e accessori"), ("Casa - Infissi"), ("Casa - Illuminazione"),
									("Casa - Accessori"), ("Casa - Tavola e Cucina"), ("Casa - Bagno"), ("Casa - Mobili e arredamento"), ("Cartoleria"),
									("Computer e accessori"), ("Cosmetici e profumi"), ("Elettronica"), ("Elettrodomestici"), ("Edilizia"),
									("Ferramenta"), ("Forniture elettriche"), ("Gioielleria"), ("Giardinaggio"), ("Giocattoli"), 
									("Illuminazione"), ("Intrattenimento"),("Igiene personale"), ("Libri e riviste"), ("Motocicli") ,
									("Musica"), ("Prodotti per animali"), ("Sartoria"), ("Sport"), ("Telefonia"),
									("Viaggi e vacanze");
									
    INSERT INTO products (name, categoryId, sellerId, price, description, imgsrc) VALUES ("Camicia", 5, 6, 10, "camicia in lino taglia 46", "1.jpg"),
                                                        ("Cravatta", 10, 6, 5, "cravatta di seta con motivo a quadri rossi e verdi", "2.jpg"),
														("Giacca", 9, 6, 50, "giacca in montone" ,"3.jpg"),
														("Sticker", 20, 6, 1, null, "4.jpg"),
														("Cavo usb type C", 23, 7, 7.5, "cavo usb type-c 1.5A max", "5.jpg"),
                                                        ("IPhoneX", 23, 7, 700, "apple inc", "6.jpg"),
														("Vetro protettivo IPhoneX", 23, 7, 12, "3 pezzi", "7.jpg"),
														("GEAR", 1, 7, 12, "hello", "8.jpg"),
														("Cover personalizzabile IPhoneX", 23, 7, 12, "cover in policarbonato personalizzabile con stampa", "9.jpg"),
														("The Dark Side of the Moon 1edizione", 36, 8, 1200, "Vinile , prima stampa", "10.jpg"),
														("MC PIMP - Kill these *** " , 36 , 8 , 12, "GANGSTA RAP; DIGITAL COPY; 1997 SUBURBIA RECORDS" , "11.jpg");

	INSERT INTO options (	productId , name , description , price , required , `type`	)
	VALUES ( 1 , "taglia" , "la taglia dell abito" , 0.0 , TRUE, "INTRANGE" ) , ( 1 , "motivo" , "il disegno sul tessuto" , 0.0 , TRUE, "VALUE" ) ,
           ( 1 , "ricamo testo" , "breve testo ricamato su maniche e colletto" , 10.0 , FALSE, "TEXTVALUE" ) , ( 1 , "vestibilità" , "stie dei fianchi", 0.0 , TRUE , "VALUE") ,
		   ( 2,  "motivo" , "il disegno sul tessuto" , 0.0 , TRUE, "VALUE" ) , ( 2 , "ricamo testo" , "breve testo ricamato su maniche e colletto" , 10.0 , FALSE, "TEXTVALUE" ) ,
		   ( 3 , "taglia" , "la taglia dell abito" , 0.0 , TRUE, "INTRANGE" ) ,( 3 , "ricamo testo" , "breve testo ricamato su maniche e colletto" , 10.0 , FALSE, "TEXTVALUE" ), ( 3 , "vestibilità" , "stie dei fianchi" , 0.0 , TRUE , "VALUE") ,
		   ( 5 , "lunghezza" , "lunghezza del cavo" , 0.0 , TRUE, "VALUE" ),
		   ( 6 , "colore" , "colore della placca posteriore in metallo" , 0.0 , TRUE, "VALUE" ) , ( 6 , "memoria interna" , "spazio di archiviazione disponibile" , 0.0 , TRUE, "VALUE" ) , 
		   ( 8 , "A" , "BOH VALUE" , 0.0 , TRUE , "VALUE") , ( 8 , "numero denti" , "quanti denti deve avere la corona" , 0.0 , TRUE , "INTRANGE") , ( 8 , "diametro" , "diametro a valle della dentatura" , 0.0 , TRUE , "FLOATRANGE") , ( 8 , "modello" , "fornisci direttamente un file STL" , 3.0 , FALSE , "FILEVALUE") , ( 8 , "incisione" , "testo da incidere sulla faccia dell ingranaggio" , 0.0 , FALSE , "TEXTVALUE") ,
		   ( 9 , "colore" , "colore della plastica" , 0.0 , TRUE, "VALUE" ), ( 9 , "stampa" , "immagine da stampare" , 0.0 , FALSE, "FILEVALUE" ) ,
		   ( 11 , "formato" , "formato della traccia , influisce sulla qualità della riproduzione" , 0.0 , TRUE , "VALUE");

    INSERT INTO intRangeOptions ( productId, name , minimum, maximum ) 
	VALUES ( 1 , "taglia", 35,  55) ,(3 , "taglia", 35,  55),
    	   ( 8 , "numero denti" , 0 , null );


    INSERT INTO floatRangeOptions ( productId, name , minimum, maximum ) 
	VALUES ( 8 , "diametro" , 1.0 , 1000.0 );

    INSERT INTO valueOptions (  productId, name , `value` , price ) 
	VALUES ( 1 , "motivo", "righe bianche e blu" , 0.0) , (1 , "motivo", "righe bianche e rosse" , 0.0), (1 , "motivo", "righe bianche e nere" , 0.0), (1 , "motivo", "tropical" , 10.0),
		   ( 1 , "vestibilità", "Regular" , 0.0) , (1 , "vestibilità", "Slim" , 0.0) ,
		   ( 2 , "motivo", "righe bianche e blu" , 0.0) , (2 , "motivo", "righe bianche e rosse" , 0.0), (2 , "motivo", "righe bianche e nere" , 0.0), (2 , "motivo", "tropical" , 3.0),
		   ( 3 , "vestibilità", "Regular" , 0.0) , (3 , "vestibilità", "Slim" , 20.0) ,
		   ( 5 , "lunghezza", "1.0 mt" , 0.0) , (5 , "lunghezza", "1.5 mt" , 2.0), (5 , "lunghezza", "2.0 mt" , 3.0),
	       ( 6 , "colore" , "deep white" , 0.0) , ( 6 , "colore" , "void black" , 0.0) , ( 6 , "colore" , "vibrant red" , 0.0) , ( 6 , "colore" , "FuScHiA" , 0.0) , 
		   ( 6 , "memoria interna" , "256 GB" , 0.0) , ( 6 , "memoria interna" , "512 GB" , 100.0) , ( 6 , "memoria interna" , "1 TB" , 200.0 ) , ( 6 , "memoria interna" , "1.5 TB" , 400.0),
		   ( 8 , "A" , "false" , 0.0) , ( 8 , "A" , "true" , 10.0) ,
		   ( 9 , "colore" , "bianco" , 0.0) , ( 9 , "colore" , "nero" , 0.0) , ( 9 , "colore" , "giallo" , 0.0) , ( 9 , "colore" , "verde" , 0.0) , ( 9 , "colore" , "blu" , 0.0) , ( 9 , "colore" , "viola" , 0.0) , ( 9 , "colore" , "rosso" , 0.0) , ( 9 , "colore" , "arancio" , 0.0),
		   ( 11 , "formato" , "WAV" , 2.0) , ( 11 , "formato" , "MP3" , 1.0) , ( 11 , "formato" , "AIFF?" , 2.0);




	INSERT INTO orders (clientId , sellerId , price ) VALUES (1,6, 10);
	CALL insertOrderDetail(1,1,1,"('motivo'),('taglia'),('vestibilità')","('tropical'),('40'),('Slim')");

    INSERT INTO orders (clientId , sellerId , price ) VALUES (1 , 6, 15);
	CALL insertOrderDetail(2,2,3,"('motivo')","('righe bianche e rosse')");
														   
    

    INSERT INTO orders (clientId , sellerId , price ) VALUES (2, 6,  25);
			CALL insertOrderDetail(3,1,2,"('motivo'),('taglia'),('vestibilità')","('tropical'),('40'),('Regular')");	
			CALL insertOrderDetail(3,2,1,"('motivo')","('righe bianche e nere')");	
	
    INSERT INTO orders (clientId , sellerId , price ) VALUES (2, 7, 19.5);
		CALL insertOrderDetail(4,5,1,"('lunghezza')","('1.0 mt')");	
		CALL insertOrderDetail(4,7,1,"","");	