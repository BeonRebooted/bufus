/*si è cambiato my.ini max_allowed_packet=1M    TO 1024M*/
DROP DATABASE IF EXISTS `shops4scuola`;
CREATE DATABASE `shops4scuola` DEFAULT CHARACTER SET utf8 ;

/*noooo, non è supportato nei prepared statements :-/ */
USE shops4scuola;


create table if not exists users (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(64)  NOT NULL,
    email varchar(64) NOT NULL,
    password varchar(64) NOT NULL,
    `type` varchar(8)  NOT NULL CHECK (`type` IN ('USER', 'SELLER', 'ADMIN')),
   PRIMARY KEY (id),
   UNIQUE (email)
);

CREATE UNIQUE INDEX mail_idx ON users (`email`);	
													
create table if not exists categories (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(128) NOT NULL,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX catname_idx ON categories (`name`);	
												      
create table if not exists products (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(256) NOT NULL,
    categoryId int NOT NULL,
    sellerId int NOT NULL,
    price float NOT NULL CHECK (price > 0 ),
    deleted boolean DEFAULT FALSE,
    description text,
    imgsrc text,
    PRIMARY KEY (id)
);	
ALTER TABLE products ADD CONSTRAINT FK_productCategory FOREIGN KEY (categoryId) REFERENCES categories(id);
ALTER TABLE products ADD CONSTRAINT FK_productSeller FOREIGN KEY (sellerId) REFERENCES users(id);

CREATE INDEX prodcat_idx ON products (`categoryId`);

    delimiter $$
									
    CREATE TRIGGER sellerIsTrulyASeller 
    BEFORE INSERT
    ON products 
    FOR EACH ROW
            BEGIN
			    declare occorrenze INT;
			    SELECT count(*) INTO occorrenze FROM users where users.id = NEW.sellerId and users.type = "SELLER";
			    IF occorrenze = 0		
			    THEN
			           SET @s = 'sellerId deve essere associato a un account venditore !';
                       SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @s;
				END IF;
           END $$
    delimiter ;		




create table if not exists options (	
    productId int NOT NULL,
    name varchar(32) NOT NULL CHECK(CHAR_LENGTH(name) > 0),
    description text,
    price float NOT NULL,
    required boolean NOT NULL,
    `type` varchar(16) CHECK( `type` in ('INTRANGE','FLOATRANGE', 'FILEVALUE','TEXTVALUE','VALUE')) ,
    PRIMARY KEY (productId, name)
);
ALTER TABLE options ADD CONSTRAINT FK_optionProduct FOREIGN KEY (productId) REFERENCES products(id);

create table if not exists valueOptions (		
    productId int NOT NULL,
    name varchar(32) NOT NULL CHECK(CHAR_LENGTH(name) > 0),
    `value` varchar(32) NOT NULL CHECK(CHAR_LENGTH(`value`) > 0),
    price float CHECK(price >= 0 ),
    PRIMARY KEY (productId, name, `value`)
);
ALTER TABLE valueOptions ADD CONSTRAINT FK_valueOptionIdName FOREIGN KEY (productId, name) REFERENCES options(productId, name);

								   

create table if not exists intRangeOptions ( 
    productId int NOT NULL,
    name varchar(32) NOT NULL CHECK(CHAR_LENGTH(name) > 0),
    minimum int,
    maximum int,
    PRIMARY KEY (productId, name)
);
ALTER TABLE intRangeOptions ADD CONSTRAINT FK_intRangeOptionIdName FOREIGN KEY (productId, name) REFERENCES options(productId, name);

									
										
create table if not exists floatRangeOptions ( 
    productId int NOT NULL,
    name varchar(32) NOT NULL CHECK(CHAR_LENGTH(name) > 0),
    minimum float,
    maximum float,
    PRIMARY KEY (productId, name)
);
ALTER TABLE floatRangeOptions ADD CONSTRAINT FK_floatRangeOptionIdName FOREIGN KEY (productId, name) REFERENCES options(productId, name);

								
									

CREATE TABLE IF NOT EXISTS resetPasswordTokens (
    id INT NOT NULL AUTO_INCREMENT,
    userId INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (userId)
);
ALTER TABLE resetPasswordTokens ADD CONSTRAINT FK_resetPasswordTokenUser FOREIGN KEY (userId) REFERENCES users(id);

SET time_zone='+00:00';
/*
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

*/