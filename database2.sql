create table if not exists orders (id int NOT NULL AUTO_INCREMENT,
                                  clientId int NOT NULL,
    sellerId int NOT NULL,
    `date` dateTime NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    price float NOT NULL CHECK (price > 0),
    status varchar(32) NOT NULL DEFAULT "ATTESAPAGAMENTO" CHECK (status IN ("ATTESAPAGAMENTO","ACCETTATO","SPEDITO","ARRIVATO_STAZIONE")),
    PRIMARY KEY (id)
);

    delimiter $$
									
    CREATE TRIGGER clientIsNotAUserOrVenditoreIsNotASeller
    BEFORE INSERT
    ON orders 
    FOR EACH ROW
            BEGIN
			    declare occorrenze INT;
                   declare occorrenze2 INT;
			    SELECT count(*) INTO occorrenze FROM users where users.id = NEW.clientId and users.`type` = "USER";
			    IF occorrenze = 0		
			    THEN
			           SET @s = 'clientId deve essere associato a un account cliente !';
                       SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @s;
				END IF;
                
			            SELECT count(*) INTO occorrenze2 FROM users where users.id = NEW.sellerId and users.`type` = "SELLER";
			            IF occorrenze2 = 0		
			            THEN
			                SET @s = 'sellerId deve essere associato a un account venditore !';
                    SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @s;
				          END IF;

           END $$
    delimiter ;
				


					
create table if not exists orderPaymentTokens (
    id int NOT NULL AUTO_INCREMENT,
    orderId int NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (orderId)
);	
ALTER TABLE orderPaymentTokens ADD CONSTRAINT FK_orderPaymentTokensOrder FOREIGN KEY (orderId) REFERENCES orders(id);

create table if not exists detailOptions (
    id int NOT NULL AUTO_INCREMENT,
    orderId int NOT NULL, 
    detailIndex int NOT NULL,
    name varchar(32) NOT NULL CHECK(CHAR_LENGTH(name) > 0),
    PRIMARY KEY (id)
);
												 												 
create table if not exists orderDetails (
    id int NOT NULL AUTO_INCREMENT,
    orderId int NOT NULL,
    detailIndex int NOT NULL,
    productId int NOT NULL,
    quantity int CHECK ( quantity > 0 ),
    PRIMARY KEY(id),
    UNIQUE(orderId,detailIndex)
);

ALTER TABLE orderDetails ADD CONSTRAINT FK_orderDetailsOrder FOREIGN KEY (orderId) REFERENCES orders(id);
ALTER TABLE orderDetails ADD CONSTRAINT FK_orderDetailsProduct FOREIGN KEY (productId) REFERENCES products(id);

										   
    delimiter $$

    CREATE TRIGGER dettaglioProdottoRimossoOAltroVenditore
    BEFORE INSERT
    ON orderDetails
    FOR EACH ROW
    BEGIN
		    declare occorrenze INT;
            declare occorrenze2 INT;
		    SELECT count(*) INTO occorrenze FROM products 
				where products.id = new.productId
				AND products.deleted = TRUE;
		    IF occorrenze != 0		
		    THEN
                SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'si sta inserendo l acquisto di un prodotto fuori vendita !';
			END IF;
            SELECT count(*) INTO occorrenze2 FROM products 
				where products.id = new.productId 
		        AND products.sellerId NOT IN (SELECT sellerId FROM orders WHERE orders.id = new.orderId);
		    IF occorrenze2 != 0		
            THEN
                SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'il prodotto si riferisce a un venditore diverso da quello associato all ordine !';
	   		END IF; 
    END $$
	
	delimiter ;
	
    delimiter $$
	CREATE TRIGGER setDetailIndex
    BEFORE INSERT
    ON orderDetails
    FOR EACH ROW
            BEGIN
			    declare occorrenze INT;
			    SELECT count(*) INTO occorrenze FROM orderDetails 
				where orderDetails.orderId = new.orderId;
			    IF occorrenze = 0		
			    THEN
				SET new.detailIndex = 0;
				ELSE
				SET new.detailIndex = occorrenze ;
				END IF;
           END $$
    delimiter ;
	
	DROP PROCEDURE IF EXISTS insertOrderDetail;
    
	delimiter $$
	CREATE PROCEDURE insertOrderDetail(idOrdine_ int, idProdotto_ int, quantità_ int, opzioni_ text, valori_ text)

		BEGIN
			START TRANSACTION;
			DROP TEMPORARY TABLE IF EXISTS _dettaglio; DROP TEMPORARY TABLE IF EXISTS _opzioniNomi; DROP TEMPORARY TABLE IF EXISTS _opzioniValori; DROP TEMPORARY TABLE IF EXISTS _opzioniFornite;
			
			CREATE TEMPORARY TABLE _dettaglio(idOrdine INT NOT NULL, idProdotto INT NOT NULL, indiceDettaglio INT DEFAULT NULL);
			INSERT INTO _dettaglio (idOrdine, idProdotto) VALUES (idOrdine_, idProdotto_);
		/*tablella per opzioni_ */
			CREATE TEMPORARY TABLE _opzioniNomi(id INT NOT NULL AUTO_INCREMENT, nome VARCHAR(64) NOT NULL CHECK(CHAR_LENGTH(nome) > 0) , PRIMARY KEY(id));
			IF CHAR_LENGTH(opzioni_) <> 0
			THEN
				SET @query = CONCAT("INSERT INTO _opzioniNomi (nome) VALUES ", opzioni_);
				PREPARE stmt1 FROM @query; EXECUTE stmt1; DEALLOCATE PREPARE stmt1;	
			END IF;
		/*tablella per valori_ */
			CREATE TEMPORARY TABLE _opzioniValori(id INT NOT NULL AUTO_INCREMENT, valore VARCHAR(64) NOT NULL CHECK(CHAR_LENGTH(valore) > 0) , PRIMARY KEY(id));
			IF CHAR_LENGTH(valori_) <> 0
			THEN
				SET @query = CONCAT("INSERT INTO _opzioniValori (valore) VALUES ", valori_);			
				PREPARE stmt1 FROM @query; EXECUTE stmt1; DEALLOCATE PREPARE stmt1;
			END IF;
		/*tabella per ricombinare nomi + valori */
			CREATE TEMPORARY TABLE _opzioniFornite (id INT NOT NULL AUTO_INCREMENT, nome VARCHAR(64) NOT NULL CHECK(CHAR_LENGTH(nome) > 0), valore text NOT NULL CHECK(CHAR_LENGTH(valore) > 0), tipo VARCHAR(64) DEFAULT NULL, idOpzioneDettaglio INT DEFAULT NULL, PRIMARY KEY(id));
			INSERT INTO _opzioniFornite (nome, valore) SELECT nome, valore FROM _opzioniNomi JOIN _opzioniValori ON _opzioniNomi.id = _opzioniValori.id;

		/*vediamo se le opzioni fornite esistono*/		
			UPDATE _opzioniFornite SET tipo = ( SELECT `type` FROM options WHERE options.name = _opzioniFornite.nome AND options.productId = idProdotto_ );
			SELECT COUNT(*) INTO @NUMERO_ERRORI FROM _opzioniFornite WHERE tipo = NULL ;
			IF @NUMERO_ERRORI = 0
			THEN
				/*vediamo se sono state fornite tutte le opzioni obbligatorie*/
				SELECT COUNT(*) INTO @NUMERO_OPZIONI_OBBLIGATORIE_FORNITE 
					FROM options 
					WHERE productId = idProdotto_ 
					AND required = TRUE 
					AND name IN	(SELECT DISTINCT nome FROM _opzioniFornite) ;
				SELECT COUNT(*) INTO @NUMERO_OPZIONI_OBBLIGATORIE 
                	FROM options 
					WHERE productId = idProdotto_ AND required = TRUE;
				IF @NUMERO_OPZIONI_OBBLIGATORIE = @NUMERO_OPZIONI_OBBLIGATORIE_FORNITE
				THEN
				/*il check della validità dei valori lo fanno le tabelle*/
					INSERT INTO orderDetails (orderId, productId, quantity) VALUES (idOrdine_, idProdotto_, quantità_);
				/*get newly created id*/
				    SET @idDettaglio = LAST_INSERT_ID();
					
					UPDATE _dettaglio SET indiceDettaglio = (SELECT detailIndex FROM orderDetails WHERE id = @idDettaglio);
					
					INSERT INTO detailOptions (orderId, detailIndex, name) 
                    SELECT /*DISTINCT ?*/idOrdine, indiceDettaglio, nome  
                    FROM _opzioniFornite JOIN _dettaglio;
					UPDATE _opzioniFornite
					SET idOpzioneDettaglio = 
						(SELECT id FROM detailOptions 
							WHERE detailOptions.name = _opzioniFornite.nome 
							AND detailOptions.orderId = idOrdine_ 
							AND detailOptions.detailIndex = ( SELECT detailIndex FROM orderDetails WHERE id = @idDettaglio	)
						);
					INSERT INTO intDetailOptions (detailOptionId, `value`) SELECT idOpzioneDettaglio , valore FROM _opzioniFornite WHERE tipo = 'INTRANGE';
					INSERT INTO floatDetailOptions (detailOptionId, `value`) SELECT idOpzioneDettaglio , valore FROM _opzioniFornite WHERE tipo = 'FLOATRANGE';
					INSERT INTO valueDetailOptions (detailOptionId, `value`) SELECT idOpzioneDettaglio , valore FROM _opzioniFornite WHERE tipo ='VALUE';
					INSERT INTO fileDetailOptions (detailOptionId, fileName) SELECT idOpzioneDettaglio , valore FROM _opzioniFornite WHERE tipo = 'FILEVALUE';
					INSERT INTO textDetailOptions (detailOptionId, `value`) SELECT idOpzioneDettaglio , valore FROM _opzioniFornite WHERE tipo = 'TEXTVALUE';
					
					COMMIT;
				ELSE SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'non sono state fornite tutte le opzioni obbligatorie !'; ROLLBACK;
				END IF;		
				SELECT * FROM _opzioniFornite;
			ELSE SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'non tutte le opzioni fornite esistono !'; ROLLBACK;
			END IF;
		END$$
	delimiter ;
	

															
create table if not exists valueDetailOptions(
    id int NOT NULL AUTO_INCREMENT,
    detailOptionId int NOT NULL,
    `value` VARCHAR(32) NOT NULL CHECK(CHAR_LENGTH(`value`) > 0),
    PRIMARY KEY (id)
);
create table if not exists fileDetailOptions(
    id int NOT NULL AUTO_INCREMENT,
    detailOptionId int NOT NULL,
    fileName VARCHAR(32) NOT NULL CHECK(CHAR_LENGTH(fileName) > 0),
    PRIMARY KEY (id)
);
																		  
create table if not exists textDetailOptions(
    id int NOT NULL AUTO_INCREMENT,
    detailOptionId int NOT NULL,
    `value` VARCHAR(32) NOT NULL CHECK(CHAR_LENGTH(`value`) > 0),
    PRIMARY KEY (id)
);
create table if not exists intDetailOptions(
    id int NOT NULL AUTO_INCREMENT,
    detailOptionId int NOT NULL,
    `value` int NOT NULL,
    PRIMARY KEY (id)
);

    delimiter $$

    CREATE TRIGGER opzioniDettaglioIntInRange
    BEFORE INSERT
    ON intDetailOptions
    FOR EACH ROW
            BEGIN
				declare nomeOpzione VARCHAR(64); 
			    declare low INT;
				declare high INT; 
				declare indiceDettaglio_ INT; 
				DECLARE idOrdine_ INT;
				DECLARE idProdotto_ INT ;
				SELECT detailIndex , orderId INTO indiceDettaglio_ , idOrdine_ FROM detailOptions WHERE id = new.detailOptionId ;
				SELECT orderId INTO idProdotto_  FROM orderDetails WHERE orderId = idOrdine_ AND detailIndex  = indiceDettaglio_ ;
				SELECT name INTO nomeOpzione FROM detailOptions WHERE id = new.detailOptionId;
			    SELECT minimum, maximum  INTO low, high FROM intRangeOptions WHERE intRangeOptions.name = nomeOpzione AND intRangeOptions.productId = idProdotto_ ;    /*esite la tale opzione per il prodotto*/
			    IF new.`value` < low OR new.`value` > high
			    THEN
			           SET @s = 'si sta inserendo un valore esterno al range previsto per la tale opzione!';
                       SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @s;
				END IF;
           END $$
    delimiter ;
	
create table if not exists floatDetailOptions(id int NOT NULL AUTO_INCREMENT,
																			  detailOptionId int NOT NULL,
																			  `value` float NOT NULL,
																			 PRIMARY KEY (id)
																			);
																			
	    delimiter $$																		

    CREATE TRIGGER opzioniDettaglioFloatInRange
    BEFORE INSERT
    ON floatDetailOptions
    FOR EACH ROW
            BEGIN
				declare nomeOpzione VARCHAR(64); 
			    declare low FLOAT;
				declare high FLOAT; 
				declare indiceDettaglio_ INT; 
				DECLARE idOrdine_ INT;
				DECLARE idProdotto_ INT ;
				SELECT detailIndex , orderId INTO indiceDettaglio_ , idOrdine_ FROM detailOptions WHERE id = new.detailOptionId ;
				SELECT orderId INTO idProdotto_  FROM orderDetails WHERE orderId = idOrdine_ AND detailIndex  = indiceDettaglio_ ;
				SELECT name INTO nomeOpzione FROM detailOptions WHERE id = new.detailOptionId;
			    SELECT minimum, maximum  INTO low, high FROM floatRangeOptions WHERE floatRangeOptions.name = nomeOpzione AND floatRangeOptions.productId = idProdotto_ ;    /*esite la tale opzione per il prodotto*/
			    IF new.`value` < low OR new.`value` > high
			    THEN
			           SET @s = 'si sta inserendo un valore esterno al range previsto per la tale opzione!';
                       SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @s;
				END IF;
           END $$
    delimiter ;

create table if not exists reviews (
    productId int NOT NULL,
    userId int,
    rating int,
    `text` varchar(256),
    hidden boolean default false,
    PRIMARY KEY (productId,userId)
);

ALTER TABLE reviews ADD CONSTRAINT FK_reviewsProduct FOREIGN KEY (productId) REFERENCES products(id);
ALTER TABLE reviews ADD CONSTRAINT FK_reviewsUser FOREIGN KEY (userId) REFERENCES users(id);

    delimiter $$

    CREATE TRIGGER utenteNonHaDettaglioAssociato
    BEFORE INSERT
    ON reviews 
    FOR EACH ROW
            BEGIN
			    declare occorrenze INT;
			    SELECT count(*) INTO occorrenze FROM orderDetails
				where orderDetails.productId = new.productId 
				AND orderDetails.orderId IN (SELECT id FROM orders WHERE orders.clientId = new.userId);
			    IF occorrenze = 0		
			    THEN
			           SET @s = 'il cliente deve aver effettivamente acquistato il prodotto !';
                       SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @s;
				END IF;
           END $$
    delimiter ;


create table if not exists notifications (
    id INT NOT NULL AUTO_INCREMENT, 
    userId int NOT NULL, 
    targetId int NOT NULL,
    targetType varchar(64) NOT NULL CHECK ( targetType IN ("UTENTI", "PRODOTTI", "RECENSIONI", "ORDINI")), 
    `date` dateTime NOT NULL DEFAULT CURRENT_TIMESTAMP(), 
    message varchar(256) NOT NULL,
    `read` boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY (id)
    /*non ho più check su targetId*/
);	

ALTER TABLE notifications ADD CONSTRAINT FK_notificationsUser FOREIGN KEY (userId) REFERENCES users(id);
								  

    delimiter $$

    CREATE TRIGGER creaNotificaOrdine
    AFTER UPDATE
    ON orders 
    FOR EACH ROW
    	BEGIN
			    IF old.status <> new.status AND new.status <> "ATTESAPAGAMENTO"
				THEN 
                	IF new.status = "ACCETTATO"
						THEN SET @destinatario = new.sellerId ;               
                   	 ELSE SET @destinatario = new.clientId ;
					 END IF;
				insert into notifications (userId, targetId, targetType , message ) VALUES (@destinatario, new.id, "ORDINI" , new.status ) ;
				END IF;
           END $$
    delimiter ;
