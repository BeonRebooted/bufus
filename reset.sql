-- please check https://stackoverflow.com/questions/2681869/resetting-auto-increment-is-taking-a-long-time-in-mysql

ALTER TABLE reviews DROP FOREIGN KEY FK_reviewsProduct; 
ALTER TABLE reviews DROP FOREIGN KEY FK_reviewsUser; 
ALTER TABLE resetpasswordtokens DROP FOREIGN KEY FK_resetPasswordTokenUser;
ALTER TABLE orderpaymenttokens DROP FOREIGN KEY FK_orderPaymentTokensOrder;
ALTER TABLE notifications DROP FOREIGN KEY FK_notificationsUser;
ALTER TABLE orderdetails DROP FOREIGN KEY FK_orderDetailsOrder;
ALTER TABLE orderdetails DROP FOREIGN KEY FK_orderDetailsProduct;
ALTER TABLE intrangeoptions DROP FOREIGN KEY FK_intRangeOptionIdName;
ALTER TABLE floatrangeoptions DROP FOREIGN KEY FK_floatRangeOptionIdName;
ALTER TABLE valueoptions DROP FOREIGN KEY FK_valueOptionIdName;
ALTER TABLE options DROP FOREIGN KEY FK_optionProduct;
ALTER TABLE products DROP FOREIGN KEY FK_productCategory;
ALTER TABLE products DROP FOREIGN KEY FK_productSeller;

TRUNCATE TABLE `reviews`;

ALTER TABLE  `reviews` AUTO_INCREMENT = 1;

TRUNCATE TABLE `resetpasswordtokens`;
ALTER TABLE  `resetpasswordtokens` AUTO_INCREMENT = 1;

TRUNCATE TABLE `orderpaymenttokens`;
ALTER TABLE  `orderpaymenttokens` AUTO_INCREMENT = 1;

TRUNCATE TABLE `notifications`;
ALTER TABLE  `notifications` AUTO_INCREMENT = 1;

TRUNCATE TABLE `detailoptions`;
ALTER TABLE  `detailoptions` AUTO_INCREMENT = 1;

TRUNCATE TABLE `filedetailoptions`;
ALTER TABLE  `filedetailoptions` AUTO_INCREMENT = 1;

TRUNCATE TABLE `floatdetailoptions`;
ALTER TABLE  `floatdetailoptions` AUTO_INCREMENT = 1;

TRUNCATE TABLE `intdetailoptions`;
ALTER TABLE  `intdetailoptions` AUTO_INCREMENT = 1;

TRUNCATE TABLE `textdetailoptions`;
ALTER TABLE  `textdetailoptions` AUTO_INCREMENT = 1;

TRUNCATE TABLE `valuedetailoptions`;
ALTER TABLE  `valuedetailoptions` AUTO_INCREMENT = 1;

TRUNCATE TABLE `orderdetails`;
ALTER TABLE  `orderdetails` AUTO_INCREMENT = 1;

TRUNCATE TABLE `orders`;
ALTER TABLE  `orders` AUTO_INCREMENT = 1;

TRUNCATE TABLE `intrangeoptions`;

TRUNCATE TABLE `floatrangeoptions`;
TRUNCATE TABLE `valueoptions`;
TRUNCATE TABLE `options`;

TRUNCATE TABLE `products`;
ALTER TABLE  `products` AUTO_INCREMENT = 1;

TRUNCATE TABLE `categories`;
ALTER TABLE  `categories` AUTO_INCREMENT = 1;

TRUNCATE TABLE `users`;
ALTER TABLE  `users` AUTO_INCREMENT = 1;



ALTER TABLE reviews ADD CONSTRAINT FK_reviewsProduct FOREIGN KEY (productId) REFERENCES products(id);
ALTER TABLE reviews ADD CONSTRAINT FK_reviewsUser FOREIGN KEY (userId) REFERENCES users(id);
ALTER TABLE resetpasswordtokens ADD CONSTRAINT FK_resetPasswordTokenUser FOREIGN KEY (userId) REFERENCES users(id);
ALTER TABLE orderpaymenttokens ADD CONSTRAINT FK_orderPaymentTokensOrder FOREIGN KEY (orderId) REFERENCES orders(id);
ALTER TABLE notifications ADD CONSTRAINT FK_notificationsUser FOREIGN KEY (userId) REFERENCES users(id);
ALTER TABLE orderdetails ADD CONSTRAINT FK_orderDetailsOrder FOREIGN KEY (orderId) REFERENCES orders(id);
ALTER TABLE orderdetails ADD CONSTRAINT FK_orderDetailsProduct FOREIGN KEY (productId) REFERENCES products(id);
ALTER TABLE intrangeoptions ADD CONSTRAINT FK_intRangeOptionIdName FOREIGN KEY (productId, name) REFERENCES options(productId, name);
ALTER TABLE floatrangeoptions ADD CONSTRAINT FK_floatRangeOptionIdName FOREIGN KEY (productId, name) REFERENCES options(productId, name);
ALTER TABLE valueoptions ADD CONSTRAINT FK_valueOptionIdName FOREIGN KEY (productId, name) REFERENCES options(productId, name);
ALTER TABLE options ADD CONSTRAINT FK_optionProduct FOREIGN KEY (productId) REFERENCES products(id);
ALTER TABLE products ADD CONSTRAINT FK_productCategory FOREIGN KEY (categoryId) REFERENCES categories(id);
ALTER TABLE products ADD CONSTRAINT FK_productSeller FOREIGN KEY (sellerId) REFERENCES users(id);
