<div id="sideCart" class=" m-0" aria-label="cart view" aria-hidden="true">
    <section class="row" role="presentation">
        <button id="sideCartClose" class="btn" onclick="cartViewObject.close()"> &times;</button>
    </section>
	<br role="presentation">
	<br role="presentation">
	<section class="row no-gutters" aria-label="il tuo carrello">
       <ul id="sideCartList" class="list-group mx-auto mt-3">
	   </ul>	
	</section>
	<section class="row text-center" role="presentation">
	    <a id="toCheckout" class="btn btn-xl text-light bg-dark"><h3>CHECKOUT</h3></a>
	</section>
</div>

<script>
    cartViewObject = {
		
		isOpen : false,
		cartButton : document.getElementById("cartButton"),
		
		toggle : function(){  if(cartViewObject.isOpen){
				              cartViewObject.close();
			              }else{
							  
							  cartViewObject.open();
							  cartViewObject.draw();
						  }
		               },
					   
		open :  function(){ cartView = $("#sideCart")[0];
							cartView.style.left = "0%";
							cartView.classList.remove("d-none");
							mainView = $("main")[0];  
							mainView.style.overflow = "hidden";
							mainView.style.height = "90vh";
							cartButton = $("#cartButton")[0];
							cartButton.classList.remove("button_idle");
							cartButton.classList.remove("bg-light");
							cartButton.classList.add("button_pushed");				
							cartButton.classList.add("bg-dark");
							cartViewObject.isOpen = true;
							cartView.ariaHidden = false;
							views.active.ariaHidden = true;
						  },
					  
		close :  function(){ cartView = $("#sideCart")[0];
		                cartView.style.left = "-100vw";
						cartView.addEventListener('animationend', (e) => {
								e.target.classList.add("d-none");
							 });
						mainView = $("main")[0];  
			            mainView.style.overflow = "auto";
			            mainView.style.height = "";
						cartButton = $("#cartButton")[0];
			            cartButton.classList.remove("button_pushed");
		                cartButton.classList.remove("bg-dark");
			            cartButton.classList.add("button_idle");				
		                cartButton.classList.add("bg-light");
                        cartViewObject.isOpen = false;	
						cartView.ariaHidden = true;
						if(views.active)
							views.active.ariaHidden = false;						
		              },
					  
		init : () => {
						$("#sideCartList")[0].innerHTML = "",
						$("#toCheckout")[0].href = URLroot() + "ui_2.php/checkout";
					 },    //resetta la vista al suo stato blank
		
		draw : async () => { cartViewObject.init();
				       products = JSON.parse(sessionStorage.getItem("cart"));
		               if(products != null && products.length > 0){
			               for(product of products){
 				               await ultraFetch(URLroot() + "service.php/products/" + product.id + "/", "GET")
							   .then(result => result.json())
							   .then(data => {
													data.options = product.options;
													data.quantity = product.quantity;
													document.getElementById("sideCartList")
														.appendChild(  cartLI(data, document.getElementById("sideCartList").childNodes.length)  );
											 });
			                }
						    document.getElementById("toCheckout").classList.replace("text-dark","text-light");
							document.getElementById("toCheckout").classList.replace("bg-transparent","bg-dark");
		                }else{
							//sets OFF state;
							document.getElementById("toCheckout").classList.replace("text-light","text-dark");
							document.getElementById("toCheckout").classList.replace("bg-dark","bg-transparent");
						}
		             }   //disegna la vista secondo lo stato corrente
	};
		


	
	function cartLIclose(elemNumber, product){
		el  = document.getElementById("cartLI" + elemNumber);
		onceAnimationEnd(el, "cartLIClose").then(() => { 
		    removeFromCart(product);
			el.parentNode.removeChild(el);
			if(! $("#checkoutview")[0].classList.contains("d-none")){
				checkoutViewObject.draw();
			}
		});
	}
	



	function cartLI(product, elemNumber){
		console.log("cartLI")
		console.log(product)
		el = document.createElement("LI");
		el.id = "cartLI" + elemNumber;
		el.classList.add("container-fluid");
		el.classList.add("rounded");
		el.classList.add("bg-light");
		el.classList.add("p-2");
		el.classList.add("my-2");
		el.classList.add("card");
				
        el.innerHTML = `
			<div class="row no-gutters">
		        <div class="col-3 my-auto">
			        <img class="img-fluid m-0" style="max-height: 20vh;" src="${URLbase()}upload/products/${product.imgsrc}" aria-label="immagine prodotto">
			    </div>
			    <div class="col mx-2">
				    <div class="row no-gutters">
					    <div class="col">
							<h4><a href="../products/${product.id}/" tabindex="0">${product.name}</a></h4>
							</br>
							<a href="../users/${product.sellerId}/products/">${product.sellerName}</a>
							<ol class="pl-0" style="list-style: none"></ol>
						</div>
						<p class="col-2">${product.quantity}pz</p>
						<div class="col-2 col-sm-1 text-center">
						<button class="btn btnClose">&times;</button>
						<p class="slimLIproductPrice mb-0">${product.price}$</p>
						</div>
					</div>
				</div>
		    </div>
		`;
		for(option of product.options){
			li = document.createElement("LI");
			if(option.tipo == "FILEVALUE"){
				li.innerHTML = `<span class="badge badge-pill badge-primary">${option.name}</span>${option.filename}`
			}else{
				
			li.innerHTML = `<span class="badge badge-pill badge-primary">${option.name}</span>${option.value}`

			}
			el.getElementsByTagName("ol")[0].appendChild(li);	
		}
		el.getElementsByTagName("button")[0].onclick = () => { cartLIclose( elemNumber, product); };
        return el;	
	}
	
</script>

