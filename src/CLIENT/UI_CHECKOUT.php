<section id="checkoutview" class="d-none bg-white container-fluid py-md-2" role="tabpanel" aria-label="checkout view">
	<div class="row">
		<div class="col-1 d-none d-md-block">
		</div>
		<div class="col-12 col-md-4 col-xl-2">
			<div class="row">
			    <div class="col"><p id="checkoutPriceLabel" style="font-size: 20px">Totale:</p></div><div class="col text-right"><p id="checkoutTotal" aria-labelledby="checkoutPriceLabel" style="font-size: 20px">AEEE$</p></div>
			</div>
			<div class="row text-center">
			    <div class="col" role="presentation">
				    <button id="checkoutSubmitButton" class="btn btn-lg bg-dark text-white">Procedi al pagamento</button>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6 col-xl-8 py-3">
			<ul id="checkoutList" class="p-0" style="list-style: none" aria-label="i tuoi prodotti">
			</ul>
		</div>
		<div class="col-1 d-none d-md-block">
		</div>
	</div>
</section>		
<script>

    checkoutViewObject = {
		
		isOpen : false,
					   
		open :  function(){ checkoutView = $("#checkoutview")[0];
							checkoutView.classList.remove("d-none");
							onceAnimationEnd(checkoutView, "swipeIn")
							.then(()=>{
								mainView = $("main")[0];  
								mainView.style.overflow = "auto";
								views.active = checkoutViewObject;
								checkoutViewObject.isOpen = true;	
								checkoutView.ariaHidden = false;
							});
						  },
					  
		close : async function(){
			if(checkoutViewObject.isOpen){
				return new Promise(resolve => {
				    checkoutView = $("#checkoutview")[0];
					mainView = $("main")[0];  
					mainView.style.overflow = "auto";
					mainView.style.height = "";
					onceAnimationEnd(checkoutView, "swipeOut")
					.then(()=>{
						 checkoutView.classList.add("d-none");
						 checkoutView.classList.remove("swipeOut");
						 checkoutViewObject.isOpen = false;
						 checkoutView.ariaHidden = true;
						 views.active = null;
						 resolve();	
					 });	
				});							
			}else{
				return new Promise(resolve => resolve());
			}					
		},
					  
		init : function(){ $("#checkoutList")[0].innerHTML = "";
		                   $("#checkoutTotal")[0].innerHTML = "0$";
						   document.getElementById("checkoutSubmitButton").onclick = postOrder;
						   
            		 },
					 
		draw : function(){ checkoutViewObject.init();
		                   products = JSON.parse(sessionStorage.getItem("cart"));
		                   if(products != null && products.length > 0){
								for(i = 0; i < products.length; i++){
									document.getElementById("checkoutList").appendChild(checkoutLI(products[i], $("#checkoutList")[0].childNodes.length));
											//viene ripetuto a ogni call
									
								}
								document.getElementById("checkoutTotal").innerText = cartTotal() + "$";
						   }
		}   //disegna la vista secondo lo stato corrente
	};
			
	async function postOrder(){
	    products = JSON.parse(sessionStorage.getItem("cart"));
		if(products != null && products.length > 0){
			total = 0;

			fd = new FormData();
			for(product of products){
				cleanOptions = [];
				for(option of product.options){
					if(option.value != ""){
						cleanOptions.push(option);
					}
				}
				product.options = cleanOptions;
			}
			fd.append("details", JSON.stringify(products));
			console.log(products);
		    ultraFetch(URLroot() + "service.php/orders/", "POST", "Basic " + sessionStorage.getItem("Authentication"), fd)
			.then(data => data.json()
			                   .then(links => {
								   bodi = document.createElement("ul");
								   bodi.classList.add("pl-0");
								   bodi.style = "list-style : none;";
                                   showAModal("ordine inviato", bodi, null, null);
								   
								   for(link in links)
									   window.open(URLroot() + links[link])
								   })
			     );
		}	
	}
	
	function checkoutLIclose(elemNumber,product){
		el  = document.getElementById("checkoutLI" + elemNumber);
		onceAnimationEnd(el, "checkoutLIClose")
		.then(() =>{
			removeFromCart(product);
			el.parentNode.removeChild(el);
			});
	}

	function checkoutLI(product, elemNumber){
		el = document.createElement("LI");
		elementId = "checkoutLI" + elemNumber;
		el.id = elementId;
		classes = [ "container-fluid" , "rounded" , "bg-light" , "p-2" , "mb-2" , "card" ];
		for(cls of classes){
			el.classList.add(cls);
		}
        el.innerHTML = `
			<div class="row no-gutters">
		        <div class="col-3 my-auto">
			        <img class="img-fluid m-0" style="max-height: 20vh;" src="${URLbase()}upload/products/${product.imgsrc}" aria-label="immagine prodotto">
			    </div>
			    <div class="col mx-2">
				    <div class="row no-gutters">
					    <div class="col">
							<h5>
								<a href="../products/${product.id}/" tabindex="0">${product.name}</a>
								<span>${product.quantity}pz</span>
							</h5>
							</br>
							<a href="../users/${product.sellerId}/products/">${product.sellerName}</a>
							<ol class="pl-0" style="list-style: none"></ol>
						</div>
						<div class="col-3 text-center">
						<button class="btn btnClose">&times;</button>
						<p class="slimLIproductPrice mb-0">${product.price}$</p>
						</div>
					</div>
				</div>
		    </div>
		`;
		for(option of product.options){
			li = document.createElement("LI");
			if(option.type == "FILEVALUE"){
				li.innerHTML = `<span class="badge badge-pill badge-primary">${option.name}</span>${option.filename}`
			}else{
				
			li.innerHTML = `<span class="badge badge-pill badge-primary">${option.name}</span>${option.value}`

			}
			el.getElementsByTagName("ol")[0].appendChild(li);	
		}
		el.getElementsByClassName("btnClose")[0].onclick = () => { checkoutLIclose(elemNumber, product); }; 
        return el;	
	}
	
	
</script>