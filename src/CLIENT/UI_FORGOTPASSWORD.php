
	<section id="forgotpasswordview" class="d-none bg-white" style="z-index: 10000; position: absolute; top: 0; width: 100vw;" aria-label="forgot password view">
	    <div class="container-fluid">
		    <div class="row">
			    <div class="col-3 d-none d-md-block">
	    		</div>
		        <div class="col">
				    <h1 class="mt-5 text-center">Inserisci l'email associata all'account</h1>
					<p class="mb-5">invieremo un link per reimpostare la password</p>
                    <form class="mt-4">
					    <label for="forgotPasswordEmail">Email</label>
					    <input type="email" class="form-control" id="forgotPasswordEmail" name="email">
						<button id="forgotPasswordSend" class="btn bg-primary text-light mt-4">invia</button>
					</form>
					<div  class="text-center">
					    <img id="forgotPasswordResult" class="img-fluid rounded-circle" src="../upload/jodorowski300_deepYellow.png" alt="MAIL SEND PENDING">
					    <p id="httpOutput">in realtà sarà la spunta che appare dopo l'invio della mail</p>
					</div>
					
			    </div>
				<div class="col-3 d-none d-md-block">
	    		</div>
			</div>
		
		</div>
		<script>
   forgotPasswordViewObject = {
		isOpen : false,
		
		open :  function(){
							forgotPasswordView = $("#forgotpasswordview")[0];
							mainView = $("main")[0];  
							forgotPasswordView.classList.remove("d-none");
							onceAnimationEnd(forgotPasswordView, "swipeIn")
							.then(()=>{
								mainView.style.overflow = "hidden";
								views.active = forgotPasswordViewObject;
								forgotPasswordViewObject.isOpen = true;
								forgotPasswordView.ariaHidden = false;
							});
						  },
					  
		close : async function(){
			if(forgotPasswordViewObject.isOpen){
			    return new Promise(resolve => {
				   forgotPasswordView = $("#forgotpasswordview")[0];
					mainView = $("main")[0];  
					mainView.style.overflow = "auto";
					mainView.style.height = "";
					 onceAnimationEnd(forgotPasswordView, "swipeOut")
				     .then(()=>{
						 forgotPasswordView.classList.add("d-none");
						 forgotPasswordView.classList.remove("swipeOut");
						 forgotPasswordViewObject.isOpen = false;
						 forgotPasswordView.ariaHidden = true;
						views.active = null;						 
						 resolve();	
					 });	
				});				
			}else{
				return new Promise(resolve => resolve());
			}		
        },
					  
		init : function(){ forgotPasswordView = $("#forgotpasswordview")[0];
			$("#forgotPasswordEmail")[0].value = "";
  			
		},    //resetta la vista al suo stato blank
					 
		draw : function(source){ 
				forgotPasswordViewObject.init();
		}
		
	};
		
		
		
		
		function inviaMail(e){
			e.preventDefault();
			document.getElementById("forgotPasswordEmail").value;
			httpRequest = new XMLHttpRequest();
			httpRequest.onreadystatechange = function(){
        	    if(httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {  
                    document.getElementById("forgotPasswordResult").src = URLbase() + "upload/jodorowski300_deepGreen.png";
					document.getElementById("forgotPasswordResult").alt = "MAIL SEND SUCCESS";
					
                }else if(httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status !== 200){
					document.getElementById("forgotPasswordResult").src = URLbase() + "upload/jodorowski300_deepRed.png";
					document.getElementById("forgotPasswordResult").alt = "MAIL SEND ERROR";
				}
				document.getElementById("httpOutput").innerHTML = httpRequest.response;
            }
            httpRequest.open('POST', URLroot() + 'service.php/forgotPassword/', true);
			formData = new FormData();
            formData.append("email", document.getElementById("forgotPasswordEmail").value);
			httpRequest.send(formData);
		}
		document.getElementById("forgotPasswordSend").onclick = inviaMail;
		</script>
</section>
