<header id="header" class="sticky-top border" aria-label="webapp header">
    <section id="headerMain" class="row no-gutters align-items-center bg-light pt-2"  role="presentation">
		<div class="col align-items-center" role="presentation">
			<a id="homeLink" class="btn" href="DAJAVASCRIPT"><p class="mb-0 h2">BUFUs</p></a>
			<a id="notificationsPill" href="DAJAVASCRIPT" class="badge badge-pill badge-danger d-none" style="font-size: 15px;" aria-label="notifiche"></a>
		</div>
		<div class="col-3 col-sm-2 col-md-1 p-0">
			<button id="cartButton" class="float-right mr-2 btn bg-light d-none" disabled>CART</button>
		</div>
	</section>
	<section id="productsViewHeader" role="search" class="d-none row no-gutters text-center bg-light mb-0">
		<section id="productsViewSearchControls" class="col-12 py-1 my-auto">
			<div class="row no-gutters mb-0">
                     <div class="col-md mx-3 my-1">					
					<label class="sr-only" for="productsSearch">cosa stai cercando?</label>
					<input type="search" id="productsSearch" name="searchquery" class="form-control form-control-sm">
			   </div>
			   <div class="col-md mx-3 my-1">
				   <select id="searchCategory" class="form-control form-control-sm">
				   </select>
			   </div>
			</div>
		</section>
		<section id="productsViewSellerControls" class="col-12 row no-gutters">
			<hr class="col-12 d-md-none mx-1"/>
			<section id="productsToggleOnlyUserView" class="row no-gutters col-12 col-sm-6 px-3 py-2">
				<div class="col">
					<label for="productsToggleOnlyUser" class="float-left form-control form-control-sm mb-0">
						<input type="checkbox" id="productsToggleOnlyUser" class="d-none">I miei prodotti
					</label>
				</div>
			</section>
			<section id="productsCreateProductView" class="col-12 col-sm-6 px-3 py-2">
				<button id="productsCreateProduct" class="btn bg-primary form-control form-control-sm">Aggiungi prodotto</button>
			</section>
		</section>
	</section>
</header>