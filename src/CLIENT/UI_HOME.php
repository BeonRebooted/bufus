<!-- toggles between d-none and d-flex -->            
<section id="homeview" class="d-none bg-white container-fluid align-items-center" style="height: 90vh;" role="tabpanel" aria-label="home view">
    <ol class="row w-100 mx-auto" style="list-style: none">
		<li class="col-6 col-lg-3 my-2"><button id="productsButton" class="btn btn-lg menuBut" aria-hidden="false"><img class="img-fluid" style="max-height: 100%; " src="DA_JAVASCRIPT" alt="DA_JAVASCRIPT"></button></li>
		<li class="col-6 col-lg-3 my-2"><button id="ordersButton" class="btn btn-lg bg-danger menuBut"><img class="img-fluid" style="max-height: 100%;" src="DA_JAVASCRIPT" alt="DA_JAVASCRIPT"></button></li>
		<li class="col-6 col-lg-3 my-2"><button id="extraButton" class="btn btn-lg bg-warning menuBut">I miei </button></li>
		<li class="col-6 col-lg-3 my-2"><button id="settingsButton" class="btn btn-lg bg-success menuBut"><img class="img-fluid" style="max-height: 100%;" src="DA_JAVASCRIPT" alt="DA_JAVASCRIPT"></button></li>							
	</ol>
</section>
			
<script>

    homeViewObject = {
		
		isOpen : false,
					   
		open :  function(){
							homeView = $("#homeview")[0];
							//homeView.style.left = "0%";
							homeView.classList.remove("d-none");
					        homeView.classList.add("d-flex");
							mainView = $("main")[0];  
                            onceAnimationEnd(homeView, "swipeIn")
							.then(()=>{
								mainView.style.overflow = "auto";
							    homeViewObject.isOpen = true;	
								views.active = homeViewObject;	
								homeView.ariaHidden = false;
							});
						  },
					  
		close :  async function(){
			                 if(homeViewObject.isOpen){
							    return new Promise(resolve => {
								    homeView = $("#homeview")[0];
									mainView = $("main")[0];  
									mainView.style.overflow = "auto";
									mainView.style.height = "";
									 onceAnimationEnd(homeView, "swipeOut")
								     .then(()=>{
										 homeView.classList.add("d-none");
										 homeView.classList.remove("d-flex");
										 homeView.classList.remove("swipeOut");
										 homeViewObject.isOpen = false;	
										 views.active = null;
										 homeView.ariaHidden = true;
										 resolve();
									 });	
								 });
							 }else{
								return true;
							 }
		              },
					  
		init : function(){ homeView = $("#homeview")[0];
			               //homeView.style.transition = "left 0.3s";
		       homeview.innerHTML = `<ol class="row w-100 mx-auto pl-0" style="list-style: none" role="tablist" aria-label="sezioni APP">
										<li class="col-6 col-lg-3 my-2" role="tab"><button id="productsButton" class="btn btn-lg menuBut"><img class="img-fluid" style="max-height: 100%;" src="DA_JAVASCRIPT"></button></li>
										<li class="col-6 col-lg-3 my-2" role="tab"><button id="ordersButton" class="btn btn-lg bg-danger menuBut"><img class="img-fluid" style="max-height: 100%;" src="DA_JAVASCRIPT"></button></li>
										<li class="col-6 col-lg-3 my-2" role="tab"><button id="extraButton" class="btn btn-lg bg-warning menuBut">I miei </button></li>
										<li class="col-6 col-lg-3 my-2" role="tab"><button id="settingsButton" class="btn btn-lg bg-success menuBut"><img class="img-fluid" style="max-height: 100%;" src="DA_JAVASCRIPT"></button></li>							
									</ol>`;									
            		     },    //resetta la vista al suo stato blank
					 
		draw : function(source){ homeViewObject.init();
								 userButtons = [
												{	id : "productsButton",	onclick : () => {cambiaStato('products/');},    innerText : "prodotti",		backgroundImage : 'url(\'' + URLbase() + 'upload/icons-xl/tag-black.png\')',	backgroundColor : 'hsl(180, 60%, 70%)'	},
												{	id : "ordersButton",	onclick : () => {cambiaStato('orders/');},	innerText : "ordini",	backgroundImage : 'url(\'' + URLbase() + 'upload/icons-xl/wallet-black.png\')',	backgroundColor : 'hsl(270, 60%, 70%)'	},
												{   id : "extraButton",	onclick : () => {cambiaStato('notifications/');},   innerText : "notifiche",	backgroundImage : 'url(\'' + URLbase() + 'upload/icons-xl/star-black.png\')',	backgroundColor : 'hsl(0, 60%, 70%)'	},
												{   id : "settingsButton",	onclick : () => {cambiaStato('settings/');},	innerText : "impostazioni",    backgroundImage : 'url(\'' + URLbase() + 'upload/icons-xl/gear6-black.png\')',	backgroundColor : 'hsl(90, 60%, 70%)'	}
											   ];
								 sellerButtons = [
												{   id : "productsButton",	onclick : () => {cambiaStato('products/');},	innerText : "prodotti",    backgroundImage : 'url(\'' + URLbase() + 'upload/icons-xl/tag-black.png\')',    backgroundColor : 'hsl(180, 60%, 70%)'	},
												{   id : "ordersButton",	onclick : () => {cambiaStato('orders/');},   innerText : "ordini",    backgroundImage : 'url(\'' + URLbase() + 'upload/icons-xl/wallet-black.png\')',    backgroundColor : 'hsl(270, 60%, 70%)'	},
												{   id : "extraButton",	onclick : () => {cambiaStato('reviews/');},    innerText : "RECENSIONI",    backgroundImage : 'url(\'' + URLbase() + 'upload/icons-xl/chat-black.png\')',    backgroundColor : 'hsl(0, 60%, 70%)'    },
												{   id : "settingsButton",	onclick : () => {cambiaStato('settings/');},    innerText : "impostazioni",    backgroundImage : 'url(\'' + URLbase() + 'upload/icons-xl/gear6-black.png\')',    backgroundColor : 'hsl(90, 60%, 70%)'    }
											   ];
		
								 user  = getUser();
								
								 if(user != null){
									switch(user.type){
										case "SELLER":
											buts = sellerButtons;
										break;
										case "USER":
											buts = userButtons;
										break;
									}
									for(but of buts){
										btn = document.getElementById(but.id);
										btn.innerHTML = "" ;
										btn.classList.add("p-0");
										el = document.createElement("p");
										el.style.fontSize = "15px";
										el.classList.add("text-light");
										el.classList.add("mb-0");
										el.innerText = but.innerText;
										btn.style.height = "35vw";
										btn.style.backgroundImage = but.backgroundImage;
										btn.style.backgroundPosition = "center center";
										btn.style.backgroundRepeat = 'no-repeat';
										btn.style.backgroundSize = 'contain';
										btn.style.backgroundColor = but.backgroundColor;
										btn.onclick = but.onclick;
										btn.appendChild(el);
										
									}
								 } 
		                       }   //disegna la vista secondo lo stato corrente
	};
</script>