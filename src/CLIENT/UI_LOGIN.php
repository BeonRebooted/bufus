<section id="loginview" class="d-none bg-white" style="z-index: 10000; position: absolute; top: 0; width: 100vw;" role="tabpanel" aria-label="login view">
    <div class="container-fluid">
        <div class="row">
			<div class="col-3 d-none d-md-block" aria-hidden="true">
			</div>
			<div id="logincontent" class="col" role="presentation">
				<h1 class="mt-5 text-center">Benvenuto su BUFUs</h1>
				
				<form style="margin-top: 15vh" aria-label="login to browse products" aria-live="polite">
					<label for="loginmail">Email</label>
					<input type="email" class="form-control" id="loginmail" name="email">
					<br/>
					<label for="loginpassword">Password</label>
					<input type="password" class="form-control" id="loginpassword" name="password">
					<a id="loginforgot" href="../forgotpassword/"><small>ho dimenticato la password</small></a>
					<br/>
					<button id="logingogo" class="btn bg-primary text-light" style="margin-top: 5vh">login</button>
					<a id="loginregister" href="../register/" style="float: right; margin-top: 5vh"><small>oppure registrati</small></a>
					<output class="sr-only" id="loginOutcome" aria-label="esito login"></output>
				</form>
			</div>
			<div class="col-3 d-none d-md-block" aria-hidden="true">
			</div>
		</div>
	</div>
</section>
<script>
    loginViewObject = {
		isOpen : false,
		
		open :  function(){
							loginView = $("#loginview")[0];
							mainView = $("main")[0];  
							loginView.classList.remove("d-none");
							onceAnimationEnd(loginView, "swipeIn")
							.then(()=>{
								mainView.style.overflow = "hidden";
								views.active = loginViewObject;
								loginViewObject.isOpen = true;
								loginView.ariaHidden = false;
								loginViewObject.accessibility();
							});
						  },
					  
		close : async function(){
			if(loginViewObject.isOpen){
			    return new Promise(resolve => {
				   loginView = $("#loginview")[0];
					mainView = $("main")[0];  
					mainView.style.overflow = "auto";
					mainView.style.height = "";
					 onceAnimationEnd(loginView, "swipeOut")
				     .then(()=>{
						 loginView.classList.add("d-none");
						 loginView.classList.remove("swipeOut");
						 loginViewObject.isOpen = false;
						 loginView.ariaHidden = true;
						views.active = null;						 
						 resolve();	
					 });	
				});				
			}else{
				return new Promise(resolve => resolve());
			}		
        },
					  
		init : function(){ loginView = $("#loginview")[0];	
		                   registerViewObject.init();
            		     },    //resetta la vista al suo stato blank
					 
		draw : function(source){ 
			loginViewObject.init();
		},
		
		accessibility : function(){
			$("#loginmail")[0].focus();
		}
		
	};

	function login(event){
		event.preventDefault();	
                
                auth = "Basic : " + btoa("" + document.getElementById("loginmail").value + ":" + document.getElementById("loginpassword").value);  
		ultraFetch(URLroot() + 'service.php', "POST", auth, null)
		.then(response => {
			if(response.ok){
				response.json()
				.then(data => {
				    sessionStorage.setItem("user", JSON.stringify(data));
					sessionStorage.setItem("Authentication", btoa("" + document.getElementById("loginmail").value + ":" + document.getElementById("loginpassword").value));  
					cambiaStato("home/");
				});
			}else{
				loginOutcome.innerText = "Email o password non corretti"
				if( !document.getElementById("logincontent").classList.contains("bg-danger")){
					document.getElementById("logincontent").classList.add("bg-danger");
				}						 
			}
		});
	} 
		 
	document.getElementById("logingogo").onclick = login;
	document.getElementById("loginforgot").onclick = (e) => { e.preventDefault(); cambiaStato('forgotpassword/')};
	document.getElementById("loginregister").onclick = (e) => { e.preventDefault(); cambiaStato('register/')};
</script>
