<div id="notificationsview" class="d-none bg-white container-fluid py-md-2" role="tabpanel" aria-label="notifications view">
	<div class="row">
		<div class="col-12 col-md-6 py-3 mx-auto">
		    <h3>Notifiche</h3>
		    <ul id="notificationsList" class="list-group" style="list-style: none">
		    </ul>
		</div>
	</div>
</div>
			
			
<script>

	notificationsViewObject = {
        isOpen : false,
		
		open :  function(){ notificationsView = $("#notificationsview")[0];
							mainView = $("main")[0];  
							notificationsview.classList.remove("d-none");
							onceAnimationEnd(notificationsView, "swipeIn")
							.then(()=>{
								mainView.style.overflow = "auto";
								notificationsViewObject.isOpen = true;	
								notificationsView.ariaHidden = false;
								views.active = notificationsViewObject;
								
							});
						  },
					  
		close : async function(){
			if(notificationsViewObject.isOpen){
				return new Promise(resolve => {
				    notificationsView = $("#notificationsview")[0];
					mainView = $("main")[0];  
					mainView.style.overflow = "auto";
					mainView.style.height = "";
					onceAnimationEnd(notificationsView, "swipeOut")
					.then(()=>{
						 notificationsView.classList.add("d-none");
						 notificationsView.classList.remove("swipeOut");
						 notificationsView.ariaHidden = true;
						 notificationsViewObject.isOpen = false;
						 views.active = null;
						 resolve();	
					 });	
				});							
			}else{
				return new Promise(resolve => resolve());
			}		
		},
					  
		init : function(){ notificationsview = $("#notificationsview")[0];
		                   $("#notificationsList")[0].innerHTML  = "";
            		     },    //resetta la vista al suo stato blank
 
		draw : function(source){ notificationsViewObject.init();
								 ultraFetch(URLroot() + "service.php/users/" + getUser().id + "/", "GET", "Basic : " + sessionStorage.getItem("Authentication"))
								 .then(
									response => {
										response.json().then(data => {
											for(i = 0; i < data.notifications.length; i++){
												document.getElementById("notificationsList").appendChild(LInotification(data.notifications[i]));
											}
                                            if(document.getElementById("notificationsList").childNodes.length == 0){
												li = document.createElement("li");
												li.innerText = "nessuna notifica";
												li.classList.add("list-group-item");
												document.getElementById("notificationsList").appendChild(li);		
											}   	
										});

									}
								 );
		}
	};

	function closeNotification(notificationId){
		el  = document.getElementById("notificationLI" + notificationId);
		onceAnimationEnd(el, "notificationsLIClose").then(() => { 
			el.parentNode.removeChild(el);
			erano = $("#notificationsPill")[0].innerText;
			sono = parseInt(erano) - 1;
			$("#notificationsPill")[0].innerText = sono;
			if(sono == 0){
				$("#notificationsPill")[0].classList.add("d-none");
			}
		});
		ultraFetch(URLroot() + "service.php/notifications/" + notificationId + "/", "DELETE", "Basic " + sessionStorage.getItem("Authentication"))
		.then(response => {
             response.text().then(text => console.log(text))
		});
	}

	function LInotification(data){
		el = document.createElement("li");
		el.id = "notificationLI" + data.id;
		el.classList.add("list-group-item");
		el.classList.add("bg-light");
		row = document.createElement("div");
			row.classList.add("row");
			row.innerHTML = `<p class="col mb-0">notifica ${data.id}</p><div class="col p-0"><button class="btn btnClose mb-0 float-right">&times;</button></div>`;
		el.appendChild(row);
		row = document.createElement("div");
			row.classList.add("row");
			row.innerHTML = `<p class="col mb-0">${data.date}<br/><a href="${URLroot()}ui_2.php/${convertType(data.targetType)}/${data.targetId}/">${data.targetType}/${data.targetId}</a> : ${data.message}</p>`;
	    el.appendChild(row);
		el.getElementsByClassName("btnClose")[0].onclick = () => { closeNotification(data.id); };
		return el;
		
	}
	
	function convertType(typeString){
		switch (typeString){
			case "ORDINI":
			    return "orders";
			case "UTENTI":
			    return "users";
			case "PRODOTTI":
			    return "products";
		}
	}
			

			
</script>