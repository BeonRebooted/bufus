<section id="ordersview" class="d-none bg-white container-fluid py-md-2" role="tabpanel" aria-label="orders view">
	<div class="row">
		<div class="col-1 d-none d-md-block">
		</div>
		<div class="col-12 col-md py-3">
		    <h3 id="pendingOrdersListLabel" class="text-center">Ordini in sospeso</h3>
		    <ul id="pendingOrdersList" class="pl-0" style="list-style: none" aria-labelledby="pendingOrdersListLabel">
		    </ul>
		</div>
	    <div class="col-1 d-none d-md-block">
		</div>
		<div class="col-12 col-md py-3">
		    <h3 id="ordersListLabel" class="text-center">Storico</h3>
		    <ul id="ordersList" class="pl-0" style="list-style: none" aria-labelledby="ordersListLabel">
			</ul>
		</div>
		<div class="col-1 d-none d-md-block">
		</div>
	</div>
</section>
			
			
<script>

	ordersViewObject = {
        isOpen : false,
		
		open :  function(){ ordersView = $("#ordersview")[0];
							mainView = $("main")[0];  
							ordersView.classList.remove("d-none");
							onceAnimationEnd(ordersView, "swipeIn")
							.then(()=>{
								mainView.style.overflow = "auto";
								ordersViewObject.isOpen = true;	
								ordersView.ariaHidden = false;
								views.active = ordersViewObject;								
							});
						  },
					  
		close : async function(){
			if(ordersViewObject.isOpen){
				return new Promise(resolve => {
				    ordersView = $("#ordersview")[0];
					mainView = $("main")[0];  
					mainView.style.overflow = "auto";
					mainView.style.height = "";
					onceAnimationEnd(ordersView, "swipeOut")
					.then(()=>{
						 ordersView.classList.add("d-none");
						 ordersView.classList.remove("swipeOut");
						 ordersViewObject.isOpen = false;
						 ordersView.ariaHidden = true;
						 views.active = null;
						 resolve();	
					 });	
				});							
			}else{
				return new Promise(resolve => resolve());
			}		
		},
					  
		init : function(){ ordersView = $("#ordersview")[0];
		                   $("#pendingOrdersList")[0].innerHTML  = "";
						   $("#ordersList")[0].innerHTML  = "";
						   //ordersView.style.transition = "left 0.3s";
            		     },    //resetta la vista al suo stato blank
 
		draw : function(source){ ordersViewObject.init();
								 ultraFetch(URLroot() + "service.php/orders/", "GET", "Basic " + sessionStorage.getItem("Authentication"))
								 .then(
									response => {
										response.json().then(data => {
											for(i = 0; i < data.length; i++){
												if(data[i].status == "ARRIVATO_STAZIONE"){
													document.getElementById("ordersList").appendChild(LIorder(data[i]));
												}else{
													document.getElementById("pendingOrdersList").appendChild(LIorder(data[i]));
												}
											}
                                            if(document.getElementById("pendingOrdersList").childNodes.length == 0){
												li = document.createElement("li");
												li.innerText = "nessun ordine in sospeso";
												li.classList.add("list-group-item");
												document.getElementById("pendingOrdersList").appendChild(li);		
											}   
											if(document.getElementById("ordersList").childNodes.length == 0){
												li = document.createElement("li");
												li.innerText = "nessun ordine passato";
												li.classList.add("list-group-item");
												document.getElementById("ordersList").appendChild(li);	
											}	
										});

									}
								 );
		}
	};


	function paymentConfirmed(){
		location.reload();
	}
			
	function paymentError(){
		showAModal("payment failed", document.createElement("div"), null, null);
	}
	
	function changeOrderState(orderId){
		ultraFetch(URLroot() + "service.php/orders/" + orderId + "/", "POST", "Basic : " + sessionStorage.getItem("Authentication"))
		.then(response => {
			if(response.ok){			
                li = document.getElementById("orderLI" + orderId);
				onceAnimationEnd(li, "orderLIFlipOut")
				.then(()=> { 
					ultraFetch(URLroot() + "service.php/orders/" + orderId + "/", "GET", "Basic : " + sessionStorage.Authentication)
					.then(data => 
						{
							data.json()
							.then(order => {
								//se lo stato cambia può essere necessario cambiare lista
								li.replaceWith(LIorder(order))
								}
							)
						}
					)}
				);
			}
		});
	}

	function LIorder(data){
		el = document.createElement("li");
		el.classList.add("border");
		el.classList.add("p-0");
		el.classList.add("my-3");
		el.id = "orderLI" + data.id;
		row = document.createElement("div");
		row.style.color = "rgb(#fffff)"
		row.classList.add("row");
		row.classList.add("bg-light");
		row.classList.add("no-gutters");
		row.classList.add("p-2");
		row.innerHTML = `<a class="col mb-0 text-dark" role="button" href="#orderLI${data.id}details" aria-expanded="false" aria-controls="orderLI${data.id}details" data-toggle="collapse">(${data.id} | ${data.status})    ${data.sellerName} vende a ${data.clientName} per ${data.price}$</a>`;
		
		if(data.payTokenId != null){
			row.innerHTML += `<button class="col-3 btn bg-primary float-right LIorderBut"> PAGA</button>`;
			row.getElementsByClassName("LIorderBut")[0].onclick = ()=>{  window.open(` ${ URLroot() }paymentservice/payment.php/${data.payTokenId}/`); };
		}else{
			if(user.type == "SELLER" && data.status != "ARRIVATO_STAZIONE" && data.status != "ATTESAPAGAMENTO"){	
				row.innerHTML +=`<button class="col-3 btn bg-primary float-right LIorderBut">step</button>`;
				row.getElementsByClassName("LIorderBut")[0].onclick = ()=>{ changeOrderState(data.id); };
			}
		}
			
	    el.appendChild(row);
		
		
		row = document.createElement("ul");
		row.ariaLabel = "dettagli ordine";
		row.classList.add("collapse");
		//row.classList.add("list-group");
		row.classList.add("p-0");
		row.classList.add("mt-2");
		row.id = "orderLI" + data.id + "details";
		for(detail in data.details){
			li = document.createElement("li")
			li.classList.add("list-group-item")
			li.classList.add("border-0")
			li.classList.add("border-bottom")
			li.innerHTML = `<a href="${ URLroot() }ui_2.php/products/${data.details[detail].productId}/">product ${data.details[detail].productId}</a> :  ${data.details[detail].quantity } pezzi`
			li.onclick = (evt) => { evt.preventDefault(); cambiaStato("products/" + data.details[detail].productId + "/") }
			row.appendChild(li);
		}
		el.appendChild(row);
		return el;
		
	}
			

			
</script>