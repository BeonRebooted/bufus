		<template id="client-product-option-template">
			<style>
				 .orderLIFlipIn {
					animation-name: orderLIFlipIn;
					animation-duration: 0.1s;
				}
				.orderLIFlipOut {
					animation-name: orderLIFlipOut;
					animation-duration: 0.1s;
				}
				
				@keyframes orderLIFlipOut { 
					0% {  transform: rotateX(0deg); height: 100%; }
					100% { transform: rotateX(90deg); height: 0;}
				}

				@keyframes orderLIFlipIn { 
					0% {  transform: rotateX(90deg); height: 0%; }
					100% { transform: rotateX(0deg); height: 100%;}
				}
			</style>
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
			<div class="row no-gutters bg-light my-4 rounded" style="font-size: 11px; border-style: solid; border-width: 1px; border-color: #dee2e6">
				<div class="col-3 my-2 row no-gutters">
					<input id="root_selezionata" type="checkbox" class="form-control mb-0 mr-3">
				</div>
				<div class="col-9 row no-gutters mt-2">
					<div class="col-12 row no-gutters" style="font-size: 15px">
						<p id="root_nome" class="col-8 mb-0" style="font-size: 15px"></p>
						<div class="col-1"></div>
						<p id="root_prezzo" class="mb-0 col-3"></p>
					</div>
					<p id="root_descrizione" class="col-12 mb-0">
					</p>
				</div>
				<div id="root_controls" class="col-12 my-2 row no-gutters d-none">
				</div>
			</div>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
		</template>
		<script>
		
		  // If your code is inside of an HTML Import you'll need to change the above line to:
		  // let tmpl = document.currentScript.ownerDocument.querySelector('#x-foo-from-template');
			  
			class ClientProductOption extends HTMLElement {
				
				get selected(){
					return this.isSelected;
				}
				
				set selected(val){
					if(val){
						this.elSelezionata.checked = true;
					}else{
						this.elSelezionata.checked = false;
					}
					this.isSelected = val;
					this.toggleControls();
				}
				
				get required(){
					return this.isRequired;
				}

				set required(val){
					if(val){
						this.elSelezionata.disabled = true;
						this.elSelezionata.checked = true;
					}else{
						this.elSelezionata.disabled = false;
					}
				}

				set disabled(val) {
					// Reflect the value of the disabled property as an HTML attribute.
					if (val) {
					  this.setAttribute('disabled', '');
					} else {
					  this.removeAttribute('disabled');
					}
				}
				
				get option(){
					return this.storedOption;
				}

				set option(option){
					this.storedOption = option;
				}

				// Can define constructor arguments if you wish.
				constructor(product, option) {
					super(); // always call super() first in the constructor.
						
						
					this.storedProduct = product;
					this.storedOption = option;
					
					let shadowRoot = this.attachShadow({mode: 'open'});
					let freshNode = document.querySelector('#client-product-option-template').content.cloneNode(true);
					
					this.elNome = freshNode.getElementById("root_nome")
					this.elSelezionata = freshNode.getElementById("root_selezionata")
					this.elDescrizione = freshNode.getElementById("root_descrizione")
					this.elPrezzo = freshNode.getElementById("root_prezzo")
					this.elTipo = freshNode.getElementById("root_tipo")
					this.elControls = freshNode.getElementById("root_controls")
					
					//setting values
					this.elNome.innerText = option.name;
					this.elSelezionata.checked = option.required;
					if(option.required){
						option.selected = 1;
						this.elSelezionata.disabled = true;
						//i controlli dell opzione sono aperti
						this.elControls.classList.remove("d-none")
					}else{
						this.elSelezionata.oninput = (e) => {
							for(option of this.storedProduct.options){
								if(option.name == this.storedOption.name){
									if(e.target.checked){
										option.selected = 1;
									}else{
										delete option.selected
									}
									this.selected = e.target.checked;
									this.storedOption = option;
									pvo.store();
									pvo.items.priceItem.draw(false);
									break;
								}
							}
						}
					}
					this.elDescrizione.innerText = option.description;
					this.elPrezzo.innerText = option.price;
					
					//this.elTipo.value = option.tipo;
					
					this.updateSub();
					shadowRoot.appendChild(freshNode);
				}
				
				updateSub(){
					switch(this.storedOption.type){
						case "TEXTVALUE" :
							var inp = ELFromTXT("<input type='text'>")
							inp.oninput = (e) => {
								this.storedOption.value = e.target.value;
							}
							this.storedOption.value = "";
							this.elControls.appendChild(inp);
						break;
						case "FILEVALUE" :
							var inp = ELFromTXT("<input type='file'>")
							inp.oninput = (e) => {
								this.storedOption.value = e.target.files[0];
							}
							this.storedOption.value = new File([],"ciao.txt",{
								  type: "text/plain",
							})
							//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
							this.elControls.appendChild(inp);
						break;
						case "INTRANGE" :
							var inp = ELFromTXT("<input type='number' class='form-control'>")
							inp.oninput = (e) => {
								this.storedOption.value = e.target.value;
							}
							var mid = Math.floor((this.storedOption.maximum + this.storedOption.minimum) / 2)
							this.storedOption.value = mid;
							inp.value = mid;
							this.elControls.appendChild(inp);
						break;
						case "FLOATRANGE" :
							var inp = ELFromTXT("<input type='number' class='form-control'>")
							inp.oninput = (e) => {
								this.storedOption.value = e.target.value;
							}
							var mid = (this.storedOption.maximum + this.storedOption.minimum) / 2
							this.storedOption.value = mid;
							inp.value = mid;
							this.elControls.appendChild(inp);
						break;
						case "VALUE" :
							var inp = ELFromTXT("<select class='form-control mx-2'></select>")
							//questo aggiora anche pvo.product   (option è passata per riferimento
							inp.onchange = (e) => {
								this.storedOption.value = e.target.value;
								pvo.items.priceItem.draw(false);
							}
							for(let val of this.storedOption.values){
								inp.appendChild(ELFromTXT("<option>" + val.value + "</option>"))
							}
							//il valore vuoto è default per type.VALUE senza valori
							this.storedOption.value = (this.storedOption.value) ? this.storedOption.value : (this.storedOption.values[0]) ? this.storedOption.values[0].value : "";
							pvo.items.priceItem.draw(false);
							this.elControls.appendChild(inp);
						break;						
					}
					
				}
				
				toggleControls(){
					console.log("toggleControls")
					if(this.isSelected){
							console.log("flipIN")
							var onAnimationEndCb = () => {
								this.elControls.removeEventListener('animationend', onAnimationEndCb);
								this.elControls.classList.remove("orderLIFlipIn");
							}
							this.elControls.addEventListener('animationend', onAnimationEndCb);
							this.elControls.classList.add("orderLIFlipIn");
							this.elControls.classList.remove("d-none")
					}else{
							console.log("flipOUT")
							var onAnimationEndCb = () => {
								this.elControls.removeEventListener('animationend', onAnimationEndCb);
								this.elControls.classList.remove("orderLIFlipOut");
								this.elControls.classList.add("d-none")
							}
							this.elControls.addEventListener('animationend', onAnimationEndCb);
							this.elControls.classList.add("orderLIFlipOut");
						
					}
				}
			}
			
			customElements.define('client-option', ClientProductOption);
		</script>

