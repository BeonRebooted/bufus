<section id="productview" class="d-none bg-white container-fluid p-md-2" role="tabpanel" aria-label="product view">
	<div class="row no-gutters">
	    <article class="col my-3 container-fluid rounded p-2" aria-label="prodotto">
			    <div class="row">
					<div class="col-12 col-md-6 col-lg row no-gutters">
						<section id="productImgSection" class="col mx-auto text-center" role="presentation">
							<img id="productImg" src="DA_JAVASCRIPT" alt="DA_JAVASCRIPT" class="img-fluid m-0" aria-label="immagine prodotto">
						</section>
					</div>
					<div class="d-none d-lg-block mx-2" aria-hidden="true"></div>
					<div class="col-12 col-md-6 col-lg row no-gutters">
					    <section id="productMainSection" class="col my-5 my-sm-0" aria-label="product info">
							<div class="row no-gutters">
								<div class="col"><h5 id="productName" class="rh5 text-break" aria-label="nome prodotto">NOMEPRODOTTO</h5></div>
							</div>
							<div class="row no-gutters" role="presentation">
							    <div class="col-12" role="presentation"><a href="DAJAVASCRIPT" id="productCategory" class="text-break" aria-label="categoria prodotto">categoria</a></div>
								<div class="col-8" role="presentation"><a href="DAJAVASCRIPT" id="productSeller" class="text-break" aria-label="venditore">venditore</a></div>
								<div class="col-4 text-right"><p id="productPrice" class="mb-0 rh6">soldi$</p></div>
							</div>
							<div class="row"></div>
							<div class="row no-gutters">
							    <div class="col d-flex align-items-center"><button id="editProdBut" class="btn d-block my-2 border-primary float-left" style="border-width: 3px;">Edit product</button></div>
								<div class="col d-flex align-items-center flex-row-reverse"><button id="addCartBut" class="btn d-block my-2 border-primary float-right" style="border-width: 3px;">aggiungi al carrello</button></div>
							</div>
							<?php require "./CLIENT/UI_PRODUCT/productOptions.php" ?>
						</section>
					</div>
					<hr class="d-lg-none" aria-hidden="true">
					<div class="d-none d-lg-block mx-2" aria-hidden="true"></div>
					<div class="col-12 col-lg">
					    <section id="productDescriptionSection" class="row no-gutters mt-5 m-lg-0" role="presentation">
							<div class="col-12">
								<h5>Descrizione</h5>
							</div>
							<div class="col-12">
								<p id="productDescription" class="text-break"></p>
							</div>
						</section>
						<hr>
						<section id="productReviewsSection" class="row no-gutters" role="presentation">
							<div class="col">
								<h5 id="productreviewslabel">Recensioni</h5>
								<button id="addReviewBut" class="btn border-primary float-right" style="border-width: 3px;">Aggiungi recensione</button>
							</div>
							<div class="col-12 mt-5">
								<ul id="productReviews" class="pl-0" aria-labelledby="productreviewslabel">
								</ul>
							</div>
						</section>
					</div>
				</div>
		</article>	
	</div>	
	<script>
		 <!--require dalla radice dei sorgenti-->
		 <?php require "./CLIENT/UI_PRODUCT/product.js"; ?>
	</script>	
</section>
	