		<template id="product-option-template">
			<style>
				 .orderLIFlipIn {
					animation-name: orderLIFlipIn;
					animation-duration: 0.1s;
				}
				.orderLIFlipOut {
					animation-name: orderLIFlipOut;
					animation-duration: 0.1s;
				}
				
				@keyframes orderLIFlipOut { 
					0% {  transform: rotateX(0deg); height: 100%; }
					100% { transform: rotateX(90deg); height: 0;}
				}

				@keyframes orderLIFlipIn { 
					0% {  transform: rotateX(90deg); height: 0%; }
					100% { transform: rotateX(0deg); height: 100%;}
				}
			</style>
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
			<div class="row no-gutters bg-light my-4 rounded px-sm-4 px-2" style="font-size: 11px; border-style: solid; border-width: 2px; border-color: #dee2e6;">
				<div class="col-12 my-2 row no-gutters">
					<div class="col-8">
						<input id="root_nome" type="text" class="mb-0" style="width: inherit">
						<label for="root_nome">nome</label>
					</div>
					<div class="col-1">
					</div>
					<div class="col-1">
						<input id="root_obbligatoria" type="checkbox" class="mb-0" style="width: inherit">
						<label for="root_obbligatoria">obbl.</label>
					</div>
					<div class="col-1">
					</div>
					<button id="root_close" class="btn border-primary col-1 form-control form-control-sm" disabled>&times;</button>
				</div>
				<div class="col-12 my-2 row no-gutters">
					<div class="col-8">
						<input id="root_descrizione" type="text" class="mb-0" style="width: inherit">
						<label for="root_descrizione">descrizione</label>
					</div>
					<div class="col-1"></div>
					<div class="col-3">
						<input id="root_prezzo" type="number" class="numberNoControls mb-0" style="width: inherit">
						<label for="root_prezzo">prezzo</label>
					</div>
				</div>
				<div class="col-12 my-2 row no-gutters">
					<div class="col">
						<select id="root_tipo" class="" style="width: inherit">
						</select>
						<label for="root_tipo">tipo</label>
					</div>
				</div>
				<div id="root_data" class="col-12 my-2 row no-gutters">
				</div>
			</div>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
		</template>
		<script>
		
		  // If your code is inside of an HTML Import you'll need to change the above line to:
		  // let tmpl = document.currentScript.ownerDocument.querySelector('#x-foo-from-template');
			  
			class SellerProductOption extends HTMLElement {
			
				get editMode() {
					return this.hasAttribute('editmode');
				}
				
				set editMode(val) {
					// Reflect the value of the open property as an HTML attribute.
					if (val) {
					  this.setAttribute('editmode', '');
					  for(let ele of [this.elNome , this.elObbligatoria , this.elDescrizione , this.elPrezzo, this.elTipo , this.elClose]){
						ele.classList.add("form-control","form-control-sm");
						ele.style.width = "";
						ele.removeAttribute("disabled");
						ele.removeAttribute("readonly");
					  }				  
					} else {
					  this.removeAttribute('editmode');
					  for(let ele of [this.elNome , this.elObbligatoria , this.elDescrizione , this.elPrezzo, this.elTipo, this.elClose]){
						ele.classList.remove("form-control","form-control-sm");
						ele.style.width = "inherit"
						ele.setAttribute("disabled","");
						ele.setAttribute("readonly","");
					  }
					}
				}
				
				get option(){
					return this.storedOption;
				}

				set option(option){
					this.storedOption = option;
				}

				// Can define constructor arguments if you wish.
				constructor(product, option) {
					super(); // always call super() first in the constructor.
						
						
					this.storedProduct = product;
					this.storedOption = option;
					
					let shadowRoot = this.attachShadow({mode: 'open'});
					let freshNode = document.querySelector('#product-option-template').content.cloneNode(true);
					
					this.elNome = freshNode.getElementById("root_nome")
					this.elObbligatoria = freshNode.getElementById("root_obbligatoria")
					this.elDescrizione = freshNode.getElementById("root_descrizione")
					this.elPrezzo = freshNode.getElementById("root_prezzo")
					this.elTipo = freshNode.getElementById("root_tipo")
					this.elClose = freshNode.getElementById("root_close")
					this.elData = freshNode.getElementById("root_data")
					
					//setting values
					this.elNome.value = option.name;
					this.elNome.oninput = (e) => {
						var oldName = this.storedOption.name;
						for(option of this.storedProduct.options){
							if(option.name == oldName){
								option.name = e.target.value;
								this.storedOption = option;
								pvo.store();
								break;
							}
						}
					}
					this.elObbligatoria.value = option.required;
					this.elObbligatoria.oninput = (e) => {
						for(option of this.storedProduct.options){
							if(option.name == this.storedOption.name){
								option.required = (e.target.checked) ? 1 : 0;
								this.storedOption = option;
								pvo.store();
								break;
							}
						}
					}
					this.elDescrizione.value = option.description;
					this.elDescrizione.oninput = (e) => {
						for(option of this.storedProduct.options){
							if(option.name == this.storedOption.name){
								option.description = e.target.value;
								this.storedOption = option;
								pvo.store();
								break;
							}
						}
					}
					this.elPrezzo.value = option.price;
					this.elPrezzo.oninput = (e) => {
						for(option of this.storedProduct.options){
							if(option.name == this.storedOption.name){
								option.price = parseFloat(e.target.value)
								this.storedOption = option;
								pvo.store();
								break;
							}
						}
					}
					for(name of ["VALUE","TEXTVALUE","FILEVALUE","INTRANGE","FLOATRANGE"]){
								let opt = document.createElement("option")
								opt.innerText = name
								this.elTipo.appendChild(opt);
					}
					
					this.elTipo.value = option.type;
					this.elTipo.onchange = v => { 
						this.storedOption.type = v.target.value;
						onceAnimationEnd (this.elData, "orderLIFlipOut")
							.then(
								() => {					
										this.elData.classList.remove("orderLIFlipOut")
										this.updateSub();
										onceAnimationEnd(this.elData, "orderLIFlipIn").then(() => { this.elData.classList.remove("orderLIFlipIn") })
									  }
								)
		
						sessionStorage.setItem("product",JSON.stringify(pvo.product))
					}
					this.elClose.onclick = () => {
							this.remove();
							let options = this.storedProduct.options;
							options.splice(options.indexOf(this.storedOption), 1)
							pvo.store();
					};
					
					this.updateSub();
					shadowRoot.appendChild(freshNode);
				}
				
				updateSub(){
					var cont = this.elData;
					switch(this.storedOption.type){
						case "VALUE":
							delete this.storedOption.minimum;
							delete this.storedOption.maximum;
							this.storedOption.values = (this.storedOption.values)? this.storedOption.values : []
							cont.innerHTML = "";
							
							let subControls = ELFromTXT('<div class="col-12 row no-gutters mb-3"></div>')
							
							let clr = ELFromTXT("<button id='root_VALUE_clearAll' class='border-primary btn col-1 form-control-sm'>C</button>");
							clr.onclick = (e) => {
								heading = "si stanno a rimuovere tutti i valori"
								body = ELFromTXT("<p>sei sicuro?</p>")
								confirmCallback = () => {
									this.storedOption.values = []
									pvo.store();
									pvo.draw(pvo.currentSource);
															  
								}
								showAModal(heading, body, confirmCallback)
							}
							subControls.appendChild(clr);
							
							subControls.appendChild(ELFromTXT('<div class="col"></div>'));
							
							let neu = ELFromTXT("<button id='root_VALUE_addValue' class='border-primary btn col-1 form-control-sm'>+</button>");
							neu.onclick = (e) => {
								let optionName = this.storedOption.name;
								showOptionValueModal(optionName)
							}
							subControls.appendChild(neu);

							cont.appendChild(subControls);
							let vals = ELFromTXT('<ol id="child" class="row no-gutters col-12" style="list-style: none"></ol>')
							console.log("subs VALUE")
							console.log(this.storedOption.values)
							for(let value of this.storedOption.values){
								let row = ELFromTXT('<li id="root_VALUE_values_' + value.value + '" class="border rounded col-12 row no-gutters"><div class="col">' + value.value + '<span class="float-right pr-2">' + value.price + '$</span>'+'</div></li>')
								let edit = ELFromTXT('<button id="root_VALUE_values_' + value.value + '_edit" class="btn border-primary col-1"></button>')
									edit.style.backgroundImage = 'url(\'' + URLbase() + 'upload/icons-xl/gear6-black.png\')'
									edit.style.backgroundPosition = "center center";
									edit.style.backgroundRepeat = 'no-repeat';
									edit.style.backgroundSize = 'contain';
									edit.onclick = (e) => {
										let valName = e.target.id.split("root_VALUE_values_")[1].split("_edit")[0]
										showOptionValueModal(this.storedOption.name, valName)
										pvo.store();
										pvo.draw(pvo.currentSource)
									}
								row.appendChild(edit)
								vals.appendChild(row);
							}
							cont.appendChild(vals);
						break;
						case "INTRANGE":
							delete this.storedOption.values;
							cont.innerHTML = "";
							let min = document.createElement("input")
							min.id = "root_INTRANGE_MIN"
							min.classList.add("form-control" , "form-control-sm")
							min.setAttribute("type" , "number" ); 
							this.storedOption.minimum = (this.storedOption.minimum)? this.storedOption.minimum : -10;
							min.value = this.storedOption.min;
							min.oninput = (e) => {
								console.log("int min input")
								neuVal = e.target.value
								max = this.storedOption.maximum;
								if(parseInt(neuVal) > parseInt(max))
											e.target.value = max;
							}
							let max = document.createElement("input")
							max.id = "root_INTRANGE_MAX"
							max.classList.add("form-control" , "form-control-sm")
							max.setAttribute("type" , "number" ); 
							this.storedOption.maximum = (this.storedOption.maximum)? this.storedOption.maximum : 10;
							max.value = this.storedOption.maximum;
							max.oninput = (e) => {
								console.log("int max input")
								neuVal = e.target.value
								min = this.storedOption.minimum;
								if(parseInt(neuVal) < parseInt(min))
											e.target.value = min;
							}
							cont.appendChild(min)
							cont.appendChild(ELFromTXT('<label for="root_INTRANGE_MIN">minimo</label>'))
							cont.appendChild(max)
							cont.appendChild(ELFromTXT('<label for="root_INTRANGE_MAX">massimo</label>'))
						case "FLOATRANGE":
							delete this.storedOption.values;
							cont.innerHTML = "";
							let min2 = document.createElement("input")
							min2.id = "root_FLOATRANGE_MIN"
							min2.classList.add("form-control" , "form-control-sm")
							min2.setAttribute("type" , "number" ); 
							this.storedOption.minimum = (this.storedOption.minimum)? this.storedOption.minimum : -10;
							min2.value = this.storedOption.minimum;
							min2.oninput = (e) => {
								console.log("float min input")
								let neuVal = e.target.value
								let max = this.storedOption.maximum;
								if(parseFloat(neuVal) > parseFloat(max))
											e.target.value = max;
							}
							let max2 = document.createElement("input")
							max2.id = "root_FLOATRANGE_MAX"
							max2.classList.add("form-control" , "form-control-sm")
							max2.setAttribute("type" , "number" ); 
							this.storedOption.maximum = (this.storedOption.maximum)? this.storedOption.maximum : 10;
							max2.value = this.storedOption.maximum;
							max2.oninput = (e) => {
								console.log("float max input")
								let neuVal = e.target.value
								let min = this.storedOption.minimum;
								console.log(neuVal + " " + min)
								if(parseFloat(neuVal) < parseFloat(min))
											e.target.value = min;
							}
							cont.appendChild(min2)
							cont.appendChild(ELFromTXT('<label for="root_FLOATRANGE_MIN">minimo</label>'))
							cont.appendChild(max2)
							cont.appendChild(ELFromTXT('<label for="root_FLOATRANGE_MAX">massimo</label>'))
						break;
						case "FILEVALUE":
						case "TEXTVALUE":
							delete this.storedOption.values;
							delete this.storedOption.minimum;
							delete this.storedOption.maximum;
							cont.innerHTML = "";
						break;
					}}
			}
			
			customElements.define('seller-option', SellerProductOption);
		</script>

