<section class="modal fade pl-0" id="editProductModal" tabindex="-1" role="dialog" aria-labelledby="editProductModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="editProductModalLabel" style="font-size: 15px;">Modal title</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
		  </div>
		  <div class="modal-footer">
			<img id="imgFilePreview" src="DAJAVASCRIPT" alt="PRODUCT IMAGE" class="img-fluid" style="display: none">
			<button type="button" id="editProductModalDismiss" class="btn btn-secondary" data-dismiss="modal">Discard</button>
			<button type="button" id="editProductModalSave" class="btn btn-primary">Save changes</button>
		  </div>
		</div>
	</div>		
</section>


<section class="modal fade pl-0" id="productReviewModal" tabindex="-1" role="dialog" aria-labelledby="productReviewModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="productReviewModalLabel" style="font-size: 15px;">Inserisci recensione</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
		    <form aria-label="inserisci i dati della recensione">
				<input id="reviewRating" type="range" min="1" max="5" class="form-control-range mt-5"><label for="reviewRating" class="mb-5">RATING</label>
				<div id="reviewRatingFeedback" class="d-none">Looks good!</div>
				<textarea id="reviewText" class="form-control"></textarea><label for="reviewText">COMMENTO</label>
			</form>
		  </div>
		  <div class="modal-footer">
			<button type="button" id="reviewDismiss" class="btn btn-secondary" data-dismiss="modal">Discard</button>
			<button type="button" id="reviewSave" class="btn btn-primary">Save changes</button>
		  </div>
		</div>
	</div>		
</section>

<script>

	async function showAModal(heading, body, confirmCallback, dismissCallback = null){
		$('#editProductModal').modal();
		$('#editProductModal .modal-body')[0].innerHTML = "";
		$('#editProductModal .modal-body')[0].appendChild(body);
		$('#editProductModalLabel')[0].innerText = heading;
		$('#editProductModalSave')[0].onclick = () => { confirmCallback() ; $('#editProductModal').modal("hide");  };
		$('#editProductModalDismiss')[0].onclick = dismissCallback;
		return new Promise(resolve => {		
			$('#editProductModal').on('shown.bs.modal', function (e) {
				resolve();
			});
		})
		
	}
	
	
	function showReviewModal(id){
		$("#reviewSave")[0].onclick = () => {
			fd = new FormData();
			rateInput = document.getElementById("reviewRating");
			textInput = document.getElementById("reviewText");
			fd.append("rating",rateInput.value);
			fd.append("text",textInput.value);
			ultraFetch(URLroot() + "service.php/products/" + id + "/reviews/", "POST", "Basic " + sessionStorage.getItem("Authentication"), fd)
			.then(() => {
				$("#productReviewModal").modal("hide");
				productViewObject.draw("products/" + id + "/");
			});
		};
		$("#reviewDismiss")[0].onclick = () => {};
		$("#productReviewModal").modal();
	}
	
	
    function showProductConfirmSendModal(prod){
		body = document.createElement("p");
		body.innerText = "Vuoi salvare le modifiche?";
		confirmCallback = async () => {
			uploadProductEdit(pvo.product)
			.then(prod => {
				oldProd = JSON.parse(JSON.stringify(prod));
				productViewObject.init();
				
				$('#editProductModal').modal('hide');
				productViewObject.draw("products/" + prod.id + "/");
			})
			.catch((err) => {
				pushie("something was wrong", err);
				$('#editProductModal').modal('hide');
			});
		};
		dismissCallback = () => { 
			productViewObject.init();
			cambiaStato("products/" + prod.id + "/");
			$('#editProductModal').modal('hide');
		};
		showAModal("STAI TERMINANDO LE MODIFICHE", body, confirmCallback, dismissCallback);
	}
	
    function showProductEditNameModal(prod){
        heading = "MODIFICA NOME";

		textIn = document.createElement("input");
		textIn.type = "text";
		textIn.classList.add("form-control");
		textIn.value = prod.name;
		body = textIn;
		confirmCallback = () => { 
		    prod.name = textIn.value;
			pvo.store();
			$('#editProductModal').modal('hide');
			productViewObject.draw(pvo.currentSource);
		};
        showAModal(heading, body, confirmCallback)
		.then(() => {
			textIn.focus();
		});
	}
	
	function showProductEditPriceModal(prod){
		 heading = "MODIFICA PREZZO";
				
		textIn = document.createElement("input");
		textIn.type = "number";
		textIn.min = 0;
		textIn.classList.add("form-control");
		textIn.value = prod.price;
		body = textIn;
		confirmCallback = () => { 
		    prod.price = textIn.value;
			productViewObject.store();
			$('#editProductModal').modal('hide');
			//initProductMode(prod);
			productViewObject.draw(pvo.currentSource);
		};
        showAModal(heading, body, confirmCallback)
		.then(() => {
			textIn.focus();
		});
	}
	
    function showProductEditDescriptionModal(prod){	
	
		heading = "MODIFICA DESCRIZIONE";
				
		textIn = document.createElement("input");
		textIn.type = "text";
		textIn.classList.add("form-control");
		textIn.value = prod.description;
		body = textIn;
		confirmCallback = () => { 
		    prod.description = textIn.value;
			productViewObject.store();
			$('#editProductModal').modal('hide');
			productViewObject.draw(productViewObject.currentSource);
		};
        showAModal(heading, body, confirmCallback)
		.then(() => {
			textIn.focus();
		});
	}
	
    function showProductEditImageModal(prod){
		heading = "MODIFICA IMMAGINE (non 4MB di immagine plz)";
		body = document.createElement("div");
		    p = document.createElement("p");
		        p.innerText = "Select image";
		    body.appendChild(p);
			fileBuf = document.createElement("input");
				fileBuf.type = "file";
				fileBuf.classList.add("btn");
				//fileBuf.style.display = "block";
				fileBuf.onchange = evt => {
					const [file] = fileBuf.files;
					if (file) {
                        previewImg.src = URL.createObjectURL(file);
			            prod.imgSrc = previewImg.src;
                    }
			    }
		    body.appendChild(fileBuf);
			previewImg = document.createElement("img");
				previewImg.classList.add("img-fluid");
		    body.appendChild(previewImg);
        
		confirmCallback = () => {
			    fileBuf.onchange = () => {};
				[arr] = fileBuf.files;
				$('#productImg')[0].src = URL.createObjectURL(arr);
				productViewObject.product.imgSrc = previewImg.src;
				imgFileFrom(prod.imgSrc)
					.then(blob => toBase64(blob))
					.then(tex => {
								pvo.product.imgFile = tex
								pvo.store();
								$('#editProductModal').modal("hide");
								productViewObject.draw(productViewObject.currentSource);
					});
		};
		dismissCallback = () => {
			productViewObject.draw(pvo.currentSource);
		};
        showAModal(heading,body,confirmCallback,dismissCallback)
		.then(() => {
			fileBuf.focus();
		});		
	}
	
	function showProductEditCategoryModal(prod){	
        heading = "MODIFICA CATEGORIA";
		sel = document.createElement("select");
		    sel.classList.add("form-control");
		    sel.id = "selectEditCategory";
		    sel.size = 15;
		console.log("per rimuovere la scrollbar vedi https://www.w3schools.com/howto/howto_css_hide_scrollbars.asp");
		ultraFetch(URLroot() + "service.php/products/categories/",
             	   "GET")
		.then((categories) => {
			categories.json().then(entries => {
				for(category of entries){
		            opt = document.createElement("option");
		            opt.value = category.id;
	                opt.innerText = category.name;
					opt.style.borderTop = "1px solid black";
	                sel.appendChild(opt);
	            }
				sel.childNodes[0].style.borderTop = "0px";
			});					
        });
		confirmCallback = () => {
			pvo.product.categoryName = selectEditCategory.childNodes[selectEditCategory.value - 1].innerText;
			pvo.product.categoryId = sel.value - 1;
			productViewObject.store();
			$('#editProductModal').modal('hide');
			productViewObject.draw(productViewObject.currentSource);
		}
		showAModal(heading,sel,confirmCallback)
		.then(() => {
			sel.focus();
		});
	}

	function showOptionValueModal(optionName, optionValue = null){
		placeHolder = "__VALORE"
		newMode = false;
		if(!optionValue){
			heading = "AGGIUNGI NUOVO VALORE PER \"" + optionName + "\"";
			optionValue = placeHolder
			newMode = true;
		}else{
			oldValue = optionValue
			heading = "AGGIORNA VALORE PER \"" + optionName + "\" (era : \"" + oldValue + "\")";
			
		
		}

		textIn = document.createElement("input");
		textIn.type = "text";
		textIn.value = optionValue;
		textIn.classList.add("form-control");
		prezzo = 0;
		//se non è un nuovo valore, può avere il prezzo
		for(option of pvo.product.options){
			if(option.name == optionName){
				found = false
				for(valu of option.values){
					if(valu.value == textIn.value){
						found = true;
						prezzo = parseFloat(valu.price)
						break;
					}
				}
			}
		}
		price = document.createElement("input");
		price.type = "number";
		price.value = prezzo;
		price.classList.add("form-control");
		
		body = document.createElement("div");
		body.appendChild(textIn);
		body.appendChild(price);
		
		confirmCallback = () => {
			//manca la validazione input
			if(textIn.value == placeHolder){
				return;
			}
			if(newMode){
				for(option of pvo.product.options){
					if(option.name == optionName){
						found = false
						for(valu of option.values){
							if(valu.value == textIn.value){
								found = true;
								valu.price = price.value
								//esiste già il tale valore
								break;
							}
						}
						if(!found){
							option.values.push(
								{ 
									productId: option.productId,
									name: option.name,
									price: price.value,
									value: textIn.value
								}
							)
						}
						break;
					}
				}				
			}else{
				for(option of pvo.product.options){
					if(option.name == optionName){
						for(valu of option.values){
							if(valu.value == oldValue){
								valu.value = textIn.value;
								valu.price = price.value;
								break;
							}
						}
						break;
					}
				}
			}
			console.log("SPARICSCI !");
			$('#editProductModal').modal('hide');
			//initProductMode(prod);
			pvo.store();
			productViewObject.draw(pvo.currentSource);
		};

		showAModal(heading, body, confirmCallback)
		.then(() => {
			textIn.focus();
		});
	}

    async function uploadProductEdit(prod){
		    formData = new FormData();
			formData.append("name", prod.name);
			formData.append("price", prod.price);
			formData.append("categoryId", prod.categoryId);
			formData.append("description", prod.description);
			formData.append("deleted", prod.deleted);
			formData.append("options", JSON.stringify(prod.options));
			
			//si tratta di una creazione
			if(prod.id == null || prod.id <= 0){
				method = "POST";
				url = URLroot() + "service.php/products/";
				if(! prod.imgFile){
					alert("please provide an image");
					throw "imageMissing";
				}
			}else{
				method = "PUT";
				url = URLroot() + "service.php/products/" + prod.id + "/";
			}
			data = await ultraFetch(url,
                method ,
				"Basic " + sessionStorage.getItem("Authentication"),
				formData)
			json = await data.json();
			console.log(json)
			if(prod.imgFile){
				uploadProductEditImg(prod);
			}
			return json;
	}
	
	async function uploadProductEditImg(prod){
		    //image = document.getElementById("imgFileBuffer").files[0];
			image = dataURLtoFile(pvo.product.imgFile, prod.id + ".jpg")
			formData = new FormData();
			formData.append("image",image);
			//si tratta di una creazione
			
			if(prod.id == null){
				method = "POST";
				url = URLroot() + "service.php/products/";
			}else{
				method = "PUT";
				url = URLroot() + "service.php/products/" + prod.id + "/image/";
			}
			
			await ultraFetch(url, "PUT" , "Basic " + sessionStorage.getItem("Authentication"),	formData)
			.then(data => 
				data.json()
				.then()
			);
	}
</script>