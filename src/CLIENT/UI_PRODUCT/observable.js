product = {
	xId :	0 ,
	idListeners : [],
	xNome :	"jiao" ,
	nomeListeners : [],
	xCategoria :	"" ,
	categoriaListeners : [],
	xCategoriaId :	0 ,
	categoriaIdListeners : [],	
	xDescrizione :	"" ,
	descrizioneListeners : [],
	xIdVenditore : 0,
	idVenditoreListeners : [],
	xPrezzo :	-1 ,
	prezzoListeners : [],
	xEliminato : -1 ,
	eliminatoListeners : [],
	xImgSrc : "",
	imgSrcListeners : [],
	xSellerName : "" ,
	sellerNameListeners : [],
	xOptions : [],
	optionsListeners : [],
	
	clone : function(product) {
		console.log("cloning")
		console.log(product)
		for(attribute of Object.keys(product)){
			this[attribute] = product[attribute]
		}
	},
	
	get id(){
		return this.xId;
	},
	
	set id(val){
		this.xId = val;
		for(listener of this.idListeners)
			listener(val)
	},

	addIdListener : function(listener){
		this.idListeners.push(listener)
	},
	
	get nome(){
		return this.xNome;
	},
	
	set nome(val){
		this.xNome = val;
		for(listener of this.nomeListeners)
			listener(val)
	},
	
	addNomeListener : function(listener) {
		this.nomeListeners.push(listener)
	},

	get categoria(){
		return this.xCategoria;
	},
	
	set categoria(val){
		this.xCategoria = val;
		for(listener of this.categoriaListeners)
			listener(val)
	},
	
	addCategoriaListener : function(listener) {
		this.categoriaListeners.push(listener)
	},

	get categoriaId(){
		return this.xCategoriaId;
	},
	
	set categoriaId(val){
		this.xCategoriaId = val;
		for(listener of this.categoriaIdListeners)
			listener(val)
	},
	
	addCategoriaIdListener : function(listener) {
		this.categoriaIdListeners.push(listener)
	},

	get descrizione(){
		return this.xDescrizione;
	},
	
	set descrizione(val){
		this.xDescrizione = val;
		for(listener of this.descrizioneListeners)
			listener(val)
	},
	
	addDescrizioneListener : function(listener) {
		this.descrizioneListeners.push(listener)
	},

	get idVenditore(){
		return this.xIdVenditore;
	},
	
	set idVenditore(val){
		this.xIdVenditore = val;
		for(listener of this.idVenditoreListeners)
			listener(val)
	},
	
	addIdVenditoreListener : function(listener) {
		this.idVenditoreListeners.push(listener)
	},

	get prezzo(){
		return this.xPrezzo;
	},
	
	set prezzo(val){
		this.xPrezzo = val;
		for(listener of this.prezzoListeners)
			listener(val)
	},
	
	addPrezzoListener : function(listener) {
		this.prezzoListeners.push(listener)
	},

	get eliminato(){
		return this.xEliminato;
	},
	
	set eliminato(val){
		this.xEliminato = val;
		for(listener of this.eliminatoListeners)
			listener(val)
	},
	
	addEliminatoListener : function(listener) {
		this.eliminatoListeners.push(listener)
	},

	get imgSrc(){
		return this.xImgSrc;
	},
	
	set imgSrc(val){
		this.xImgSrc = val;
		for(listener of this.imgSrcListeners)
			listener(val)
	},
	
	addImgSrcListener : function(listener) {
		this.imgSrcListeners.push(listener)
	},

	get sellerName(){
		return this.xSellerName;
	},
	
	set sellerName(val){
		this.xSellerName = val;
		for(listener of this.sellerNameListeners)
			listener(val)
	},
	
	addSellerNameListener : function(listener) {
		this.sellerNameListeners.push(listener)
	},

	get options(){
		return this.xOptions;
	},
	
	set options(val){
		this.xOptions = val;
		for(listener of this.optionsListeners)
			listener(val)
	},
	
	addOptionsListener : function(listener) {
		this.optionsListeners.push(listener)
	}
}
{
	"id":6,
	"nome":"IPhoneX",
	"categoria":"Elettronica",
	"categoriaId":23,
	"descrizione":"apple inc",
	"idVenditore":7,
	"prezzo":700,
	"eliminato":0,
	"imgsrc":"6.jpg",
	"sellerName":"ESCOSHOP",
	"options":[
		{
			"idProdotto":6,
			"nome":"colore",
			"descrizione":"colore della placca posteriore in metallo",
			"prezzo":0,
			"obbligatoria":1,
			"tipo":"VALUE",
			"values":[
				{
					"idProdotto":6,
					"nome":"colore",
					"valore":"deep white",
					"prezzo":0
				},
				{
					"idProdotto":6,
					"nome":"colore",
					"valore":"FuScHiA",
					"prezzo":0
				},
				{
					"idProdotto":6,
					"nome":"colore",
					"valore":"vibrant red",
					"prezzo":0
				},
				{
					"idProdotto":6,
					"nome":"colore",
					"valore":"void black",
					"prezzo":0
				}
			]
		},
		{
			"idProdotto":6,
			"nome":"memoria interna",
			"descrizione":"spazio di archiviazione disponibile",
			"prezzo":0,
			"obbligatoria":1,
			"tipo":"VALUE",
			"values":[
				{
					"idProdotto":6,
					"nome":"memoria interna",
					"valore":"1 TB",
					"prezzo":200
				},
				{
					"idProdotto":6,
					"nome":"memoria interna",
					"valore":"1.5 TB",
					"prezzo":400
				},
				{
					"idProdotto":6,
					"nome":"memoria interna",
					"valore":"256 GB",
					"prezzo":0
				},
				{"idProdotto":6,
				"nome":"memoria interna",
				"valore":"512 GB",
				"prezzo":100
				}
			]
		}
	]
}