blurWidth = "10px";
blurWidth2 = "20px";
						
productViewObject = {
	isOpen : false,
	
	product : null,
	
	currentSource : null,
	
	productView : document.getElementById("productview"),
	mainView : document.getElementsByTagName("main")[0],
	
	items : {
		nameItem : { element :  document.getElementById("productName"),
					init : () => {	
						pvo = productViewObject
						me = pvo.items.nameItem
						me.element.innerText = "NOME PRODOTTO"
						me.element.style.textShadow = ""
						me.element.onclick = ""
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.nameItem
						if(editMode){
							me.element.innerText = pvo.product.name;
							me.element.style.textShadow = "#8f8 0 0 " + blurWidth;
							me.element.onclick = () => { showProductEditNameModal(productViewObject.product); }
						}else{
							me.element.innerText = pvo.product.name;
						}
					}
				},
			
		priceItem : { element : document.getElementById("productPrice"),
					init : () => {
						pvo = productViewObject
						me = pvo.items.priceItem
						me.element.innerText = "PREZZO PRODOTTO"
						me.element.style.textShadow = ""
						me.element.onclick = ""
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.priceItem
						if(editMode){
							me.element.innerText = pvo.product.price + "$";
							me.element.style.textShadow = "#8f8 0 0 " + blurWidth;
							me.element.onclick = () => { showProductEditPriceModal(productViewObject.product); }
						}else{
							price = pvo.product.price;
							for(option of pvo.product.options){
								if(option.selected){
									price += option.price;
									if(option.type == "VALUE"){
										for(value of pvo.product.options[pvo.product.options.indexOf(option)].values){
											if(value.value == option.value){
												price += value.price;
												break;
											}
										}
									}
								}
							}
							me.element.innerText = price + "$";
						}
					}
				},
		catItem : {	element :  document.getElementById("productCategory"),
					init : () => {
						pvo = productViewObject
						me = pvo.items.catItem
						me.element.innerText = "CATEGORIA PRODOTTO"
						me.element.style.textShadow = ""
						me.element.onclick = ""
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.catItem
						if(editMode){
							me.element.innerText = pvo.product.categoryName;
							me.element.style.textShadow = "#8f8 0 0 " + blurWidth;
							me.element.onclick = (e) => { e.preventDefault(); showProductEditCategoryModal(productViewObject.product); }
						}else{
							me.element.href = URLroot() + "ui_2.php/products/categories/" + pvo.product.categoryName + "/";
							me.element.innerText = pvo.product.categoryName;	
						}
					}
				},
		descItem : { element : document.getElementById("productDescription"),
					init : () => {
						pvo = productViewObject
						me = pvo.items.descItem
						me.element.innerText = "DESCRIZIONE PRODOTTO"
						me.element.style.textShadow = ""
						me.element.onclick = ""
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.descItem
						if(editMode){
							me.element.innerText = pvo.product.description;
							me.element.style.textShadow = "#8f8 0 0 " + blurWidth;
							me.element.onclick = () => { showProductEditDescriptionModal(productViewObject.product); }
						}else{
							me.element.innerText = pvo.product.description;
						}
					}
				},
		sellerItem : { element  : document.getElementById("productSeller"),
					init : () => {
						productViewObject.items.sellerItem.element.innerText =  "NOME VENDITORE";
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.sellerItem
						me.element.innerText = pvo.product.sellerName;
						me.element.href = URLroot() + "ui_2.php/users/" + pvo.product.sellerId  + "/products/";
					}
				},
				
		imgItem : {	element : document.getElementById("productImg"),
					init : () => {
						pvo = productViewObject
						me = pvo.items.imgItem
						me.element.style.boxShadow = "";
						me.element.onclick = "";
						me.element.src = URLbase() + "/upload/screen-white.png";
						me.element.alt = "PRODUCT CREATION IMAGE PLACEHOLDER";
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.imgItem
						if(editMode){
							pvo.product = JSON.parse(sessionStorage.product)
							me.element.style.boxShadow = "#8f8 0 0 " + blurWidth2;
							me.element.onclick = () => { showProductEditImageModal(productViewObject.product); };
							//se hai salvato l'immagine
							if(pvo.product.imgFile){
								me.element.src = URL.createObjectURL( dataURLtoFile( pvo.product.imgFile, "imgHERRO.jpg" ) )
							}else{
								me.element.src = URLbase() + "upload/products/" + pvo.product.imgsrc;		
								me.element.alt = "PRODUCT DISPLAY IMAGE";
								imgFileFrom(URLbase() + "upload/products/" + pvo.product.imgsrc)
								.then(blob => toBase64(blob))
								.then(tex => {
									pvo.product.imgFile = tex
									pvo.store();
								});
							}
							//in userMode l'immagine viene sempre scaricata
						}else{
							me.element.src = URLbase() + "upload/products/" + pvo.product.imgsrc;		
							me.element.alt = "PRODUCT DISPLAY IMAGE";
							imgFileFrom(URLbase() + "upload/products/" + pvo.product.imgsrc)
							.then(blob => toBase64(blob))
							.then(tex => {
								pvo.product.imgFile = tex
								pvo.store();
							});
						}
					}
				},	
		reviewsItem : {	element : document.getElementById("productReviews"),
					init : () => {
						productViewObject.items.reviewsItem.element.innerHTML = "HERE COMMENTS"
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.reviewsItem;
						if(editMode){
							
						}else{
							me.element.innerHTML = "<li class='list-group-item' style='list-style: none'>nessuna recensione</li>";
							ultraFetch(URLroot() + "service.php/products/" + pvo.product.id + "/reviews/", "GET", "Basic " + sessionStorage.getItem("Authentication"))
								.then(response => {					
									response.json()
										.then(data => {
											if(data.canIReview){
												pvo.items.reviewBut.element.classList.remove("d-none");
												pvo.items.reviewBut.element.disabled = false;
												pvo.items.reviewBut.element.onclick = () => {
													showReviewModal(pvo.product.id);
												}
											}
											if(data.entries.length > 0)
												me.element.innerHTML = "";
											for(entry in data.entries)
												reviewLI(data.entries[entry]);	
										});
									});
						}
					}
				},	
				
		buyBut : {	element : document.getElementById("addCartBut"),
					init : () => {
						pvo = productViewObject
						me = pvo.items.buyBut
						me.element.classList.remove("d-block");
							if(! me.element.classList.contains("d-none"))
								me.element.classList.add("d-none");
							me.element.onclick = "";
							me.element.disabled = true;
							me.element.style.boxShadow = "";						
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.buyBut
						if(editMode){
							me.element.classList.remove("d-none")
							me.element.style.boxShadow = "#8f8 0 0 " + blurWidth;
							me.element.disabled = false;
							if(pvo.product.deleted){
								me.element.innerText = "FUORI VENDITA" ;
								me.element.classList.replace("border-primary","border-danger");
							}else{
								me.element.innerText = "IN VENDITA";
								me.element.classList.replace("border-danger","border-primary");
							}
							me.element.innerText = (pvo.product.deleted) ? "FUORI VENDITA" : "IN VENDITA";
							me.element.onclick = toggleBuy;
						}else{
							user = getUser();				
							if(user != null && user.type == "SELLER"){				
								if(isMyProduct(pvo.product)){		
									
								}else{
		
								}							
								if(pvo.product.deleted){						
									me.element.innerText = "FUORI VENDITA";
									me.element.disabled = true;
									me.element.classList.replace("border-primary","border-danger");
																		
								}else{
									me.element.classList.replace("border-danger","border-primary");
									me.element.innerText = "IN VENDITA";
									me.element.disabled = true;
								}
								me.element.classList.replace("d-none", "d-block");
												
							}else{		
								me.element.classList.replace("d-none" , "d-block");		
								if(pvo.product.deleted){
									me.element.innerText = "Fuori vendita";
									me.element.classList.replace("border-primary","border-danger");
									me.element.disabled = true;	
								}else{
									me.element.disabled = false;
									me.element.innerText = "aggiungi al carrello";
									me.element.classList.replace("border-danger","border-primary");
									me.element.onclick = () => {
										prod = JSON.parse(JSON.stringify(pvo.product));
										for(opt of prod.options){
											if(! opt.selected){
												prod.options.splice(prod.options.indexOf(opt),1)
											}
										}
										pvo.product.quantity = 1;
										addToCart(pvo.product)
										onceAnimationEnd(me.element, "fadingBlue").then(() => {me.element.classList.remove("fadingBlue")})		
									};
								}
							}
						}
					}
				},
		editBut : {	element : document.getElementById("editProdBut"),
					init : () => {
						pvo = productViewObject
						me = pvo.items.editBut
						me.element.classList.remove("d-block");
							if(! me.element.classList.contains("d-none"))
								me.element.classList.add("d-none");
							me.element.onclick = "";
							me.element.disabled = true;
							me.element.style.boxShadow = "";						
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.editBut
						if(editMode){
							me.element.innerText = "Done";
							me.element.disabled = false;
							me.element.classList.remove("d-none");
							me.element.onclick = () => { 
								showProductConfirmSendModal(productViewObject.product);
							}
						}else{
							me.element.innerText = "Edit product";	
							user = getUser();				
							if(user != null && user.type == "SELLER"){				
								if(isMyProduct(pvo.product)){		
									me.element.classList.replace("d-none","d-block");
									me.element.disabled = false;
									me.element.onclick = () => { 
										cambiaStato("products/" + pvo.product.id + "/edit/");
									};
								}else{
									me.element.classList.replace("d-block","d-none");
									me.element.disabled = true;
								}				
							}else{		
								me.element.classList.replace("d-block" , "d-none");
								me.element.disabled = true;			
							}
						}
					}
				},
		reviewBut : {	element : document.getElementById("addReviewBut"),
					init : () => {
						pvo = productViewObject
						me = pvo.items.reviewBut
						me.element.classList.remove("d-block");
							if(! me.element.classList.contains("d-none"))
								me.element.classList.add("d-none");
							me.element.onclick = "";
							me.element.disabled = true;
							me.element.style.boxShadow = "";						
					},
					draw : (mode) => {}
				}

	},
				
	open :  function(){
		pvo = productViewObject;
		pvo.productView.classList.remove("d-none");
		onceAnimationEnd(pvo.productView, "swipeIn")
		.then(()=>{
			pvo.mainView.style.overflow = "auto";
			views.active = pvo;
			pvo.productView.ariaHidden = false;
			pvo.isOpen = true;
		});
	},
	
	close :  async function(){
		pvo = productViewObject;
		if(pvo.isOpen){
			return new Promise(resolve => {
				pvo.mainView.style.overflow = "auto";
				pvo.mainView.style.height = "";
			    onceAnimationEnd(pvo.productView, "swipeOut")
				.then(()=>{
					 pvo.productView.classList.add("d-none");
					 pvo.productView.classList.remove("swipeOut");
					 pvo.isOpen = false;
					 pvo.productView.ariaHidden = true;
					 views.active = null;
					 resolve();	
				 });	
			});							
		}else{
			return new Promise(resolve => resolve());
		}
	},

	init : function(){ 
		pvo = productViewObject;
		for(item in pvo.items){
			pvo.items[item].init();
		}
		//tutti i campi vuoti e senza callback da click o ombreggiature
		
 	},    //resetta la vista al suo stato blank

	draw : async function(source){
		pvo = productViewObject;
		pvo.currentSource = source;
		//["products","xxx","sub"]
		pieces = source.split("/");
		id = pieces[1];
		sub = pieces[2];
		switch(id){
			//special key NEW (no id existing)
			case "new":
				console.log("drawing newProduct")
				pvo.product = JSON.parse(sessionStorage.product);
				for(item in pvo.items){
					pvo.items[item].draw(true);
				}
			break;
			default :
				switch(sub){
					case "edit":
						console.log("drawing editProduct");
						loadedFromMem = false;
						if(sessionStorage.product){
							storedProduct = JSON.parse(sessionStorage.product);
							if(storedProduct.id == id){
								pvo.product = storedProduct;
								loadedFromMem = true;
								console.log("loaded from mem")
							}
						}
						if(!loadedFromMem){
								await ultraFetch(URLroot() + "service.php/" + pieces[0] + "/" + pieces[1], "GET")
								.then(response => response.json())
								.then(data => {
									sessionStorage.setItem("product", JSON.stringify(data));
									pvo.product = data;			
								});
						}
						for(item in pvo.items){
							pvo.items[item].draw(true);
						}
					break;
					default:
						pvo.init();
						console.log("loaded from net")
						ultraFetch(URLroot() + "service.php/" + source, "GET")
						.then(response => {	
							response.json()
							.then(data => {
								sessionStorage.setItem("product", JSON.stringify(data));
								pvo.product = data;
								pvo.quantity = 1;
								for(item in pvo.items){
									pvo.items[item].draw(false);
								}				
							});
						});					
					break;
				}
			break;
		}

	},

	store : function(){
		sessionStorage.setItem("product", JSON.stringify(pvo.product))
	}
};
	
	function createProduct(){
		for(view in views.list){
			views.list[view].close();
		}
		productViewObject.init();

		productViewObject.open();
		productViewObject.product =	{ categoryName: "PLACEHOLDER",
									  categoryId: -1,
									  description : "descrizone finta",
									  deleted: false,
									  id: -1,
									  imgsrc: URLbase() + "/upload/screen-white.png",
									  name : "nomePlaceholder",
									  price: -1,
									  sellerId : getUser().id ,
									  sellerName : getUser().name,
									  options : [] 
									} ;	
		console.log(productViewObject.product.imgsrc)
		imgFileFrom(productViewObject.product.imgsrc)
		.then(blob => toBase64(blob))
		.then(tex => { 
			pvo.product.imgFile = tex
			pvo.store();
			cambiaStato("products/new/");
		});

	}
	
	function isMyProduct(prodData){
		user = getUser();
		return user.id == prodData.sellerId;
	}
			
			
    function reviewLI(review){
				ultraFetch(URLroot() + "service.php/users/" + review.userId + "/", "GET", "Basic " + sessionStorage.getItem("Authentication"))
				.then(data => 
				    data.json()
					.then(usr => {//review.oscurata è ignorato per ora
						li = document.createElement("li");
							li.classList.add("container-fluid" , "bg-light" , "list-group-item");
							secGen = document.createElement("section");
								secGen.classList.add("row");
								pRate = document.createElement("p");
									pRate.classList.add("col" , "mb-0");
									stars = "";
									i = 0;
									for( ; i < review.rating; i++){
										stars+="★";
									}
									for( ; i < 5; i++){
										stars+="☆";
									}
									pRate.innerText = stars;
								pUser = document.createElement("p");
									pUser.classList.add("col" , "mb-0");
									pUser.innerText = "voto da : " + usr.name;
								secGen.appendChild(pRate);
								secGen.appendChild(pUser);
							secRev = document.createElement("section");
								secRev.classList.add("row");
								pText = document.createElement("p");
									pText.classList.add("col");
									pText.innerText = review.text;
								secRev.appendChild(pText);
						li.appendChild(secGen);
						li.appendChild(document.createElement("hr"));
						li.appendChild(secRev);
						pvo.reviewsItem.appendChild(li);
				    })
				);
			}

			function toggleBuy(){
				el = pvo.items.buyBut.element
				pvo.product.deleted = !pvo.product.deleted;
				pvo.store();
				pvo.draw(pvo.currentSource)
			}
