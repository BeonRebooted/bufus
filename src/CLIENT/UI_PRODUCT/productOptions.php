<br class="my-5">
<?php require "./client/ui_PRODUCT/SellerProductOption.php" ?>
<?php require "./client/ui_PRODUCT/ClientProductOption.php" ?>
<div id="productOptions">
</div>

<script>
	window.addEventListener("load", () => {
		productViewObject.items.optionsItem = {	element : document.getElementById("productOptions"),
					init : () => {
						pvo = productViewObject
						me = pvo.items.optionsItem
						me.element.innerHTML = ""
					},
					draw : (editMode) => {
						pvo = productViewObject
						me = pvo.items.optionsItem
						me.element.innerHTML = "";
						
						if(editMode){
							reinsertOptionsControls();
							data = productViewObject.product.options;
							for(entry in data){
								a = new SellerProductOption(pvo.product, data[entry]);
								pvo.items.optionsItem.element.appendChild(a);	
								a.editMode = true;
																
							}
						}else{
							ultraFetch(URLroot() + "service.php/products/" + pvo.product.id + "/options/", "GET", "Basic " + sessionStorage.getItem("Authentication"))
								.then(response => {					
									response.json()
										.then(data => {
											if(data.length > 0){
												pvo.product.options = data;
												for(entry in data){
													a = new ClientProductOption(pvo.product, data[entry])
													pvo.items.optionsItem.element.appendChild(a);	
													a.editMode = false;
												}
											}
										});
								});
						}
					}
				}	

			function reinsertOptionsControls(){
				ctrls = ELFromTXT("<div id='optionsControls' class='row no-gutters'></div>");
				clearAll = ELFromTXT("<button class='col-1 btn form-control form-control-sm border-primary'>clr</button>");
				clearAll.onclick = () => { 
											showAModal( "si stanno eliminando tutte le opzioni",
													    ELFromTXT("<p>sei sicuro?</p>"),
													    ()=> { 
															pvo.product.options = [];
															sessionStorage.setItem("product", JSON.stringify(pvo.product));
															pvo.draw(pvo.currentSource) 
														} 
													  )
										 }
				ctrls.appendChild(clearAll);
				searchBar = ELFromTXT("<input type='text' class='col form-control form-control-sm'>");
				ctrls.appendChild(searchBar);
				addOne = ELFromTXT("<button class='col-1 btn  form-control form-control-sm border-primary'>+</button>");
				addOne.onclick = () => { 
					bodi = ELFromTXT("<div class='row'></div>");
					tex = ELFromTXT("<input type='text' class='col-8 form-control' id='newOptionName'>");
					bodi.appendChild(ELFromTXT("<label class='col-4' for='newOptionName'>nome</label>"));
					bodi.appendChild(tex);
					showAModal( "NUOVA OPZIONE",
							   bodi,
								()=> { 
			 							pvo.product.options.push({ productId: pvo.product.id , name: tex.value, description: "", price: 0, required: 0, type: "VALUE", values: [] });
										sessionStorage.setItem("product", JSON.stringify(pvo.product));
										pvo.draw(pvo.currentSource) 
									} 
							  )
				}
				ctrls.appendChild(addOne);
				
				productViewObject.items.optionsItem.element.appendChild(ctrls);
			}
	
	});

</script>