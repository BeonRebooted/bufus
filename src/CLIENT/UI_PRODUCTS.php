<section id="productsview" class="d-none bg-white container-fluid" role="tabpanel" aria-label="products view">
	<div class="row">
	    <!--div class="col-2 d-none d-md-block"></div-->
		<section id="productsViewContents" class="col py-3">
			<ul id="products" class="pl-0 row" style="list-style: none">
			</ul>
		</section>
		<!--div class="col-2 d-none d-md-block"></div-->
	</div>
</section>
			
<script>
	document.getElementById("productsCreateProduct").onclick = () => { createProduct() };
	document.getElementById("productsSearch").oninput = () => { productQuerySuggestions(document.getElementById("productsSearch").value); };
	document.getElementById("searchCategory").onkeydown = (evt) => {
		if(evt.code == "Enter"){
			cat = document.getElementById("searchCategory").value;
			(cat != "")? productsFilterByCategory(cat) : cambiaStato("products/");
		}
	}
	
	document.getElementById("productsSearch").onkeydown = (evt) => {
		if(evt.code == "Enter"){
			evt.preventDefault();
			console.log("aggiungere in seguito la ricerca per nome sui prodotti di un solo venditore");
			cambiaStato("products/name/" + document.getElementById("productsSearch").value + "/");
		}
	}
			
</script>
			
<script>

    productsViewObject = {
		
		isOpen : false,
		onlyMyProductsBut : document.getElementById("productsToggleOnlyUser"),
	    onlyMyProductsView : document.getElementById("productsToggleOnlyUserView"),
		sellerControls : document.getElementById("productsViewSellerControls"),
		addProductBut : document.getElementById("productsCreateProduct"),
		addProductView: document.getElementById("productsCreateProductView"),
		searchName: document.getElementById("search"),
		searchCat: document.getElementById("searchCategory"),
		searchBar: document.getElementById("productsViewHeader"),
		
		open :  function(){ 
		                    pvo = productsViewObject;
		                    productsView = $("#productsview")[0];
							productsView.classList.remove("d-none");
                            productsViewObject.searchBar.classList.remove("d-none");
							onceAnimationEnd(productsView, "swipeIn")
							.then(()=>{
								mainView = $("main")[0];  
								productsView.classList.remove("swipeIn");
								mainView.style.overflow = "auto";
								productsViewObject.isOpen = true;
								productsView.ariaHidden = false;
								views.active = pvo;
							});	
				},
					  
		close : async function(){
			                 pvo = productsViewObject;
			                 if(pvo.isOpen){
							    return new Promise(resolve => {
								    productsView = $("#productsview")[0];
									mainView = $("main")[0];  
									mainView.style.overflow = "auto";
									mainView.style.height = "";
									onceAnimationEnd(productsView, "swipeOut")
								     .then(()=>{
										 productsView.classList.add("d-none");
										 productsViewObject.searchBar.classList.add("d-none");
										 productsView.classList.remove("swipeOut");
										 productsViewObject.isOpen = false;	
										 views.active = null;
										 productsView.ariaHidden = true;
										 resolve();	
									 });	
								 });
							 }else{
								 return new Promise(resolve => resolve());
							 }				
		              },
					  
		init : function(){ 
			$("#products")[0].innerHTML = "";
			pvo = productsViewObject;
			pvo.onlyMyProductsBut.disabled = false;
			pvo.onlyMyProductsBut.checked = false;
			pvo.addProductBut.disabled = false;
			pvo.sellerControls.classList.remove("d-none");
			pvo.onlyMyProductsView.classList.remove("d-none");
			pvo.addProductView.classList.remove("d-none");
			document.getElementById("productsSearch").oninput = "";
			ultraFetch(URLroot() + "service.php/products/categories/", "GET")
				.then((categories) => {
					categories.json()
						.then(entries => {
							pvo.searchCat.innerText = "";
							//ne crei uno blank
							opt = document.createElement("option");
							opt.value = "";
							opt.selected = true;
							opt.innerText = "ALL";
							opt.style.borderTop = "1px solid black";
							opt.onclick = (e)=>{e.preventDefault()};
							pvo.searchCat.appendChild(opt);
											
											
											
							for(category of entries){
								opt = document.createElement("option");
								opt.value = category.name;
								opt.innerText = category.name;
								opt.style.borderTop = "1px solid black";
								pvo.searchCat.appendChild(opt);
							}
							pvo.searchCat.childNodes[0].style.borderTop = "0px";
						});					
				});
        },    //resetta la vista al suo stato blank
					 
		draw : function(source){
			productsViewObject.init();
		    ultraFetch(URLroot() + "service.php/" + source,"GET")
				.then(response => {
				    response.json()
						.then(data => {
							if(data.length == 0){
								el = document.createElement("li");
								el.classList.add("mx-auto");
								el.classList.add("text-center");
								el.innerHTML = "<br/><br/><br/><br/><br/><br/><br/>NESSUN PRODOTTO DA VISUALIZZARE"
								document.getElementById("products").appendChild(el);								
							}
				            for(i = 0; i < data.length; i++){
				                prod = data[i];
				                el = getLIproduct(prod);
				                document.getElementById("products").appendChild(el);
				            }
				        })
						.catch((e) => {
							//niente dati
							console.log("!!!!!!! ERROR !!!!!!!");
							//console.log(e)
							el = document.createElement("li");
							el.classList.add("mx-auto");
							el.classList.add("text-center");
							el.innerHTML = "<br/><br/><br/><br/><br/><br/><br/>NESSUN PRODOTTO DA VISUALIZZARE"
							document.getElementById("products").appendChild(el);
						});
				});
			user = getUser();
			if(user.type == "USER"){
				pvo.sellerControls.classList.add("d-none");
			    pvo.onlyMyProductsBut.disabled = true;
				pvo.onlyMyProductsView.classList.add("d-none");
				pvo.addProductBut.disabled = true;
				pvo.addProductView.classList.add("d-none");
		    }else{
				pvo.sellerControls.classList.remove("d-none");
				pvo.onlyMyProductsBut.disabled = false;
				pvo.onlyMyProductsBut.onclick = toggleOnlyUserProducts;
				pvo.onlyMyProductsView.classList.remove("d-none");
				pvo.addProductBut.disabled = false;
				pvo.addProductView.classList.remove("d-none");
			}
		}   //disegna la vista secondo lo stato corrente
	};


	function getLIproduct(product){
		el = document.createElement("LI");
		el.classList.add( "col-12" , "col-md-4" , "col-lg-3" , "p-2" , "my-2" );
        el.innerHTML = 
			`<article class="row no-gutters border align-items-end bg-light container-fluid h-100" aria-label="product">
		        <div class="col-12 py-3 my-auto text-center" role="presentation">
			        <img class="img-fluid m-0 pr-md-2"  style="max-height: 100%;" src="${ URLbase() }upload/products/${ product.imgsrc }" alt="PRODUCT DISPLAY IMAGE">
			    </div>
			    <div class="col mx-2" role="presentation">
				    <div class="row no-gutters">
					    <a href="${ URLroot() }ui_2.php/products/${ product.id }/" tabindex=0><h4> ${product.name}</h4></a>
					</div>
				    <div class="row no-gutters">
					    <div class="col-6"><a style="float: bottom;" aria-label="seller: " href="${ URLroot() }ui_2.php/users/${ product.sellerId }/products/" tabindex=0>${ product.sellerName }</a></div>
                        <div class="col-6 text-right"><p class="mb-2" style="font-size: 18px;">${ product.deleted ? 'fuori vendita' : product.price + "$" }</p></div>
					</div>
				</div>
		    </article>
		`;
		el.getElementsByTagName("img")[0].onclick = () => { cambiaStato(`products/${ product.id }/`)};
		el.onkeydown = (key)=> { if (key.code == "Enter") cambiaStato(`products/${ product.id }/`) };
        return el;		
	}
	

		
			
	function toggleOnlyUserProducts(){
		if(productsViewObject.onlyMyProductsBut.checked){
			productsViewObject.draw("users/" + getUser().id + "/products/");
		}else{
			productsViewObject.draw("products/");
		}

	}
			
			
	function productsFilterByCategory(input){
		if(productsViewObject.onlyMyProductsBut.checked){
			//productsViewObject.draw("users/" + getUser().id + "/products/categories/" + input + "/");
			cambiaStato("users/" + getUser().id + "/products/categories/" + input + "/");
		}else{
			cambiaStato("products/categories/" + input + "/");
			//productsViewObject.draw("products/categories/" + input + "/");
		}		
	}
			

</script>