
	<section id="registerview" class="d-none" role="tabpanel" aria-label="registration view">
	    <div class="container-fluid">
		    <div class="row">
			    <div class="col-3 d-none d-md-block">
	    		</div>
		        <div id="pageContent" class="col" role="presentation">
				    <h1 class="mt-5 text-center">Registrati su BUFUs</h1>
				    <p class="text-center">crea un account cliente oppure venditore</p>
					<! FORM>
					<form style="margin-top: 15vh">
					    <! NAME>
					    <label for="register-name">Name</label>
					    <input type="text" class="form-control" id="register-name" name="name">
						<br/>
                        <! MAIL>					   
					    <label for="register-email">Email</label>
					    <input type="email" class="form-control" id="register-email" name="email">
						<br/>
						<! PASSWORD>
						<label for="register-password">Password</label>
						<input type="password" class="form-control" id="register-password" name="password">
						<br/>
						<! PASSWORD REPEAT>
						<label for="register-repeat">Repeat Password</label>
						<input type="password" class="form-control" id="register-repeat" name="passwordRepeat">
						<br/>
						<br/>
						<! USER TYPE>
						<div class="row">
						    <div class="col text-center" role="presentation">
							    <input type="radio" class="form-control" id="register-client" name="userType" value="basic" checked><label class="mt-3" for="register-client">Utente Cliente</label>
						    </div>
							<div class="col text-center" role="presentation">
						        <input type="radio" class="form-control" id="register-seller" name="userType" value="seller"><label class="mt-3" for="register-seller">Utente Venditore</label>
							</div>
						</div>
						<br/>
						<button id="registerButton" class="btn bg-primary text-light" style="margin-top: 5vh">registrati</button>
					</form>
			    </div>
				<div class="col-3 d-none d-md-block">
	    		</div>
			</div>
		
		</div>
		<script>
    registerViewObject = {
		isOpen : false,
		
		open :  function(){
							registerView = $("#registerview")[0];
							mainView = $("main")[0];  
							registerView.classList.remove("d-none");
							onceAnimationEnd(registerView, "swipeIn")
							.then(()=>{
								mainView.style.overflow = "auto";
								views.active = registerViewObject;
								registerViewObject.isOpen = true;
								registerView.ariaHidden = false;
							});
						  },
					  
		close : async function(){
			if(registerViewObject.isOpen){
			    return new Promise(resolve => {
				   registerView = $("#registerview")[0];
					mainView = $("main")[0];  
					mainView.style.overflow = "auto";
					mainView.style.height = "";
					 onceAnimationEnd(registerView, "swipeOut")
				     .then(()=>{
						 registerView.classList.add("d-none");
						 registerView.classList.remove("swipeOut");
						 registerViewObject.isOpen = false;
						 registerView.ariaHidden = true;
						views.active = null;						 
						 resolve();	
					 });	
				});				
			}else{
				return new Promise(resolve => resolve());
			}		
        },
					  
		init : function(){ registerView = $("#registerview")[0];
			$("#register-name")[0].value = "";
			$("#register-email")[0].value = "";
			$("#register-password")[0].value = "";
			$("#register-repeat")[0].value = "";
			$("#register-client")[0].checked = true;

			rvo = registerViewObject;

			
   			document.getElementById("register-name").oninput = rvo.checkNome;
			document.getElementById("register-email").oninput = rvo.checkEmail;
			document.getElementById("register-password").oninput = rvo.checkPassword;
			document.getElementById("register-repeat").oninput = rvo.checkRepeat;
			document.getElementById("registerButton").onclick = rvo.invia;
		},    //resetta la vista al suo stato blank
					 
		draw : function(source){ registerViewObject.init(); },
		
		clearValidations(sub){
			target = document.getElementById("register-" + sub);
			target.classList.remove("is-valid" , "is-invalid");
			for(el of document.querySelectorAll("[id^='register-"+ sub +"-']"))
				el.parentNode.removeChild(el)
		},
		
		validate(sub,message){
			rvo = registerViewObject;
			rvo.clearValidations(sub);
			target = document.getElementById("register-" + sub);
			target.classList.add("is-valid");
			feedback = document.createElement("div");
			feedback.id = "register-" +  sub + "-valid";
			feedback.classList.add("valid-feedback" , "d-block");
			feedback.innerHTML = message;
			target.after(feedback);
		},
		
		invalidate(sub,message){
			rvo = registerViewObject;
			rvo.clearValidations(sub);
			target = document.getElementById("register-" + sub);
			target.classList.add("is-invalid");
			feedback = document.createElement("div");
			feedback.id = "register-" +  sub + "-invalid";
			feedback.classList.add("invalid-feedback" , "d-block");
			feedback.innerHTML = message;
			target.after(feedback);
		},
	
		checkNome(){
			rvo = registerViewObject;
			name = document.getElementById("register-name").value;
			if(name.length == 0){
				rvo.invalidate("name","please provide a name");
				return false;
			}else{
				rvo.validate("name", "")
				return true;
			}
				
		},
		
		checkEmail(){
				rvo = registerViewObject;
				mail = document.getElementById("register-email").value;
				if(mail.match(/\w+\@\w+\.\w+/) == null){
					rvo.invalidate("email","please provide an email");
					return false;
				}else{
					rvo.validate("email", "")
					return true;
				}
		},
		
		checkPassword(buddy){
				rvo = registerViewObject;
				if(buddy)
					rvo.checkRepeat(false);
				pwd = document.getElementById("register-password").value;
				is5charsOrMore = ( pwd.length >= 5 );
				containsAnyDigit = /\d/.test(pwd);
				 if(!is5charsOrMore || !containsAnyDigit){
					 rvo.invalidate("password", "at least 5 characters including a digit")
			         return false;
				 }else{
					 rvo.validate("password", "")
					 return true;
				 }
		},
		
		checkRepeat(buddy){
				rvo = registerViewObject;
				if(buddy)
					rvo.checkPassword(false);
				pwd = document.getElementById("register-password").value;
				pwdRep = document.getElementById("register-repeat").value;
				if(pwd != pwdRep){
					rvo.invalidate("repeat", "repeat")
					return false;
				}else{
					rvo.validate("repeat", "")
					return true;
				}		
		},
		
		invia(e){
				e.preventDefault();
				rvo = registerViewObject;
				if(rvo.checkNome() & rvo.checkEmail() & rvo.checkPassword(false) & rvo.checkRepeat(false)){
					httpRequest = new XMLHttpRequest();
					httpRequest.onreadystatechange = function(){
        	  	        if(httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {
        	  		        if( !document.getElementById("pageContent").classList.contains("bg-primary")){
								document.getElementById("pageContent").classList.add("bg-primary");
							}	  		
        	            }
						console.log(httpRequest.response);	
        	        }
        	        httpRequest.open('POST', 'http://localhost/progettoscuola/src/service.php/users/', true);
					formData = new FormData();
                    formData.append("email", document.getElementById("register-email").value);
                    formData.append("password", document.getElementById("register-password").value);
					formData.append("name", document.getElementById("register-name").value);
					if(document.getElementById("register-client").checked){
						formData.append("type", "USER");
					}else{
						formData.append("type", "SELLER");
					}
					httpRequest.send(formData);
				}else{
					console.log("eggià")
				}
		}
	};
		</script>
	</section>    
