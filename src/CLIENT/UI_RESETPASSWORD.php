
	<section id="resetpasswordview" class="d-none bg-white" style="z-index: 10000; position: absolute; top: 0; width: 100vw;" role="tabpanel" aria-label="reset password view"> 
	    <div class="container-fluid">
		    <div class="row">
			    <div class="col-3 d-none d-md-block">
	    		</div>
		        <div class="col">
				    <h1 class="my-5 text-center">Inserisci la nuova password</h1>
                    <form class="mt-4">
					    <input type="hidden" id="tokenId" value="" name="tokenId">
					    <label for="resetPassword">New password</label>
					    <input type="password" class="form-control" id="resetPassword" name="password">
						<div id="resetPassword-invalid" class="invalid-feedback d-none">
                            la password deve essere almeno 5 caratteri di cui un numero
                        </div>
						<label for="resetRepeatPassword">Repeat password</label>
					    <input type="password" class="form-control" id="resetRepeatPassword" name="repeatPassword">
						<div id="resetRepeat-invalid" class="invalid-feedback d-none">
                            scriviDueVolteLaStessa
                        </div>
						<button id="resetSubmit" class="btn bg-primary text-light mt-4">Reimposta</button>
					</form>
					<div id="resetResult" class="text-center">
					    <img class="img-fluid rounded-circle" src="http://localhost/progettoscuola/upload/jodorowski300_deepYellow.png" alt="reset pending">
					    <p>in realtà sarà la spunta che appare dopo l'invio della mail</p>
					</div>
			    </div>
				<div class="col-3 d-none d-md-block">
	    		</div>
			</div>
		
		</div>
		<script>
		
		
		
   resetPasswordViewObject = {
		isOpen : false,
		
		open :  async function(){
							resetPasswordView = $("#resetpasswordview")[0];
							mainView = $("main")[0];  
							resetPasswordView.classList.remove("d-none");
							var a = await onceAnimationEnd(resetPasswordView, "swipeIn");
							mainView.style.overflow = "hidden";
							views.active = resetPasswordViewObject;
							resetPasswordViewObject.isOpen = true;
							resetPasswordView.ariaHidden = false;
							//});
				 },
					  
		close : async function(){
			if(resetPasswordViewObject.isOpen){
			    return new Promise(resolve => {
				   resetPasswordView = $("#resetpasswordview")[0];
					mainView = $("main")[0];  
					mainView.style.overflow = "auto";
					mainView.style.height = "";
					 onceAnimationEnd(resetPasswordView, "swipeOut")
				     .then(()=>{
						 resetPasswordView.classList.add("d-none");
						 resetPasswordView.classList.remove("swipeOut");
						 resetPasswordViewObject.isOpen = false;
						 resetPasswordView.ariaHidden = true;
						views.active = null;						 
						 resolve();	
					 });	
				});				
			}else{
				return new Promise(resolve => resolve());
			}		
        },
					  
		init : function(){ 
			resetPasswordView = $("#resetpasswordview")[0];
		  
			$("#tokenId")[0].value = "";
			$("#resetPassword")[0].value = "";
			$("#resetRepeatPassword")[0].value = "";
		},    //resetta la vista al suo stato blank
					 
		draw : function(source){ 
			resetPasswordViewObject.init();
			$("#tokenId")[0].value = source.split("/")[1];
		}
		
	};
		
	function validaRepeat(){
		pwd = document.getElementById("resetPassword").value;
				pwdRep = document.getElementById("resetRepeatPassword").value;
				if(pwd != pwdRep){
					hidden = document.getElementById("resetRepeat-invalid").classList.contains("d-none");
					if(hidden){
						document.getElementById("resetRepeat-invalid").classList.toggle("d-none");
						document.getElementById("resetRepeatPassword").classList.toggle("is-invalid");
					}
					return false;
				}else{
					hidden = document.getElementById("resetRepeat-invalid").classList.contains("d-none");
					if( !hidden ){
						document.getElementById("resetRepeat-invalid").classList.toggle("d-none");
						document.getElementById("resetRepeatPassword").classList.toggle("is-invalid");
					}
					return true;
				}
			}
			function validaRepeatEpassword(){
				validaRepeat();
				
				pwd = document.getElementById("resetPassword").value;
				is5charsOrMore = ( pwd.length >= 5 );

				containsAnyDigit = /\d/.test(pwd);
				 if(!is5charsOrMore || !containsAnyDigit){
					 hidden = document.getElementById("resetPassword-invalid").classList.contains("d-none");
					 if(hidden){
						 document.getElementById("resetPassword-invalid").classList.toggle("d-none");
						 document.getElementById("resetPassword").classList.toggle("is-invalid");
					 }
			         return false;
				 }else{
					 hidden = document.getElementById("resetPassword-invalid").classList.contains("d-none");
					 if( !hidden ){
						 document.getElementById("resetPassword-invalid").classList.toggle("d-none");
					     document.getElementById("resetPassword").classList.toggle("is-invalid");
					 }
					 return true;
				 }
			}
			function reimpostaPassword(e){
				e.preventDefault();
				tokenId = document.getElementById("tokenId").value;
				console.log("voglio inviare a : " +  URLroot() + 'service.php/forgotPassword/' + tokenId + '/')
				if(validaRepeat() && validaRepeatEpassword()){
    				httpRequest = new XMLHttpRequest();
    			    httpRequest.onreadystatechange = function(){
        	            if(httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {  
                            document.getElementById("resetResult").innerHTML = "<img class=\"img-fluid rounded-circle\" src=\"" + URLbase() + "upload/jodorowski300_deepGreen.png\" alt=\"reset success\">";
							cambiaStato("login");
                        }else if(httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status !== 200){
					        document.getElementById("resetResult").innerHTML = "<img class=\"img-fluid rounded-circle\" src=\"" + URLbase() + "upload/jodorowski300_deepRed.png\" alt=\"reset failure\">";
				        }
						console.log(httpRequest.response);
                    }
                    httpRequest.open('POST', URLroot() + 'service.php/forgotPassword/' + tokenId + '/', true);
			        formData = new FormData();
                    formData.append("password", document.getElementById("resetPassword").value);
			        httpRequest.send(formData);	
				}

		    }
			
			document.getElementById("resetPassword").oninput = validaRepeatEpassword;
			document.getElementById("resetRepeatPassword").oninput = validaRepeat;
			document.getElementById("resetSubmit").onclick = reimpostaPassword;
		</script>
	</section>