<section id="settingsview" class="d-none bg-white container-fluid align-items-center" style="height: 90vh;" role="tabpanel" aria-label="settings view">
    <div class="row h-100">
	    <div class="col-md-6 mx-auto my-auto">
		    <button id="logoutButton" class="form-control mt-2">LOGOUT</button>
			<button id="settingsResetPassword" class="form-control mt-2">RESET PASSWORD</button>
		</div>
	</div>
</section>


<script>
    settingsViewObject = {
		isOpen : false,
		
		open :  function(){ settingsView = $("#settingsview")[0];
							mainView = $("main")[0];  
							settingsView.classList.remove("d-none");
							onceAnimationEnd(settingsView, "swipeIn")
							.then(()=>{
								views.active = settingsViewObject;
								settingsViewObject.isOpen = true;
								settingsView.ariaHidden = false;
							});
						  },
					  
		close : async function(){
			if(settingsViewObject.isOpen){
				return new Promise(resolve => {
				    settingsView = $("#settingsview")[0];
					mainView = $("main")[0];  
					mainView.style.overflow = "auto";
					mainView.style.height = "";
					onceAnimationEnd(settingsView, "swipeOut")
					.then(()=>{
						 views.active = null;
						 settingsView.classList.add("d-none");
						 settingsView.classList.remove("swipeOut");
						 settingsViewObject.isOpen = false;
						 settingsView.ariaHidden = true;
						 resolve();	
					 });	
				});							
			}else{
				return new Promise(resolve => resolve());
			}	
	    },
					  
		init : function(){ 
		                   settingsView = $("#settingsview")[0];
			               //settingsView.style.transition = "left 0.3s";
		},    //resetta la vista al suo stato blank
					 
		draw : function(source){ settingsViewObject.init();}
	};
	
    function logout(){
		sessionStorage.clear();
		location.reload();
	}
	
	function resetPassword(event){
		event.preventDefault();
		fd = new FormData();
        fd.append("email", getUser().email);
		ultraFetch(URLroot() + 'service.php/forgotPassword/',
			'POST',
			null,
			fd
		).then(response => 
			response.text()
			.then(text => {
				a = text.split("<a href=\"")[1];
				b = a.split("/\">ecco il tuo link </a>")[0];
				pieces = b.split("/");
				token = pieces[pieces.length - 1 ];
				cambiaStato("resetpassword/" + token + "/");
			})
		);
	}
	
	document.getElementById("settingsResetPassword").onclick = resetPassword;
	document.getElementById("logoutButton").onclick = logout;
</script>
