	function URLroot(){
		root = document.URL.toLowerCase().split('ui_2.php')[0];
		return root;
	}
	
	function URLbase(){
		return URLroot().toLowerCase().split('src')[0];
	}
	
	function getUser(){
		usr = sessionStorage.getItem("user");
		return (usr) ? JSON.parse(usr) : null;
	}
	
	async function ultraFetch(url, method, authorization = null, payload = null){
		req = {
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
		}

        if(["GET","POST","PUT","DELETE"].includes(method)){
			req.method = method;
		}else{
			throw "unrecognized HTTP method in ";
		}
		if(payload != null)
			req.body = payload;
		if(authorization != null)
			req.headers.Authorization = authorization;
		const response = await fetch( url , req );
        return response;
	}
	
	async function imgFileFrom(src){
		//fetching
		prom = ultraFetch(src,"GET", null, null)
					.then(r => { const reader = r.body.getReader()			
						 return new ReadableStream({
						 start(controller) {
							 return pump();
							 function pump() {
								 return reader.read().then(({ done, value }) => {
								 // When no more data needs to be consumed, close the stream
									 if (done) {
										 controller.close();
										 return;
									 }
									 // Enqueue the next data chunk into our target stream
									 controller.enqueue(value);
									 return pump();
								 });
							 }
						 }
						 })
					})
					.then(stream => new Response(stream))
					.then(response => response.blob())
			/*
			.then(blob => URL.createObjectURL(blob))
			.then(url => console.log("image.src = " + url))
			*/
					.catch(err => console.error(err));
		blb = await prom;
		return blb;
	}
	
	//https://stackoverflow.com/questions/36280818/how-to-convert-file-to-base64-in-javascript
	const toBase64 = file => new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => resolve(reader.result);
		reader.onerror = error => reject(error);
	});
	
	https://stackoverflow.com/questions/35940290/how-to-convert-base64-string-to-javascript-file-object-like-as-from-file-input-f
	function dataURLtoFile(dataurl, filename) {
       
		var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
            
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        
        return new File([u8arr], filename, {type:mime});
    }
	
	//https://www.codegrepper.com/code-examples/javascript/get+image+from+javascript	
	function createFileFrom(dir){
		/* defining runtime variables */
		var extension                        =   dir.split('.').pop();
		var keys                             =   {"png":"IMG","jpg":"IMG","jpeg":"IMG",
												  "js":"SCRIPT","json":"SCRIPT",
												  "mp3":"AUDIO","wav":"AUDIO"};
		var obj                              =   document.createElement(keys[extension]) || {};
		obj.src                              =   dir;
					
		/* onload function called when the resource is loaded */
		obj.onload  = (e) => {
			return e;
			//elements.push(e.path[0]);
		}

		/* make sure that the data is compitable */
		if(keys[extension]==null){console.error("not supported media type "+extension);return;}
	}
	
	let onceAnimationEnd = (el, animClass) => {
		return new Promise(resolve => {
			var onAnimationEndCb = () => {
				el.removeEventListener('animationend', onAnimationEndCb);
				resolve();
			}
			el.addEventListener('animationend', onAnimationEndCb);
			el.classList.add(animClass);
		});
	}
	
	function ELFromTXT(text){
		el = document.createElement("div");
		el.innerHTML = text;
		return el.children[0];
	}