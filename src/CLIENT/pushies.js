function pushiesLength(){
	return document.getElementById("pushies").children.length;
}

function pushie(header,body){
		tst = document.createElement("li");
		if(document.getElementById("pushies").lastChild.id !== undefined){
			lastId = document.getElementById("pushies").lastChild.id;
		}
		else{
			lastId = "pushie-1";
		}
		nextId = "pushie" + (Number.parseInt(lastId.substr(6)) + 1);
		tst.id = nextId;
		tst.classList.add("toast");
		tst.classList.add("show");
		tst.setAttribute("role", "alert");
		tst.setAttribute("aria-live", "assertive");
		tst.setAttribute("aria-atomic", "true");
		tst.innerHTML = `
				  <div class="toast-header">
					<strong class="mr-auto">${header}</strong>
					<small class="text-muted">just now</small>
					<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>
				  <div class="toast-body">
					${body}
				  </div>
				`
		document.getElementById("pushies").appendChild(tst);
		pushies = document.getElementById("pushies");
		setTimeout((id)=>{pushies.removeChild(document.getElementById(id))},2000, nextId);
}