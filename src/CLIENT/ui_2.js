	window.onpageshow = () => {
		console.log("pageShow");
		views = {};
		views.active = null;
		views.list = {};
		views.list.products = productsViewObject;
		views.list.cart = cartViewObject;	
	    views.list.checkout = checkoutViewObject;
		views.list.product = productViewObject;
		views.list.home = homeViewObject;
		views.list.login = loginViewObject;
		views.list.register = registerViewObject;
		views.list.forgotpassword = forgotPasswordViewObject;
		views.list.resetpassword = resetPasswordViewObject;
		views.list.orders = ordersViewObject;
		views.list.settings = settingsViewObject;
		views.list.notifications = notificationsViewObject;
		
		
		document.getElementById("cartButton").onclick = () => { cartViewObject.toggle() };
		
		document.getElementById("homeLink").href = URLroot() + "ui_2.php/home/";
		document.getElementById("notificationsPill").href = URLroot() + "ui_2.php/notifications/";
		
		
		trail = window.location.href.split("ui_2.php/")[1];
		query = (trail)? trail : "";
		
		if(sessionStorage.getItem("Authentication") != null) {
		    if(query != null){
			    showView(query);
		    }
		}
		else{
			pieces = query.split("/");
			switch(pieces[0]){
				case "register":
					cambiaStato("register");
				break;
				case "forgotpassword":
				case "resetpassword":
					cambiaStato(query);
				break;
				break;
				default:
					cambiaStato("login");
			}
		}
	}
	
    window.onpopstate = function (event) {
        query = (event)? (event.state) ? event.state.query : event.target.url.split("ui_2.php/")[1] : "";
        showView(query);
    }
		
    function cambiaStato(queryString){
		console.log("cambiaStato");
		if(queryString[queryString.length - 1] != "/"){
			queryString = queryString + "/";
		}
        
        history.pushState( { query : queryString } , null, URLroot() + "ui_2.php" + "/" + queryString );
			
        showView(queryString);		
    }
		
    async function showView(queryString){	
		console.log("showView");
		cartB = document.getElementById("cartButton");
			cartB.disabled = true;
		
			if(! cartB.classList.contains("d-none"))
				cartB.classList.add("d-none");
		
		user = getUser();			
		if(user != null && user.type == "USER"){
			cartB.classList.remove("d-none");
			cartB.disabled = false;
		}
		
		for(view in views.list){
		  await views.list[view].close();
		  
		}

		checkNotifications();

		view = queryToView(queryString);

        views.list[view].draw(queryString);		
		views.list[view].open();
		//sempre lui perchè safe e immediatamente antecedente il contenuto mostrato
		views.list.cart.cartButton.focus();
		
	}

	var prevScrollpos = window.pageYOffset;
	window.onscroll = function() {
	  var currentScrollPos = window.pageYOffset;
	  if (prevScrollpos > currentScrollPos) {
		document.getElementById("header").style.top = "0%";
		document.getElementById("header").style.transition = "top 0.2s";
		
	  } else {
		document.getElementById("header").style.top = "-100%";
		document.getElementById("header").style.transition = "top 0.5s";
	  }
	  prevScrollpos = currentScrollPos;
	} 
		
	function queryToView(queryString){
		if(queryString != null && queryString.length > 0){
			//il primo slash è già assente ma per esser sicuri
			if(queryString[0] == "/"){
				queryString.substr(1,queryString.length);
			}
			pieces = queryString.split("/");
			nPieces = pieces.length;
			if(nPieces >= 1){
				switch(pieces[0]){
					case "users":
				        if(nPieces >= 2 && pieces[1].length > 0){
						    numeric = Number(pieces[1]);	
							if(!isNaN(numeric) && numeric > 0){
								if(nPieces >= 3 && pieces[2].length > 0){
									//user sub arg
									switch(pieces[2]){
										case "products":
										    return "products";
										break;
										default:
										    return "error";
										break;
									}
								}else{
									return "user";
								}
							}else{
								return "users";
							}
						}else{
							return "users";
						}
					break;
					case "products":
					    if(nPieces >= 2 && pieces[1].length > 0){
						    numeric = Number(pieces[1]);	
							if(!isNaN(numeric) && numeric > 0){
								return "product";
							}else if( "new" == pieces[1] ){
								return "product";
							}
						}else{
							return "products";
						}
					break;
					
					case "orders":
					case "notifications":					
					case "login":
					case "register":
					case "home":
					case "checkout":
					case "settings":
					case "forgotpassword":
					case "resetpassword":
					    return pieces[0];
					break;

					default:
					    return "home";
					break;
				}
			}else{
			    return "home";
			}
		}else{
			return "home";
		}
		
	}
		
	function checkNotifications(){
		user = getUser();
		if(user != null && sessionStorage.Authentication != null)
			ultraFetch(URLroot() + "service.php/users/" + user.id + "/", "GET", "Basic : " + sessionStorage.Authentication)
			.then(
				response => {
					response.json()
					.then(
						user => {
							pill = $("#notificationsPill")[0];
							if(user.notifications && user.notifications.length > 0){
								pill.disabled = false;
								pill.ariaHidden = false;
								pill.innerText = user.notifications.length;
								pill.classList.remove("d-none");
							}
							else{
								pill.disabled = true;
								pill.ariaHidden = true;
								if(! pill.classList.contains("d-none"))
									pill.classList.add("d-none");
							}
						}
					);
				}
			);
	}
	


   function paymentConfirmed(){
	   if(views.active == checkoutViewObject){
			sessionStorage.removeItem("cart");
			showView("checkout/");
       }
	   if(views.active == ordersViewObject){
	        ordersViewObject.draw();
	   }
   }
