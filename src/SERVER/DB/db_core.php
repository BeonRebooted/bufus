<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<?php

class DatabaseHelper {

    public $db;
    private $servername;
    private $username;
    private $password;
    private $dbname;

    public function __construct($servername, $username, $password, $dbname) {
        $this->servername = $servername;
        $this->username = $username;
        $this->password = $password;
        $this->dbname = $dbname;
        $this->boot();
    }

    public function boot() {
        $this->db = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        if ($this->db->connect_error) {
            die("Connesione fallita al db");
        }
    }

    //rotta
    public function reboot() {
        $this->execFromDisk("../reset.sql");
        $this->db->select_db("shops4scuola");
        $this->execFromDisk("../data.sql");
        $this->db->close();
        $this->boot();

    }

    private function execFromDisk($src){
        $query = file_get_contents($src);
        $statement = $this->db->multi_query($query);
        do {
            if ($result = $this->db->store_result()) {
                //var_dump($result->fetch_all(MYSQLI_ASSOC));
                $result->free();
            }
        } while ($this->db->next_result());
        if ( false===$statement ) {
            // and since all the following operations need a valid/ready statement object
            // it doesn't make sense to go on
            // you might want to use a more sophisticated mechanism than die()
            // but's it's only an example
            die('prepare() failed: ' . $this->db->errno   . htmlspecialchars($this->db->error));
        }
    }
}

$dbh = new DatabaseHelper("localhost", "root", "", "shops4scuola");
?>