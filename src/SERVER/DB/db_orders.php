<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require_once 'db_core.php';


?>
<?php /*orders*/?>
<?php

//ROTTISSIMA
function selectOrder($orderId) {
    //fetch order
    $order = selectFromOrdersById($orderId);
    if($order){
        //fetch his details
        $details = selectFromOrderDetailsByOrder($orderId);
        if($details){
            foreach($details as &$detail){
                //fetch detail options
                $options = selectFromDetailOptionsByDetail($orderId, $detail["detailIndex"]);
                $detail["options"] = $options;
            }
            $order["details"] = $details;
        }
        return $order;
    }else{
        
    }
}

//OK
function selectUserOrders($userId) {
    global $dbh;
    
    $query = "SELECT orders.id,	orders.clientId, orders.sellerId, customers.name as clientName, sellers.name as sellerName, orders.date, orders.price, orders.status, orderPaymentTokens.id	as payTokenId
            	  FROM (
					(	orders 
						LEFT JOIN orderPaymentTokens 
						ON orders.id = orderPaymentTokens.orderId AND orders.clientId = ? )
					JOIN users AS customers ON customers.id = orders.clientId) 
				  JOIN users AS sellers ON sellers.id = orders.sellerId
				  WHERE clientId = ? OR sellerId = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("iii", $userId, $userId, $userId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $ordersGeneral = $res->fetch_all(MYSQLI_ASSOC);
        for ($i = 0; $i < count($ordersGeneral); $i++) {
            $ordersGeneral[$i] = selectOrder($ordersGeneral[$i]["id"]);
        }
        return $ordersGeneral;
    } else
        throw new Exception("DB error : " . $statement->error);
}

function selectOrderDetail($orderId, $detailIndex){
    global $dbh;
    $query = "select orderId ,detailIndex , productId , quantity  from orderdetails where orderId = ? and detailIndex = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("ii", $orderId, $detailIndex);
    $outcome = $statement->execute();
    if ($outcome) {
        $detail = $statement->get_result()->fetch_assoc();
        $query = "select * from detailOptions where orderId = ? and detailIndex = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("ii", $orderId, $detailIndex);
        $outcome = $statement->execute();
        if ($outcome) {
            $options = $statement->get_result()->fetch_all(MYSQLI_ASSOC);
            foreach($options as $option){
                switch($option["type"]){
                    case "VALUE":
                        $option["value"] = selectValueDetailOption($option["id"]);
                    break;
                    case "INTRANGE":
                         $option["value"] = selectIntDetailOption($option["id"]);
                    break;                
                    case "FLOATRANGE":
                         $option["value"] = selectFloatDetailOption($option["id"]);
                    break;
                    case "TEXTVALUE":
                         $option["value"] = selectTextDetailOption($option["id"]);
                    break;
                    case "FILEVALUE":
                         $option["value"] = selectFileDetailOption($option["id"]);
                    break;
                }
                unset($option["id"]);
            }
            $detail["options"] = $options;
            return $detail;
        }
        
    }else{
        
    }
    
}

//OK
//returns wheter or not the operation has been completed (DATA attached)
function updateOrderStatus($orderId) {
    global $dbh;

    $order = selectOrder($orderId);
    if (!$order)
        return false;
    if (($order["status"] == "ATTESAPAGAMENTO") || ($order["status"] == "ARRIVATO_STAZIONE"))
        return false;

    switch ($order["status"]) {
        case "ACCETTATO":
            $nuovostatus = "SPEDITO";
            break;
        case "SPEDITO":
            $nuovostatus = "ARRIVATO_STAZIONE";
            break;
        default:
            $nuovostatus = "fallisci";
            break;
    }
    //
    $query = "UPDATE orders SET status = ? WHERE id = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("si", $nuovostatus, $orderId);
    $outcome = $statement->execute();
    if ($outcome) {
        if ($statement->affected_rows) {
            return true;
        } else {
            return false;
        }
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function isOrderSeller($orderId, $userId) {
    global $dbh;
    $query = "SELECT * FROM orders 
				  WHERE sellerId = ? AND id = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("ii", $userId, $orderId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $res = $res->fetch_all(MYSQLI_ASSOC);
        return (count($res) > 0);
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function isOrderClient($orderId, $userId) {
    global $dbh;
    $query = "SELECT * FROM orders 
				  WHERE clientId = ? AND id = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("ii", $userId, $orderId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $res = $res->fetch_all(MYSQLI_ASSOC);
        return (count($res) > 0);
    } else
        throw new Exception("DB error : " . $statement->error);
}
?>
<?php /*order blocks */?>
<?php 
    function selectFromOrdersById($orderId){
        global $dbh;

        $query =
            "SELECT orders.id, orders.clientId, orders.sellerId, customers.name as clientName, sellers.name as sellerName,
            orders.date, orders.price, orders.status, orderPaymentTokens.id as payTokenId
            FROM 
            (
               (
                    orders 
                    LEFT JOIN orderPaymentTokens 
                    ON orders.id = orderPaymentTokens.orderId)
                JOIN users AS customers ON customers.id = orders.clientId) 
            JOIN users AS sellers ON sellers.id = orders.sellerId
            WHERE orders.id = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("i", $orderId);
        if ($statement->execute()) {
            $res = $statement->get_result();
            return $res->fetch_assoc();
        }else{
            return null;
        }
    }
    function selectFromOrderDetailsByOrder($orderId){
        global $dbh;
        $query = "SELECT * FROM orderDetails WHERE orderId = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("i", $orderId);
        if ($statement->execute()) {
            return $statement->get_result()->fetch_all(MYSQLI_ASSOC);
        }else{
            return null;
        }
    }
    function selectFromDetailOptionsByDetail($orderId, $detailIndex){
        global $dbh;
        $query = "SELECT * FROM detailOptions WHERE orderId = ? and detailIndex = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("ii", $orderId, $detailIndex);
        if ($statement->execute()) {
            return $statement->get_result()->fetch_all(MYSQLI_ASSOC);
        }else{
            return null;
        }   
    }
    function selectValueDetailOption($detailOptionId){
        global $dbh;
        $query = "select * from valueDetailOptions where detailOptionId = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("i", $detailOptionId);
        $outcome = $statement->execute();
        if ($outcome) {
            return $statement->get_result()->fetch_assoc();
        }else{
             throw("error while selecting valueDetailOption");
        }
    }
    function selectIntDetailOption($detailOptionId){
        global $dbh;
        $query = "select * from intDetailOptions where detailOptionId = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("i", $detailOptionId);
        $outcome = $statement->execute();
        if ($outcome) {
            return $statement->get_result()->fetch_assoc();
        }else{
             throw("error while selecting intDetailOption");  
        }
    }
    function selectFloatDetailOption($detailOptionId){
        global $dbh;
        $query = "select * from floatDetailOptions where detailOptionId = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("i", $detailOptionId);
        $outcome = $statement->execute();
        if ($outcome) {
            return $statement->get_result()->fetch_assoc();
        }else{
             throw("error while selecting floatDetailOption")  ; 
        }
    }
    function selectTextDetailOption($detailOptionId){
        global $dbh;
        $query = "select * from textDetailOptions where detailOptionId = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("i", $detailOptionId);
        $outcome = $statement->execute();
        if ($outcome) {
            return $statement->get_result()->fetch_assoc();
        }else{
             throw("error while selecting textDetailOption");
        }
    }
    function selectFileDetailOption($detailOptionId){
        global $dbh;
        $query = "select * from fileDetailOptions where detailOptionId = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("i", $detailOptionId);
        $outcome = $statement->execute();
        if ($outcome) {
            return $statement->get_result()->fetch_assoc();
        }else{
             throw("error while selecting fileDetailOption");  
        }
    }
?>