<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once 'db_core.php';
?>
<?php /*products*/?>
<?php

//OK
function selectProducts() {
    global $dbh;
    $query = "SELECT p.id, p.name, c.name AS categoryName, p.categoryId, p.description, p.sellerId, p.price, p.deleted, p.imgsrc, u.name as sellerName
                  FROM categories AS c
				  JOIN (products AS p
                  JOIN users AS u
                  ON u.id = p.sellerId)
				  ON p.categoryId = c.id";
    $statement = $dbh->db->prepare($query);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        return $res->fetch_all(MYSQLI_ASSOC);
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function selectProductsBySellerId($sellerId) {
    global $dbh;
    $query = "SELECT p.id, p.name, c.name AS categoryName, p.categoryId, p.description, p.sellerId, p.price, p.deleted, p.imgsrc, u.name as sellerName
                  FROM categories AS c
				  JOIN (products AS p
                  JOIN users AS u
                  ON u.id = p.sellerId)
				  ON p.categoryId = c.id
                  WHERE p.sellerId = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("s", $sellerId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result()->fetch_all(MYSQLI_ASSOC);
        return $res;
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function selectProductsByName($name) {
    global $dbh;
    //NOT PREPARED
    $query = "SELECT p.id, p.name, c.name AS categoryName, p.categoryId, p.description, p.sellerId, p.price, p.deleted, p.imgsrc, u.name as sellerName
                  FROM categories AS c
				  JOIN (products AS p
                  JOIN users AS u
                  ON u.id = p.sellerId)
				  ON p.categoryId = c.id
                  WHERE p.name LIKE '%" . $name . "%'";
    $statement = $dbh->db->prepare($query);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        return $res->fetch_all(MYSQLI_ASSOC);
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function selectProductsByCategoryName($catName) {
    global $dbh;
    $query = "SELECT p.id, p.name, c.name AS categoryName, p.categoryId, p.description, p.sellerId, p.price, p.deleted, p.imgsrc, u.name as sellerName
                  FROM categories AS c
				  JOIN (products AS p
                  JOIN users AS u
                  ON u.id = p.sellerId)
				  ON p.categoryId = c.id
                  WHERE c.name = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("s", $catName);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        return $res->fetch_all(MYSQLI_ASSOC);
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function selectProduct($productId) {
    global $dbh;
    $query = "SELECT p.id, p.name, c.name AS categoryName, p.categoryId, p.description, p.sellerId, p.price, p.deleted, p.imgsrc, u.name as sellerName
                  FROM categories AS c
				  JOIN (products AS p
                  JOIN users AS u
                  ON u.id = p.sellerId)
				  ON p.categoryId = c.id
                  WHERE p.id = ?
				  ";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("i", $productId);
    $outcome = $statement->execute();
    if ($outcome) {
        //ora le options
        $res = $statement->get_result()->fetch_assoc();
        try {
            $res["options"] = selectProductOptions($res["id"]);
            return $res;
        } catch (Exception $e) {
            throw new Exception($e);
        }
    } 
    else throw new Exception("DB error : " . $statement->error);
}

//OK
function insertProduct($sellerId, $name, $categoryId, $price, $description, $options) {
    global $dbh;
    if (!is_numeric($price) || $price <= 0)
        throw new Exception("BAD PRICE");
    if (!is_numeric($categoryId))
        throw new Exception("categoryId must be an int");

    $lev3 = "INSERT INTO products ( name, categoryId, sellerId, price , description ) VALUES ( ? , ? , ? , ? , ?)";
    $statement = $dbh->db->prepare($lev3);
    $statement->bind_param("siids", $name, $categoryId, $sellerId, $price, $description);
    $outcome = $statement->execute();
    if ($outcome) {
        $id = $statement->insert_id;
        $lev3 = "UPDATE products SET imgsrc = '" . $id . ".jpg' WHERE id = " . $id;
        $statement = $dbh->db->prepare($lev3);
        $statement->execute();
        if ($outcome) {
            //crea l'immagine del nuovo prodotto
            fopen($id . ".jpg", "w");
            //aggiunta options prodotto
            try {
                updateProductOptions($id, $options);
            } catch (Exception $e) {
                throw Exception($e);
            }
            return selectProduct($id);
        } else
            throw new Exception("DB error : " . $statement->error);
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function updateProduct($id, $name, $categoryId, $price, $description, $deleted, $options) {
    global $dbh;

    if (!is_numeric($price) || $price <= 0) {
        return ["result" => "error", "message" => "price must be a positive number"];
    }

    if (!is_numeric($categoryId) || $categoryId <= 0) {
        return ["result" => "error", "message" => "categoryId must be a positive integer"];
    }

    $lev3 = "UPDATE products SET name = ?, categoryId = ?, price = ?, description = ? , deleted = ? WHERE id = ?";
    $statement = $dbh->db->prepare($lev3);
    $statement->bind_param("sidsii", $name, $categoryId, $price, $description, $deleted, $id);
    $outcome = $statement->execute();
    if ($outcome) {
        try {
            $res = updateProductOptions($id, $options);
            return selectProduct($id);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function selectProductOptions($productId) {
    global $dbh;
    $statement = $dbh->db->prepare("SELECT * FROM options WHERE productId = ?");
    $statement->bind_param("i", $productId);
    $outcome = $statement->execute();
    if ($outcome) {
        $options = $statement->get_result()->fetch_all(MYSQLI_ASSOC);
        foreach ($options as &$option) {
            if ($option["type"] == "VALUE") {
                $option["values"] = [];
                $statement = $dbh->db->prepare("SELECT * FROM valueOptions WHERE productId = ? and name = ?");
                $statement->bind_param("is", $productId, $option["name"]);
                $outcome = $statement->execute();
                if ($outcome) {
                    $values = $statement->get_result()->fetch_all(MYSQLI_ASSOC);
                    $option["values"] = $values;
                } else
                    throw new Exception("DB error : " . $statement->error);
            } else if ($option["type"] == "INTRANGE") {
                $statement = $dbh->db->prepare("SELECT * FROM intrangeoptions WHERE productId = ? and name = ?");
                $statement->bind_param("is", $productId, $option["name"]);
                $outcome = $statement->execute();
                if ($outcome) {
                    $value = $statement->get_result()->fetch_assoc();
                    $option["minimum"] = $value["minimum"];
                    $option["maximum"] = $value["maximum"];
                } else
                    throw new Exception("DB error : " . $statement->error);
            } else if ($option["type"] == "FLOATRANGE") {
                $statement = $dbh->db->prepare("SELECT * FROM floatRangeOptions WHERE productId = ? and name = ?");
                $statement->bind_param("is", $productId, $option["name"]);
                $outcome = $statement->execute();
                if ($outcome) {
                    $value = $statement->get_result()->fetch_assoc();
                    $option["minimum"] = $value["minimum"];
                    $option["maximum"] = $value["maximum"];
                } else
                    throw new Exception("DB error : " . $statement->error);
            }
        }
        return $options;
    } else
        throw new Exception("DB error : " . $statement->error);
}

function selectProductOption($productId, $name) {
    global $dbh;
    $statement = $dbh->db->prepare("SELECT * FROM options WHERE productId = ? AND name = ?");
    $statement->bind_param("is", $productId, $name);
    $outcome = $statement->execute();
    if ($outcome) {
        return $statement->get_result()->fetch_assoc();
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function updateProductOptions($productId, $options) {
    global $dbh;
    //check validità
    foreach ($options as $option) {
        if (isSet($option["type"])) {
            switch ($option["type"]) {
                case "FILEVALUE":
                case "TEXTVALUE":
                    break;
                case "FLOATRANGE":
                    $min = null;
                    $max = null;
                    if (isSet($option["minimum"])) {
                        if (is_numeric($option["minimum"])) {
                            $min = $option["minimum"];
                        } else
                            throw new Exception("bad FLOATRANGE:minimum");
                    }
                    if (isSet($option["maximum"])) {
                        if (is_numeric($option["maximum"])) {
                            $max = $option["maximum"];
                        } else
                            throw new Exception("bad FLOATRANGE:maximum");
                    }
                    if (isSet($max) && isSet($min))
                        if ($max <= $min)
                            throw new Exception("bad FLOATRANGE:interval");
                    break;
                case "INTRANGE":
                    $min = null;
                    $max = null;
                    if (isSet($option["minimum"])) {
                        if (is_numeric($option["minimum"])) {
                            $min = 0 + $option["minimum"];
                            if (!is_int($min))
                                throw new Exception("bad INTRANGE:minimum");
                        } else
                            throw new Exception("bad INTRANGE:minimum");
                    }
                    if (isSet($option["maximum"])) {
                        if (is_numeric($option["maximum"])) {
                            $max = 0 + $option["maximum"];
                            if (!is_int($max))
                                throw new Exception("bad INTRANGE:maximum");
                        } else
                            throw new Exception("bad INTRANGE:maximum");
                    }
                    if (isSet($max) && isSet($min)) {
                        if ($max <= $min)
                            throw new Exception("bad INTRANGE:interval");
                    }
                    break;
                case "VALUE":
                    break;
                default:
                    throw new Exception("bad options:unknown_types");
            }
        } else
            throw new Exception("bad options:no_types");
    }
    $oldOptions = selectProductOptions($productId);
    //cancelliamo tutte le options vecchie
    foreach ($oldOptions as $option) {
        $outcome = true;
        switch ($option["type"]) {
            case "FILEVALUE":
            case "TEXTVALUE":
                break;
            case "INTRANGE":
                $query = "DELETE FROM intrangeoptions WHERE ( productId = ? AND name = ? )";
                $statement = $dbh->db->prepare($query);
                $statement->bind_param("is", $productId, $option["name"]);
                if (!$statement->execute())
                    throw new Exception("DB error : " . $statement->error);
            case "FLOATRANGE":
                $query = "DELETE FROM floatRangeOptions WHERE ( productId = ? AND name = ? )";
                $statement = $dbh->db->prepare($query);
                $statement->bind_param("is", $productId, $option["name"]);
                if (!$statement->execute())
                    throw new Exception("DB error : " . $statement->error);
            case "VALUE":
                    $query = "DELETE FROM valueOptions WHERE ( productId = ? AND name = ? )";
                    $statement = $dbh->db->prepare($query);
                    $statement->bind_param("is", $productId, $option["name"]);
                    if (!$statement->execute())
                        throw new Exception("DB error : " . $statement->error);
                break;
        }
        if ($outcome) {
            $query = "DELETE FROM options WHERE ( productId = ? AND name = ? )";
            $statement = $dbh->db->prepare($query);
            $statement->bind_param("is", $productId, $option["name"]);
            if (!$statement->execute())
                throw new Exception("DB error : " . $statement->error);
        } else
            throw new Exception("DB error : " . $statement->error);
    }
    //inseriamo delle nuove options
    foreach ($options as $option) {
        $query = "insert into options ( productId , name , price , description , required , type ) values ( ? , ? , ? , ? , ? , ? )";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("isdsis", $productId, $option["name"], $option["price"], $option["description"], $option["required"], $option["type"]);
        $outcome = $statement->execute();
        if ($outcome) {
            switch ($option["type"]) {
                case "FILEVALUE":
                case "TEXTVALUE":
                    break;
                case "INTRANGE":
                    $query = "INSERT INTO intrangeoptions (productId , name , minimum , maximum ) values ( ? , ? , ? , ? )";
                    $statement = $dbh->db->prepare($query);
                    $statement->bind_param("isii", $productId, $option["name"], $option["minimum"], $option["maximum"]);
                    if (!$statement->execute())
                        throw new Exception("DB error : " . $statement->error);
                case "FLOATRANGE":
                    $query = "INSERT INTO floatRangeOptions (productId , name , minimum , maximum ) values ( ? , ? , ? , ? )";
                    $statement = $dbh->db->prepare($query);
                    $statement->bind_param("isdd", $productId, $option["name"], $option["minimum"], $option["maximum"]);
                    if (!$statement->execute())
                        throw new Exception("DB error : " . $statement->error);
                case "VALUE":
                    foreach ($option["values"] as $vali) {
                        $query = "INSERT INTO valueOptions ( productId , name , value , price ) values ( ? , ? , ? , ? )";
                        $statement = $dbh->db->prepare($query);
                        $statement->bind_param("issd", $productId, $option["name"], $vali["value"], $vali["price"]);
                        if (!$statement->execute())
                            throw new Exception("DB error : " . $statement->error);
                    }
                    break;
            }
        } else
            throw new Exception("DB error : " . $statement->error);
    }
    return selectProductOptions($productId);
}

//OK
function selectProductReviews($productId) {
    global $dbh;

    $query = "SELECT * FROM reviews WHERE productId = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("i", $productId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        return $res->fetch_all(MYSQLI_ASSOC);
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function insertProductReview($productId, $userId, $voto, $testo) {
    global $dbh;

    if (!is_numeric($voto) || $voto < 1 || $voto > 5)
        throw new Exception("BAD VOTO");

    $lev3 = "INSERT into reviews (productId, userId, rating, text) VALUES (?, ?, ?, ?)";
    $statement = $dbh->db->prepare($lev3);
    $statement->bind_param("iiis", $productId, $_USER_REQUESTER, $voto, $testo);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        if ($statement->affected_rows) {
            return selectProductReview($statement->insert_id);
        } else
            throw new Exception("DB error : " . $statement->error);
    } else
        throw new Exception("DB error : " . $statement->error);
}
?>