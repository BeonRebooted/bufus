<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "db_core.php";
?>
<?php /* users */ ?>
<?php

//OK
function selectUsers() {
    global $dbh;
    $lev3 = "SELECT id , email , name , type FROM users";
    $statement = $dbh->db->prepare($lev3);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $res = $res->fetch_all(MYSQLI_ASSOC);
        return $res;
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}

//OK
function selectUser($id) {
    global $dbh;

    $lev3 = "SELECT id , email , name , type FROM users WHERE id = ?";
    $statement = $dbh->db->prepare($lev3);
    $statement->bind_param("i", $id);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $res = $res->fetch_assoc();
        return $res;
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}

//OK
function selectUserUnreadNorifications($usrId) {
    global $dbh;

    $lev = "SELECT * FROM notifications 
				 WHERE `read` = false
				 AND userId = ? ";
    $statement = $dbh->db->prepare($lev);
    $statement->bind_param("i", $usrId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $res = $res->fetch_all(MYSQLI_ASSOC);
        return $res;
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}

//OK
function insertUser($name, $email, $password, $type) {
    global $dbh;

    if (strlen($name) < 1) {
        throw new Exception("user name is empty");
    }
    if (!preg_match("/(\w|\.)+\@\w+\.\w+/", $email)) {
        throw new Exception("user email is malformed");
    }

    if ((strlen($password) < 5) || (!preg_match("/\d/", $password))) {
        throw new Exception("user password must be 5 or more characters including a digit");
    }
    if (($type != "USER") && ($type != "SELLER")) {
        throw new Exception("user type must be one of the following : USER , SELLER");
    }

    $lev3 = "INSERT INTO users ( name , email , password , type ) VALUES ( ? , ? , ? , ? )";
    $statement = $dbh->db->prepare($lev3);
    $statement->bind_param("ssss", $name, $email, $password, $type);
    $outcome = $statement->execute();
    if ($outcome) {
        if ($statement->affected_rows)
            return $statement->insert_id;
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}

//OK
function selectUsersByName($name) {
    global $dbh;
    //NOT PREPARED
    $lev3 = "SELECT id , email , name , type FROM users WHERE name LIKE '%" . $name . "%'";
    $statement = $dbh->db->prepare($lev3);
    //$statement->bind_param("s", $name);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $res = $res->fetch_all(MYSQLI_ASSOC);
        return $res;
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}

//OK
function selectUserByEmailAndPassword($email, $password) {
    global $dbh;
    $lev3 = "SELECT * FROM users WHERE email = ? AND password = ?";
    $statement = $dbh->db->prepare($lev3);
    $statement->bind_param("ss", $email, $password);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $res = $res->fetch_assoc();
        return $res;
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}

//(fix returns and logic)
function canXReviewY($userId, $productId) {
    global $dbh;
    if ($userId == null)
        return false;
    $hasOrdered = "SELECT * FROM users 
		               JOIN (orders 
				           JOIN orderDetails
					       ON orders.id = orderDetails.orderId)
		               ON orders.clientId = users.id 
				       WHERE orderDetails.productId = ?
				       AND users.id = ?";
    $statement = $dbh->db->prepare($hasOrdered);
    $statement->bind_param("ii", $productId, $userId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        $res = $res->fetch_all(MYSQLI_ASSOC);
        if (count($res) > 0) {
            $alreadyReviewed = "SELECT * FROM reviews 
       				                WHERE productId = ?
				                    AND userId = ?";
            $statement = $dbh->db->prepare($alreadyReviewed);
            $statement->bind_param("ii", $productId, $userId);
            $outcome = $statement->execute();
            if ($outcome) {
                $res = $statement->get_result();
                $res = $res->fetch_all(MYSQLI_ASSOC);
                if (count($res) > 0) {
                    return false;
                } else {
                    return true;
                }
            } else {
                throw new Exception("DB error : " . $statement->error);
            }
        } else {
            return false;
        }
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}

//(fix CALLS params order)
function isXSellerOfY($userId, $productId) {
    global $dbh;
    $query = "SELECT *
                  FROM products
                  WHERE id = ? AND sellerId = ?
				  ";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("ii", $productId, $userId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        return (count($res->fetch_assoc()) > 0);
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}

?>