<?php 
    require_once "DB/db_core.php";
    require 'DB/db_orders.php';
    require 'DB/db_users.php';
    require 'DB/db_products.php';
    
?>
<?php /*categories*/?>
<?php

//OK
function selectCategories() {
    global $dbh;
    $query = "SELECT * FROM categories ORDER BY id";
    $statement = $dbh->db->prepare($query);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result();
        return $res->fetch_all(MYSQLI_ASSOC);
    } else {
        throw new Exception("DB error : " . $statement->error);
    }
}
?>
<?php /*notifications*/?>
<?php

//OK
function selectUserNotification($userId, $id) {
    global $dbh;

    $query = "SELECT * FROM notifications WHERE id = ? AND userId = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("ii", $id, $userId);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->get_result()->fetch_assoc();
        return $res;
    } else
        throw new Exception("DB error : " . $statement->error);
}

//OK
function selectUserNotifications($userId) {
    global $dbh;

    if (isSet($userId)) {
        $query = "SELECT * FROM notifications WHERE userId = ?";
        $statement = $dbh->db->prepare($query);
        $statement->bind_param("i", $userId);
        $outcome = $statement->execute();
        if ($outcome) {
            $res = $statement->get_result();
            return $res->fetch_all(MYSQLI_ASSOC);
        } else
            throw new Exception("DB error : " . $statement->error);
    } else
        throw new Exception("userId is null");
}

//OK
function deleteNotification($id) {
    $query = "DELETE FROM notifications WHERE id = ?";
    $statement = $dbh->db->prepare($query);
    $statement->bind_param("i", $id);
    $outcome = $statement->execute();
    if ($outcome) {
        $res = $statement->affected_rows;
        if ($res == 1) {
            return true;
        } else if ($res == 0) {
            return false;
        } else
            throw new Exception("DB error : " . "INCONSISTENCY IN TABLE NOTIFICATIONS");
    } else
        throw new Exception("DB error : " . $statement->error);
}
?>