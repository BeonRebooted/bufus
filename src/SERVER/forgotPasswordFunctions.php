<?php

	function getFormRipristinoPassword($who){
		http_response_code (200);
		header("Location: http://localhost/progettoscuola/src/resetPassword.php/" . $who . "/");
        die(); 
	}
	
	function eliminaToken($id){
		global $dbh;
		$lev3 = "DELETE FROM resetPasswordTokens WHERE id = ?";
        $statement = $dbh->db->prepare($lev3);
    	$statement->bind_param("i", $id);
        $outcome = $statement->execute();
		if($outcome){
			if($statement->affected_rows){
				return true;
			}else{
				//non ho alterato la tabella
				return false;
			}
		}else{
			//non ho eseguito la query
			return false;
		}
	}
	
	function ripristinaPassword($who){
		global $dbh;
	    $lev3 = "SELECT * FROM resetPasswordTokens WHERE id = ?";
        $statement = $dbh->db->prepare($lev3);
    	$statement->bind_param("i", $who);
        $outcome = $statement->execute();
		if($outcome){
			$result = $statement->get_result();
			// "query ricerca token eseguita\n";
			if($result->num_rows){
				// "esiste token\n";
				$token = $result->fetch_assoc();
			    $lev3 = "UPDATE users SET password = ? WHERE id = ?";
			    $statement = $dbh->db->prepare($lev3);
    	        $statement->bind_param("si", $_POST["password"], $token["userId"]);
                $outcome = $statement->execute();
				if($outcome){
					// "query aggiornamento utente eseguita\n";
					if($statement->affected_rows){
						// "success!\n";
						eliminaToken($token["id"]);
						http_response_code (200);
			            die;
					}else{
						echo "nessuna modifica";
	                    http_response_code (500);
			            die();	
					}
				}else{
					echo $statement->error;
				    http_response_code (500);
			        die();	
				}
			}else{
				http_response_code (404);
			    die();
			}
			
		}else{
		    http_response_code (500);
			die();		
		}
		http_response_code (200);
			die;
	}
	
	function getPaginaPasswordDimenticata(){
		header("Location: http://localhost/progettoscuola/src/forgotPassword.php");
        http_response_code (303);
		die(); 
	}
	
	function inviaMailRipristino(){
		global $dbh;
		echo json_encode($_POST);
		$email = $_POST["email"];
        $lev3 = "SELECT * FROM users WHERE email = ?";
        $statement = $dbh->db->prepare($lev3);
    	$statement->bind_param("s", $email);
        $outcome = $statement->execute();
		$result = $statement->get_result();
		if($outcome){
			if($result->num_rows ){   //esiste l'utente
				$user = $result->fetch_assoc();
				$lev3 = "SELECT * FROM resetPasswordTokens WHERE userId = ?";
				                                                                 //"cerco un eventuale token di ripristino associato\n";
                $statement = $dbh->db->prepare($lev3);
				$statement->bind_param("i", $user["id"]);
				$outcome = $statement->execute();
		        $result = $statement->get_result();
				if($outcome){
					                                                                   // "query ricerca su token eseguita\n";
					if($result->num_rows){     //esiste già un token, eliminalo
					                                   // "esiste un token\n";
						$token = $result->fetch_assoc();
						$token = $token["id"];
						var_dump( $token);
						$lev3 = "DELETE FROM resetPasswordTokens WHERE id = ?";
						                                          // "cerco di eliminare il token\n";
                        $statement = $dbh->db->prepare($lev3);
    	                $statement->bind_param("i", $token);
                        $outcome = $statement->execute();
						if($outcome){      //query eseguita
						                                          // "query eliminazione su token eseguita\n";
							if($statement->affected_rows <= 0){
								echo "eliminazione token fallita\n";
								http_response_code(500);
							    die();
							}else{
					          // "eliminazione token riuscita\n";
							}
						}else{    //query non eseguita
						    echo "query eliminazione su token non eseguita\n";
							http_response_code(500);
							die();
						}
					}else{
						//echo "token non presente\n";
					}
					
					$lev3 = "INSERT INTO resetPasswordTokens (userId) VALUES (?)";  // "provo a creare token\n";               
                    $statement = $dbh->db->prepare($lev3);
    	            $statement->bind_param("i", $user["id"]);
                    $outcome = $statement->execute();
				    if($outcome){     // "query creazione token eseguita\n";                      
						if($statement->affected_rows > 0){     // "token creato\n";
							echo "<a href=\"http://" . $_SERVER["SERVER_NAME"] . ":" . $_SERVER['SERVER_PORT'] . "/progettoscuola/src/ui_2.php/resetpassword/". $dbh->db->insert_id ."/\">ecco il tuo link </a>";
							http_response_code (200);
							die();
						}else{
							echo "creazione token fallita\n";
							http_response_code(500);
						    die();
						}
					}else{
						echo "query creazione token non eseguita\n";
							http_response_code(500);
							die();
					}		
			    }
			}else{    //utemte non trovato
			     http_response_code (404);
	        }
		}else{
			http_response_code (500);
		}
	}


    function forgotPasswordActions(){
		global $res;
		if(isSet($res[2]) && !empty($res[2])){
		    switch($_SERVER["REQUEST_METHOD"]){
				case "GET":
				    getFormRipristinoPassword($res[2]);
				break;
				case "POST":
				    ripristinaPassword($res[2]);
			}
		}else{
			switch($_SERVER["REQUEST_METHOD"]){
				case "GET":
				    getPaginaPasswordDimenticata();
				break;
			    case "POST":
			        inviaMailRipristino();
		    	break;
			}
		}
	}
	?>