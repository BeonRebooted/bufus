<?php
    function statusCodes($code, $printDesc = false, $payload = null){

		http_response_code($code);
		
		switch($code){
			case 200://ok
			   $res = ["result" => "OK" , "wut" => "The request has succeeded. The meaning of the success depends on the HTTP method:
						GET: The resource has been fetched and is transmitted in the message body.
						HEAD: The representation headers are included in the response without any message body.
						PUT or POST: The resource describing the result of the action is transmitted in the message body.
						TRACE: The message body contains the request message as received by the server."];
			break;
			case 201://created
			    $res = ["result" => "Created", "wut" => "The request has succeeded and a new resource has been created as a result. This is typically the response sent after POST requests, or some PUT requests."];
			break;
			case 202://accepted
			    $res = ["result" => "Accepted", "wut" => "The request has been received but not yet acted upon. It is noncommittal, since there is no way in HTTP to later send an asynchronous response indicating the outcome of the request. It is intended for cases where another process or server handles the request, or for batch processing."];
			break;
			case 205://reset content
			   $res = ["result" => "Reset Content", "wut" => " Tells the user-agent to reset the document which sent this request."];
			break;
			case 300://Multiple Choice
			   $res = ["result" => "Multiple Choice", "wut" => "The request has more than one possible response. The user-agent or user should choose one of them. (There is no standardized way of choosing one of the responses, but HTML links to the possibilities are recommended so the user can pick.)"];
			break;
			case 301://Moved Permanently
			   $res = ["result" => "Moved Permanently", "wut" => "The URL of the requested resource has been changed permanently. The new URL is given in the response."];
			break;
			case 303://See Other
			   $res = ["result" => "See Other", "wut" => "    The server sent this response to direct the client to get the requested resource at another URI with a GET request."];
			break;
			case 307://Temporary Redirect
			   $res = ["result" => "Temporary Redirect", "wut" => "The server sends this response to direct the client to get the requested resource at another URI with same method that was used in the prior request. This has the same semantics as the 302 Found HTTP response code, with the exception that the user agent must not change the HTTP method used: If a POST was used in the first request, a POST must be used in the second request."];
			break;
			case 308://Permanent Redirect
			   $res = ["result" => "Permanent Redirect", "wut" => "This means that the resource is now permanently located at another URI, specified by the Location: HTTP Response header. This has the same semantics as the 301 Moved Permanently HTTP response code, with the exception that the user agent must not change the HTTP method used: If a POST was used in the first request, a POST must be used in the second request."];
			break;
			case 205://reset content
			   $res = ["result" => "Reset Content", "wut" => " Tells the user-agent to reset the document which sent this request."];
			break;
			case 400://Bad Request
			   $res = ["result" => "Bad Request", "wut" => "The server could not understand the request due to invalid syntax."];
			break;
			case 401://Unauthorized
			   $res = ["result" => "Unauthorized", "wut" => "Although the HTTP standard specifies \"unauthorized\", semantically this response means \"unauthenticated\". That is, the client must authenticate itself to get the requested response."];
			break;
			case 403://Forbidden
			   $res = ["result" => "Forbidden", "wut" => "The client does not have access rights to the content; that is, it is unauthorized, so the server is refusing to give the requested resource. Unlike 401, the client's identity is known to the server."];
			break;
			case 404://Not Found
			   $res = ["result" => "Not Found", "wut" => " The server can not find the requested resource. In the browser, this means the URL is not recognized. In an API, this can also mean that the endpoint is valid but the resource itself does not exist. Servers may also send this response instead of 403 to hide the existence of a resource from an unauthorized client. This response code is probably the most famous one due to its frequent occurrence on the web."];
			break;
			case 405://Method Not Allowed
			   $res = ["result" => "Method Not Allowed", "wut" => "The request method is known by the server but is not supported by the target resource. For example, an API may forbid DELETE-ing a resource."];
			break;
			case 406://Not Acceptable
			   $res = ["result" => "Not Acceptable", "wut" => "    This response is sent when the web server, after performing server-driven content negotiation, doesn't find any content that conforms to the criteria given by the user agent."];
			break;
			case 415://Unsupported Media Type
			   $res = ["result" => "Unsupported Media Type", "wut" => "The media format of the requested data is not supported by the server, so the server is rejecting the request."];
			break;
			case 418://I'm a teapot
			   $res = ["result" => "I'm a teapot", "wut" => "The server refuses the attempt to brew coffee with a teapot."];
			break;
			case 500://Internal Server Error
			   $res = ["result" => "Internal Server Error", "wut" => "The server has encountered a situation it doesn't know how to handle."];
			break;
			case 511://Network Authentication Required
			   $res = ["result" => "Network Authentication Required", "wut" => "The 511 status code indicates that the client needs to authenticate to gain network access."];
			break;
			default:
			   $res = ["result" => "UNKNOWN status Code", "wut" => "ma che codice è?"];
			break;
		}
		
		if($printDesc && !isSet($payload)){
			echo JSON_ENCODE($res);
			die();
		}elseif($printDesc && isSet($payload)){
			$res["wut"] = $payload;
			echo json_encode($res);
			die();
		}else{
			die();
        }	
	}	
?>