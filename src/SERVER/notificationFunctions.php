<?php 
function notificationsRoot(){
	global $res;
	global $_USER_REQUESTER;
	if(isSet($res[2]) && is_numeric($res[2])){
		switchByHTTPMethod(
			function(){ echo json_encode(getNotification($res[2])); die(); },
		    null,
			null,
			function(){ global $res; delNotification($res[2]); die(); }
		);
	}else{
		switchByHTTPMethod(
			function(){ getNotifications(); },
		    null,null,null);		
	}
}



	 function delNotification($id){
		global $_USER_REQUESTER;
		if(selectUserNotification($_USER_REQUESTER, $id)){
			try{
				$res = delNotification($id);
				statusCodes(($res) ? 200 : 500);
				die();
			}catch(Exception $e){
				statusCodes(500, true, $e);
			}
		}
		else statusCodes(404);
	}
	
 function getNotification($id){
		global $_USER_REQUESTER;
		try{
			$notif = selectUserNotification($_USER_REQUESTER, $id);
			echo JSON_ENCODE($notif);
			die();
		}catch(Exception $e){
			statusCodes(500, true, $e);
		}
}	

 function getNotifications(){
		global $_USER_REQUESTER;
		
		try{
			$notifs = selectUserNotifications($_USER_REQUESTER);
			echo JSON_ENCODE($notifs);
			die();
		}
		catch(Exception $e){
			statusCodes(500, true, $e);
		}
}
?>