<?php
	require_once "postOrder.php";



	function ordersRoot(){
		global $res;
		global $_USER_REQUESTER;
                if(! $_USER_REQUESTER){
                    statusCodes(401);
                }
                
		if(isSet($res[2]) && !empty($res[2])){
			if(is_numeric($res[2])){    //    orders/id/
			    if(isSet($res[3]) && !empty($res[3])){
					switch($res[3]){
						case "notifications":
						//orderNotificationsAction();
						statusCodes(400,true);
						break;
						default:
						statusCodes(400,true);
						die;
					}
				}
				else{
					switchByHTTPMethod(
						function(){
                                                        global $res;
							getOrder($res[2]);
						},
						null,
						function(){
                                                        global $ordersSelector;
							postOrder($ordersSelector);
						},
						null
					);
				}
			}else{
				statusCodes(405,false);
			}
		}
		else{
			switchByHTTPMethod(
				function(){
                                        global $_USER_REQUESTER;
					getUserOrders($_USER_REQUESTER);
				},
				null,
				function(){
					postOrders();
				},
				null
			);
		}
	}
/*	
	function orderNotificationsAction(){
		global $res;
		global $_USER_REQUESTER;
		//c'è un id
		if(isSet($res[4]) && !empty($res[4])){
			$foundStatePos = false;
			foreach(["ATTESAPAGAMENTO","ACCETTATO","SPEDITO","ARRIVATO_STAZIONE"] as $state){
				$foundStatePos = strpos($res[4], $state);
				if($foundStatePos)
					break;
			}
			if(! $foundStatePos){
				statusCodes(400,true);
			}
			switch($_SERVER["REQUEST_METHOD"]){
				case "GET":
				    statusCodes(405, true);
				break;
				case "PUT":
				    statusCodes(405, true);
				break;
				case "POST":
				    statusCodes(405, true);
				break;
				case "DELETE":
				    statusCodes(405, true);
				break;
				default:
					statusCodes(400,true);
			}
		//richiesta sull insieme
		}else{
			statusCodes(405, true);
			switch($_SERVER["REQUEST_METHOD"]){
				case "GET":
				break;
				case "PUT":
				break;
				case "POST":
				break;
				case "DELETE":
				break;
				default:
					statusCodes(400,true);
			}
		}
		statusCodes(200,true);
		
	}
*/
?>
<?php	
    function getOrder($orderId){
		global $_USER_REQUESTER;
		try{
			$order = selectOrder($orderId);
			if(isSet($order["id"])){
				if($_USER_REQUESTER == $order["sellerId"]){
					unset($order["payTokenId"]);
				}else if($_USER_REQUESTER != $order["clientId"]){
					statusCodes(403, true);
				}
			}
			echo JSON_ENCODE($order);
			die();
		}catch(Exception $e){
			statusCodes(500, true, "DB error");
		}
	}	

    function putOrder(){
		//muorimale
	}

    function getUserOrders($userId){
		try{
			echo JSON_ENCODE(selectUserOrders($userId));
		}catch(Exception $e){
			statusCodes(500, true, "DB error");
		}
	}	
	
	function postOrder($orderId){

		global $_USER_REQUESTER;
		
		
		if($_USER_REQUESTER == null)
			statusCodes(500, true);
		

		try{
			if(!isOrderSeller($orderId, $_USER_REQUESTER) && !isOrderClient($orderId, $_USER_REQUESTER)){
				statusCodes(403, true);
			}
			
			echo JSON_ENCODE(updateOrderStatus($orderId));
		}catch(Exception $e){
			statusCodes(500, true, "DB error");
		}

		
	}
?>