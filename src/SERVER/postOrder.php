<?php	
	require_once "productFunctions.php";
	
    function postOrders(){
		global $_USER_REQUESTER;
               
                //c'è il campo details nella form?
                $rawDetails = (isSet($_POST["details"]))? json_decode($_POST["details"], true) : [];
                //è un array popolato?
                if((!is_array($rawDetails)) || (count($rawDetails) < 1)){
                    statusCodes(404);
                }
                //smista i dettagli tra i vari venditori
                $sellers = [];
		foreach($rawDetails as $rawDetail){
                    productExistsAndSellableOrDie($rawDetail);
                    //dati freschi
                    $product = selectProduct($rawDetail["id"]);	
                    $seller = $product["sellerId"];
                    //il venditore non è nell'array ?
			if(! isSet( $sellers[ $seller ] ) ){
				$sellers[ $seller ] = Array();
			}
                        //aggiungi in fondo alla lista del venditore
			$sellers[ $seller ] [ count($sellers[$seller]) ] = $rawDetail;
		}
                //crea un ordine per ogni venditore
                //se la transazione completa, ottieni un link al pagamento
		$links = insertOrders($_USER_REQUESTER, $sellers);
                
                if(count($links) < 1){
                    $links = [];
                }else if(count($links) == 1){
                    echo json_encode([$links]);
                }else{
                    echo json_encode($links);
                }
                die();
	}
	
	function insertOrders($clientId, $sellers){
            $links = [];
            //l'ordine è diviso tra i singoli venditori
            foreach($sellers as $sellerId => $details){
                $links[$sellerId] = insertOrder( $clientId ,$sellerId, $details);
            }
            return $links;
	}
	
	function insertOrder($idCliente, $idVenditore, $dettagli){
		global $dbh;
		$idOrdine = null;
		$tokenId = null;
		$erroriToken = false;
		$erroriInDetails = false;
		$totale = 0;
                
		//check validita dettagli
		foreach($dettagli as $detail){
                        $product = selectProduct($detail["id"]);
                        if($product["sellerId"] != $idVenditore){
                            throw new Exception("prodotto appartenente a un altro venditore");
                        }
			$idProdotto = $product["id"];
			$productOptions = selectProductOptions($idProdotto);
                        if(!optionsExist($productOptions, $detail["options"])){
                            throw new Exception("opzioni inesistenti");
                        }      
                        if(! requiredOptionsGiven($productOptions, $detail["options"])){
                            throw new Exception("opzioni obbligatorie mancanti");
                        }
			if(! optionsValuesAreLegit($productOptions, $detail["options"])){
                            throw new Exception("dati opzione invalidi");
			}
		}
		//calcolo totale
		$prezzo = 0;
		foreach($dettagli as $detail){
			$product = selectProduct($detail["id"]);
			$idProdotto = $product["id"];
			$prezzo += $product["price"] * $detail["quantity"];
			$productOptions = selectProductOptions($idProdotto);
			foreach($detail["options"] as $opzione){
                                $opzioneFresca = selectProductOption($idProdotto, $opzione["name"]);
				$prezzo += $opzioneFresca["price"] * $detail["quantity"];
			}
		}
		$query = "INSERT INTO orders (clientId, sellerId, price, status) VALUES ( ?, ?, ?, 'ATTESAPAGAMENTO' )";
		$statement = $dbh->db->prepare($query);
		$statement->bind_param("iid", $idCliente, $idVenditore, $prezzo);
		$outcome = $statement->execute();
		if($outcome){
			$res = $statement->get_result();
			if($statement->affected_rows){
				$idOrdine = $statement->insert_id;
			}else{			
				echo $statement->error;
				http_response_code(403);
				die();
			}
		}else{
			echo $statement->error;
			http_response_code(403);
			die();
		}
		$statement->close();
		
		$query = "INSERT INTO orderPaymentTokens (orderId) VALUES ( ? )";
		$statement = $dbh->db->prepare($query);
		$statement->bind_param("i", $idOrdine);
		$outcome = $statement->execute();
		if($outcome){
			$res = $statement->get_result();
			if($statement->affected_rows){
				$tokenId = $statement->insert_id;
			}else{			
				$erroriToken = true;
			}
		}else{
			$erroriToken = true;
		}
		$statement->close();
		$erroriDettagli = false;
		foreach($dettagli as $detail){
			$product = selectProduct($detail["id"]);
			$idProdotto = $product["id"];
			$productOptions = selectProductOptions($idProdotto);
			
			$prezzo = $product["price"];
			$nomi = "";
			$valori = "";
			foreach($detail["options"] as $opzione){
				$nomi = $nomi . "( \"" . $opzione["name"] . "\") ,";
				$valori = $valori . "( \"" . $opzione["value"] . "\") ,";
			}
			$nomi = rtrim($nomi , ",");
			$valori = rtrim($valori , ",");
			$statement = $dbh->db->prepare("CALL insertOrderDetail(  ?, ?, ?, ?, ?)");
			$statement->bind_param("iiiss", $idOrdine, $idProdotto, $detail["quantity"], $nomi , $valori);
			$outcome = $statement->execute();
			if(! $outcome){
				echo $dbh->db->error;
				$erroriDettagli = true;
			}
			$statement->close();
		}
		
		if($erroriToken || $erroriDettagli){
			//annulla transazione
			$query = "DELETE FROM orders WHERE id = ?";
			$statement = $dbh->db->prepare($query);
			$statement->bind_param("i", $idOrdine);
			$outcome = $statement->execute();
			$statement->close();
			$query = "DELETE FROM orderDetails WHERE orderId = ?";
			$statement = $dbh->db->prepare($query);
			$statement->bind_param("i", $idOrdine);
			$outcome = $statement->execute();
			$statement->close();
			
			echo "errori nella scansione dei dettagliOrdini ".$erroriInDetails ." o nella creazione del token" .$erroriToken;
			http_response_code(500);
			die();
		}else{
			return "paymentservice/payment.php/" . $tokenId . "/";
		}		
      
        }

	function optionsExist($allProductOptions, $optionsGiven){
		foreach($optionsGiven as $option){
			$found = false;
			foreach($allProductOptions AS $opzione){
				if($option["name"] == $opzione["name"]){
					$found = true;
					break;
				}
			}
			if(! $found){
				return false;
			}
		}
		return true;
	}
	
	function requiredOptionsGiven($allProductOptions, $optionsGiven){
		foreach($allProductOptions as $option){
			if($option["required"]){
				$found = false;
				foreach($optionsGiven as $opzione){
					if($opzione["name"] == $option["name"]){
						$found = true;
						break;
					}
				}
				if(!$found){
					return false;
				}
			}
		}
		return true;
	}
	
	function optionsValuesAreLegit($allProductOptions, $optionsGiven){
		GLOBAL $dbh;
		
		foreach($optionsGiven as $opzione){
			foreach($allProductOptions as $option){
				if($option["name"] == $opzione["name"]){
					switch($option["type"]){
						case "VALUE":
							$statement = $dbh->db->prepare("SELECT * FROM `valueOptions` WHERE productId = ? AND name = ? AND value = ?");
							$statement->bind_param("iss", $option["productId"], $option["name"] , $opzione["value"]);
							$outcome = $statement->execute();
							if(!$outcome){
								throw new Exception ("non esiste il valore (maybe)");
                                                        }else{
                                                            return ($statement->get_result()->fetch_assoc() != null);
                                                        }
						break;
						case "FILEVALUE":
                                                    return true;
						break;
						case "TEXTVALUE":
                                                    return true;
						break;
						case "INTRANGE":
							$statement = $dbh->db->prepare("SELECT * FROM intRangeOptions WHERE productId = ? AND name = ?");
							$statement->bind_param("is",  $option["productId"],  $option["name"]);
							$outcome = $statement->execute();
							if($outcome){
								$data = $statement->get_result()->fetch_assoc();
								if($data["minimum"] > $opzione["value"] || $data["maximum"] < $opzione["value"] ){
									return false;
                                                                }else{
                                                                    return true;
                                                                }
							}else{
								throw new Exception ("non dovrei essere qui");
							}
						break;
						case "FLOATRANGE":
							$statement = $dbh->db->prepare("SELECT * FROM floatRangeOptions WHERE productId = ? AND name = ?");
							$statement->bind_param("is",  $option["productId"],  $option["name"]);
							$outcome = $statement->execute();
							if($outcome){
								$data = $statement->get_result()->fetch_assoc();
								if($data["minimum"] > $opzione["value"] || $data["maximum"] < $opzione["value"]){
									return false;
                                                                }else{
                                                                    return true;
                                                                }
							}else{
								throw new Exception ("non dovrei essere qui");
							}
						break;
					}
				}
			}			
		}		
	}
        
        function productExistsAndSellableOrDie($prodData){
			$product = selectProduct($prodData["id"]);
                        if(! isSet($product)){
                            statusCodes(404, true, "product " . $prodData["id"] . " not found");
                        }
                        if($product["deleted"]){
                            statusCodes(406, true, "prodotto" . $prodData["id"] . " fuori commercio");
                        }
        }
?>