<?php

function productsRoot() {
    global $res;
    if (isSet($res[2]) && !empty($res[2])) {    //si ha un filtro		    
        if (is_numeric($res[2])) {           //è un id
            productAction();
        } else {  //è una query
            productQueryAction();
        }
    } else {
        productsAction();
    }
}

function productAction() {
    global $res;
    if (isSet($res[3]) && !empty($res[3])) {
        switch ($res[3]) {
            case "image":
                productImageAction();
                break;
            case "reviews":
                productReviewsAction();
                break;
            case "options":
                productOptionsAction();
                break;
            default:
                //bad req
                http_response_code(400);
                die();
                break;
        }
    } else {
        switchByHTTPMethod(
                function() {
            global $res;
            getProduct($res[2]);
        },
                function() {
            global $res;
            putProduct($res[2]);
        },
                null,
                null
        );
    }
}

function productQueryAction() {
    global $res;
    switch ($res[2]) {
        case "categories":
            if (isSet($res[3]) && !empty($res[3])) {
                switchByHTTPMethod(function() {
                    global $res;
                        getProductsByCategoryName($res[3]);
                    die();
                },
                        null,
                        null,
                        null
                );
                die();
            } else {
                getCategories();
            }
            break;
        case "name":
            if (isSet($res[3]) && !empty($res[3])) {
                switchByHTTPMethod(function() {
                    global $res;
                        getProductsByName($res[3]);
                },
                        null,
                        null,
                        null
                );
            } else {
                http_response_code(404);
                die();
            }
            break;
    }
}

function productsAction() {
    global $res;
    switchByHTTPMethod(function() {
        getProducts();
    },
            null,
            function() {
        postProduct();
    },
            null
    );
}

function productImageAction() {
    global $res;
    switchByHTTPMethod(null,
            function() {
        global $res;
        putImage($res[2]);
        statusCodes(200);
    },
            null,
            null);
}

function productReviewsAction() {
    //broken
    //statusCodes(500);
    global $res;
    global $_USER_REQUESTER;
    //if there is some kind of indexing on single product reviews
    if (isSet($res[4]) && !empty($res[4]) && is_numeric($res[4])) {
        
    } else {
        switchByHTTPMethod(
                function() {
            global $res;
            global $_USER_REQUESTER;
            $response = array();
            $response["canIReview"] = canXReviewY($_USER_REQUESTER, $res[2]);
            $response["entries"] = selectProductReviews($res[2]);
            echo(json_encode($response));
            statusCodes(200, false);
        },
                null,
                function() {
            global $res;
            postProductReview($res[2]);
        },
                null
        );
    }
}

function productOptionsAction() {
    global $res;
    global $_USER_REQUESTER;

    // /products/xxx/options/yyy
    if (isSet($res[4]) && !empty($res[4])) {
        switchByHTTPMethod(
                function() {
                    global $res;
                    getProductOption($res[2], $res[4]);
            statusCodes(200, false);
        }, //get
                function() {
            if ($_USER_REQUESTER == getProduct($res[2])["sellerId"]) {

                statusCodes(200, false);
            } else {
                statusCodes(403, false);
            }
        }, //put
                null, //post
                function() {
            if ($_USER_REQUESTER == getProduct($res[2])["sellerId"]) {

                statusCodes(200, false);
            } else {
                statusCodes(403, false);
            }
        }  //delete
        );
    } else {
        switchByHTTPMethod(
                function() {
            global $res;
            global $_USER_REQUESTER;

            echo( json_encode(selectProductOptions($res[2])) );
            statusCodes(200, false);
        },
                null,
                function() {
            global $res;
            global $_USER_REQUESTER;
            if ($_USER_REQUESTER == getProduct($res[2])["sellerId"]) {
                postProductOption($res[2]);
                statusCodes(200, false);
            } else {
                statusCodes(403, false);
            }
        },
                function() {
            global $res;
            if ($_USER_REQUESTER == getProduct($res[2])["sellerId"]) {
                //clearProductOptions($res[2]);	
                statusCodes(200, false);
            } else {
                statusCodes(403, false);
            }
        }
        );
    }
}
?>
<?php

//http product GET
function getProduct($productId) {
    header('Content-Type: application/json; charset=utf-8');
    $data = selectProduct($productId);
    if ($data) {
        echo json_encode($data);
    }else{
        statusCodes(500);
    }
}

//http product POST
function postProduct() {
    global $dbh;
    global $_USER_REQUESTER;
    $name = $_POST["name"];
    $categoryId = $_POST["categoryId"];
    $price = $_POST["price"];
    $description = $_POST["description"];
    $options = JSON_DECODE($_POST["options"], true);
    $sellerId = $_USER_REQUESTER;
    try {
        $result = insertProduct($sellerId, $name, $categoryId, $price, $description, $options);
        echo JSON_ENCODE($result);
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}

//http product PUT
function putProduct($id) {
    global $dbh;
    global $_USER_REQUESTER;
    if (!isXSellerOfY($_USER_REQUESTER, $id)) {
        echo json_encode(["result" => "not his seller"]);
        statusCodes(500);
    } else {
        //FROM stackOverflow https://stackoverflow.com/questions/9464935/php-multipart-form-data-put-request
        // Fetch content and determine boundary
        $raw_data = file_get_contents('php://input');
        $boundary = substr($raw_data, 0, strpos($raw_data, "\r\n"));
        // Fetch each part
        $parts = array_slice(explode($boundary, $raw_data), 1);
        $data = array();

        foreach ($parts as $part) {
            // If this is the last part, break
            if ($part == "--\r\n")
                break;
            // Separate content from headers
            $part = ltrim($part, "\r\n");
            list($raw_headers, $body) = explode("\r\n\r\n", $part, 2);
            // Parse the headers list
            $raw_headers = explode("\r\n", $raw_headers);
            $headers = array();
            foreach ($raw_headers as $header) {
                list($name, $value) = explode(':', $header);
                $headers[strtolower($name)] = ltrim($value, ' ');
            }
            // Parse the Content-Disposition to get the field name, etc.
            if (isset($headers['content-disposition'])) {
                $filename = null;
                preg_match(
                        '/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/',
                        $headers['content-disposition'],
                        $matches
                );
                list(, $type, $name) = $matches;
                isset($matches[4]) and $filename = $matches[4];
                // handle your fields here
                switch ($name) {
                    // this is a file upload
                    //case 'userfile':
                    //file_put_contents($filename, $body);
                    case 'image':
                        throw new Exception("non ci sono immagini qui");
                        break;
                    // default for all other files is to populate $data
                    default:
                        $data[$name] = substr($body, 0, strlen($body) - 2);
                        break;
                }
            }
        }
        $data["options"] = JSON_DECODE($data["options"], true);
        $result = updateProduct($id, $data["name"], $data["categoryId"], $data["price"], $data["description"], $data["deleted"], $data["options"]);
        if ($result) {
            echo json_encode($result);
        } else {
            echo json_encode($result);
        }
    }
}

//db product UPDATE

function putImage($productId) {
    global $dbh;
    global $_USER_REQUESTER;
    $prod = selectProduct($productId);
    if (isSet($prod) && ($prod["sellerId"] == $_USER_REQUESTER)) {
        $in_db_name = "../upload/products/" . $productId . ".jpg";
        PUT_Imagedata($in_db_name);
        if ($in_db_name != null) {
            http_response_code(200);
        } else {
            http_response_code(500);
        }
    }
}

function getProducts() {
    header('Content-Type: application/json; charset=utf-8');
    try {
        echo JSON_ENCODE(selectProducts());
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}

function getProductsBySellerId($sellerId) {
    header('Content-Type: application/json; charset=utf-8');
    try {
        echo JSON_ENCODE(selectProductsBySellerId($sellerId));
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}

function getProductsByName($name) {
    header('Content-Type: application/json; charset=utf-8');
    try {
        echo JSON_ENCODE(selectProductsByName($name));
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}

function getProductsByCategoryName($name) {
    //TOFIX https://stackoverflow.com/questions/9946219/sql-like-wildcard-space-character
    header('Content-Type: application/json; charset=utf-8');
    try {
        echo JSON_ENCODE(selectProductsByCategoryName($name));
        
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}

function getProductReviews($productId) {
    header('Content-Type: application/json; charset=utf-8');
    try {
        echo JSON_ENCODE(selectProductReviews($productId));
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}

function postProductReview($productId) {
    header('Content-Type: application/json; charset=utf-8');
    global $_USER_REQUESTER;
    if ($_USER_REQUESTER == null)
        statusCodes(401, true);
    $voto = $_POST["rating"];
    $testo = $_POST["text"];

    try {
        echo JSON_ENCODE(insertProductReview($productId, $_USER_REQUESTER, $voto, $testo));
        die();
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}

function getProductOption($productId, $name) {
    header('Content-Type: application/json; charset=utf-8');
    try {
        echo JSON_ENCODE(selectProductOption($productId, $name));
        die();
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}

function getProductOptions($productId) {
    header('Content-Type: application/json; charset=utf-8');
    try {
        echo JSON_ENCODE(selectProductOptions($productId));
        die();
    } catch (Exception $e) {
        statusCodes(500, true, $e);
    }
}


function PUT_Imagedata($destination) {

    //FROM stackOverflow https://stackoverflow.com/questions/9464935/php-multipart-form-data-put-request
    // Fetch content and determine boundary
    $raw_data = file_get_contents('php://input');
    $boundary = substr($raw_data, 0, strpos($raw_data, "\r\n"));
    // Fetch each part
    $parts = array_slice(explode($boundary, $raw_data), 1);
    $data = array();

    foreach ($parts as $part) {
        // If this is the last part, break
        if ($part == "--\r\n")
            break;

        // Separate content from headers
        $part = ltrim($part, "\r\n");
        list($raw_headers, $body) = explode("\r\n\r\n", $part, 2);

        // Parse the headers list
        $raw_headers = explode("\r\n", $raw_headers);
        $headers = array();
        foreach ($raw_headers as $header) {
            list($name, $value) = explode(':', $header);
            $headers[strtolower($name)] = ltrim($value, ' ');
        }

        // Parse the Content-Disposition to get the field name, etc.
        if (isset($headers['content-disposition'])) {
            $filename = null;
            preg_match(
                    '/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/',
                    $headers['content-disposition'],
                    $matches
            );
            list(, $type, $name) = $matches;
            isset($matches[4]) and $filename = $matches[4];

            // handle your fields here
            switch ($name) {
                // this is a file upload
                //case 'userfile':
                //file_put_contents($filename, $body);
                case 'image':

                    file_put_contents($destination, $body);
                    break;

                // default for all other files is to populate $data
                default:
                    $data[$name] = substr($body, 0, strlen($body) - 2);
                    break;
            }
        }
    }


    /*
      $putdata = fopen("php://input", "r");




      // Open a file for writing
      $fp = fopen($destination, "w");

      // Read the data 1 KB at a time and write to the file
      while ($data = fread($putdata, 1024))
      fwrite($fp, $data);

      // Close the streams
      fclose($fp);
      fclose($putdata);
     */
}

function PUT_productData() {
    
}
?>