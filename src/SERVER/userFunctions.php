<?php
	function usersRoot(){
		global $res;
		
		$hasSelector = isSet($res[2]) && !empty($res[2]);
		if($hasSelector){
			usersSelectorFunctions();
		}else{
			switchByHTTPMethod(
				function(){	echo json_encode( getUsers()); die(); },
				null,
				function(){ postUser(); },
				null
			);
		}
		die();
	}
	
	function usersSelectorFunctions(){
			switchByHTTPMethod(
				GET_usersBySelector(),
				null,null,null
			);	
	}
	
	function GET_usersBySelector(){
		global $res;
		$selector = $res[2];
		if(is_Numeric($selector)){
			if(isSet($res[3]) && !empty($res[3])){
				switch($res[3]){
				    case "products":
					    getProductsBySellerId($selector);
						statusCodes(200);
                    break;
                    default:
					    http_response_code(404);
						die;
					break;
				}
			}else{
				getUser($selector);
			}	
		}else{
	        getUsersByName($selector);
		}
	}


?>
<?php
	function getUserFromHeader(){
		$headers = getallheaders();
		if(isSet($headers["Authorization"])){
			$auth = $headers["Authorization"];
			$auth = substr($auth, strlen("Basic "));
            $mailAndPwd = explode(":", base64_decode($auth));
			return selectUserByEmailAndPassword( $mailAndPwd[0], $mailAndPwd[1]);
		}else{
			return null;
		}
	}

	function getUsers(){
		header('Content-Type: application/json; charset=utf-8');
		try{
			$res = selectUsers();
			echo JSON_ENCODE($res);
		}catch(Exception $e){
			statusCodes(500, true, "DB error");
		}
		die();
	}

	function getUser($id){
		global $_USER_REQUESTER;
		header('Content-Type: application/json; charset=utf-8');
		try{
			$usr = selectUser($id);
			if($usr){
				if($usr["id"] == $_USER_REQUESTER)
                                    $usr["notifications"] = selectUserUnreadNorifications($_USER_REQUESTER);
                                echo JSON_ENCODE($usr);     
                        }else{
                            statusCodes(404);
                        }
			
		}catch(Exception $e){
			statusCodes(500, true, "DB error");
		}
		die();
	}
	
	function postUser(){
		global $dbh;
		header('Content-Type: application/json; charset=utf-8');
		$name = $_POST["name"];
		$email = $_POST["email"];
		$password = $_POST["password"];
		$type = $_POST["type"];
		try{
			$usrId = insertUser($name, $email, $password, $type);
			echo JSON_ENCODE(selectUser($usrId));
			http_response_code (200);
		}catch(Exception $e){
			http_response_code (500);
			echo json_encode($e);
		}
		die();
	}
	
	function getUsersByName($name){
		header('Content-Type: application/json; charset=utf-8');
		try{
			echo JSON_ENCODE(selectUsersByName($name));
		}catch(Exception $e){
			statusCodes(500, true, "DB error");
		}
		die();
	}
	?>