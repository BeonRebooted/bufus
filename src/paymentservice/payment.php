<?php
   require "../SERVER/db.php";
 
function echoPageForConfirm($token){
	echo "<button id='conf'>CONFIRM</button>";
	echo "<button id='refu'>ANYFAIL</button>";
	echo "<script>
	      function inviaSuccesso(e){
			    e.preventDefault();
			  	req = new XMLHttpRequest();
                req.open('POST', URLroot() + '/progettoscuola/src/paymentservice/payment.php/" . $token["id"] ."/',true);
                req.onreadystatechange = () => {
                                                if(req.readyState === XMLHttpRequest.DONE){
                                                    console.log(req.status);
                                                    alert(req.responseText);
         
													window.opener.paymentConfirmed();
													window.close();
                                                }
                                               }
                fd = new FormData();
                fd.append('response','CONFIRM');
                req.send(fd);
		  }
		  
		  async function inviaFallimento(e){
			    e.preventDefault();
			  	req = new XMLHttpRequest();
                req.open('POST',URLroot() + '/paymentservice/payment.php/" . $token['id' ] . "/',true);
                req.onreadystatechange = () => {
                                                if(req.readyState === XMLHttpRequest.DONE){
                                                    console.log(req.status);
                                                    console.log(req.responseText);
													window.close();
													window.opener.paymentError();
                                                }
				                               } 
                fd = new FormData();
                fd.append('response','ANYFAIL');
                req.send(fd);
		  }
		  
          document.getElementById('conf').onclick = inviaSuccesso;	
          document.getElementById('refu').onclick = inviaFallimento;	

			function URLroot(){
				root = document.URL.toLowerCase().split('ui_2.php')[0];
				return root;
			}
			
			function URLbase(){
				return URLroot().toLowerCase().split('src')[0];
			}		  

	</script>";
} 
 
if(isSet($_SERVER["PATH_INFO"])){
		$res = preg_split("/\//", $_SERVER["PATH_INFO"]);
		$token = $res[1];
		$query = "SELECT * FROM orderPaymentTokens WHERE id = ?";
		$statement = $dbh->db->prepare($query);
		$statement->bind_param("i", $token);
		$outcome = $statement->execute();
		if($outcome){
			$res = $statement->get_result();
			$res = $res->fetch_assoc();
			if(!isSet ($res["id"])){
				echo "NON ESISTE IL TOKEN";
			}else{
				switch($_SERVER["REQUEST_METHOD"]){
					case "GET":
					    echoPageForConfirm($res);
					BREAK;
					
					case "POST":
					    $response = $_POST["response"];
						if($response == "CONFIRM"){
							echo "purchase complete";
							$query = "UPDATE orders SET status = 'ACCETTATO'  WHERE id = ?";
							$statement = $dbh->db->prepare($query);
		                    $statement->bind_param("i", $res["orderId"]);
		                    $outcome = $statement->execute();
		                    if($outcome){
			                    if($statement->affected_rows){
				                    echo "order updated";
			                    }else{			
                                    echo "no order affected";
			                    }
		                    }else{
								echo $statement->error;
                                echo "broke during order update";
		                    }
							
						    $query = "DELETE FROM orderPaymentTokens WHERE id = ?";
							$statement = $dbh->db->prepare($query);
		                    $statement->bind_param("i", $res["id"]);
		                    $outcome = $statement->execute();
							if($outcome){
			                    if($statement->affected_rows){
				                    echo "token deleted\n";
									http_response_code(200);
			                    }else{			
                                    echo "no token affected";
			                    }
		                    }else{
                                echo "broke during token delete";
		                    }
						}else{
							echo "purchase failed";
						}
					BREAK;
					
					default:
					    echo "ONLY GET or POST";
                  	    http_response_code(405);
	                die();
					BREAK;
				}
			}
		}else{
		    echo "broke during query";
	        http_response_code(500);
	        die();
		}		
}else{
	echo "missing payment tokenId";
	http_response_code(405);
	die();
}

?>