<?php 
    require "./SERVER/db_interface.php";
    require "./SERVER/userFunctions.php";
    require "./SERVER/productFunctions.php";
    require "./SERVER/orderFunctions.php";
    require "./SERVER/forgotPasswordFunctions.php";
    require "./SERVER/notificationFunctions.php";
    require "./SERVER/httpCodes.php";

    $_USER_REQUESTER = null;	
    $usr = getUserFromHeader();
    if(isSet($usr)){
	$_USER_REQUESTER = $usr["id"];
    }
    
    $codaEsiste = isSet($_SERVER["PATH_INFO"]);
    
    if($codaEsiste){
        $coda = $_SERVER["PATH_INFO"];
        $codaBanale = (strlen($coda) <= 1);
        //abbiamo un URL tipo service.php/*...   ???
        if(! $codaBanale){
            $res = preg_split("/\//", $coda);
            //parte da 1 (l'elemento 0 è stringa vuota)
            switch($res[1]){
                case "users":
                    usersRoot();
                break;
                case "forgotPassword":
                    forgotPasswordActions();
                break;
                case "categories":
                    switchByHTTPMethod(
                        function(){
                            getCategories();
                        },
                        null,
                        null,
                        null
                    );
                break;		
                case "orders":
                    ordersRoot();
                break;
                case "products":
                    productsRoot();
                break;
                case "notifications":
                    notificationsRoot();
                break;
                default:
                    statusCodes(404, false);
            }
        }
        else{
            //abbiamo URL service.php/
            switchByHTTPMethod(
                function(){	
                        header("Location: ../UI_2.php");
                        die();
                    },
                    null,
                    function(){
                        tapUser();
                    },
                    function(){	
                        global $dbh;
                        $dbh->reboot();
                        echo "db rebooted";
                        die();
                    }
                );            
        }
    }//abbiamo URL service.php
    else{
        switchByHTTPMethod(
                    function(){	
                        header("Location: ./UI_2.php");
                        die();
                    },
                    null,
                    function(){
                        tapUser();
                    },
                    function(){	
                        $dbh->reboot();
                        echo "db rebooted";
                        die();
                }
            );
        }
    die();
?>
<?php	
	/** calls bound method or throws 405 : METHOD NOT ALLOWED
		after that it dies
	*/
	function switchByHTTPMethod($GETCallback, $PUTCallback, $POSTCallback,$DELETECallback){
		switch($_SERVER["REQUEST_METHOD"]){
			case "GET":
			    if(isSet($GETCallback)){
					call_user_func($GETCallback);
					die();
				}
			break;
			case "POST":
			    if(isSet($POSTCallback)){
					call_user_func($POSTCallback);
					die();
				}
			break;
			
			case "PUT":
			    if(isSet($PUTCallback)){
					call_user_func($PUTCallback);
					die();
				}
			break;	
			case "DELETE":
			    if(isSet($DELETECallback)){
					call_user_func($DELETECallback);
					die();
				}
			break;
		}
		statusCodes(405, false);
		die();
	}

    function tapUser(){
        global $_USER_REQUESTER;
        if($_USER_REQUESTER){
            header('Content-Type: application/json; charset=utf-8');
            getUser($_USER_REQUESTER);
            statusCodes(200);
        }else{
            statusCodes(403);
        }
    }
        
    function getCategories(){
        try{
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode(selectCategories());
	}catch(Exception $e){
            statusCodes(500, true, "DB ERROR");
	};
	die();
    }
	
?>