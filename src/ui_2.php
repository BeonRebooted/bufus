<?php ini_set('session.use_cookies', '0'); ?> <!--NO COOOKIES PLZ-->

<!DOCTYPE html>
<html lang="it">
	<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<style>
		    <?php require "./CLIENT/style.css"; ?>
		</style>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	     
		 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dela+Gothic+One">
	
		<title>NEGOZIETTI_WEBAPP</title>

	</head>	
    <body class="p-0" style="font-family: 'Dela Gothic One' ; font-size: 13px;" aria-live="polite">
		<script><?php require "./CLIENT/functions.js"; ?></script>
        <script><?php require "./CLIENT/UI_2.js"; ?></script>
		<script><?php require "./CLIENT/cartActions.js"; ?></script>
		<script><?php require "./CLIENT/pushies.js"; ?></script>
		<?php require "./CLIENT/UI_PRODUCT/UI_PRODUCTMODALS.php"; ?>
		<?php require "./CLIENT/UI_HEADER.php"; ?>
		<main class="container-fluid px-0" style="min-height: 90vh" aria-label="webapp content">
			    <?php require "./CLIENT/UI_LOGIN.php" ?>
	            <?php require "./CLIENT/UI_HOME.php" ?>
			    <?php require "./CLIENT/UI_PRODUCTS.php" ?>
			    <?php require "./CLIENT/UI_PRODUCT/MAIN.php" ?>
				<?php require "./CLIENT/UI_CHECKOUT.php" ?>
				<?php require "./CLIENT/UI_ORDERS.php" ?>
				<?php require "./CLIENT/UI_NOTIFICATIONS.php" ?>
				<?php require "./CLIENT/UI_SETTINGS.php" ?>
				<?php require "./CLIENT/UI_REGISTER.php" ?>
				<?php require "./CLIENT/UI_FORGOTPASSWORD.php" ?>
				<?php require "./CLIENT/UI_RESETPASSWORD.php" ?>
				<?php require "./CLIENT/UI_CART.php" ?>
		</main>
		<ol id="pushies" class="container-fluid position-fixed m-0" aria-live="polite" aria-atomic="true" style="bottom: 0; right: 0; z-index: 100;">		
		</ol>
	    <!--brutto modo ma funziona-->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>


    </body>
</html>